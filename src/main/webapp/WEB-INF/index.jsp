<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Search Page</title>
    </head>
    <body>
        <h1>Search Page</h1>
        <form:form commandName="searchForm" id="searchForm" name="searchForm" method='post'  action="${context}/webservice/test/results/1/25">
            <form:input  path="searchElement" id="searchTxtId"  placeholder="Search" tabindex="1" autofocus="true" /><br/>
            <input type="radio" name="exactMatch" value="true" checked> Exact
            <input type="radio" name="exactMatch" value="false" /> Fuzzy<br>
            <input type="submit" value="Submit"/>
        </form:form>
        <c:choose>
            <c:when test="${empty searchForm.listOfResult && searchForm.resultFound eq false}">
                not available
            </c:when>
            <c:when test="${not empty searchForm.listOfResult}">
                Total Results:   ${searchForm.resultSize}
                <div><br/></div>
                    <c:forEach items="${searchForm.listOfResult}" var="searchResult" varStatus="i">
                    <div>
                        <b>S. No.</b> ${i.index+1}<br/>
                        <b>identifier:</b> ${searchResult.recordIdentifier}<br/>
                        <b>Title:</b> ${searchResult.title}<br/>
                        <b>Description:</b> ${searchResult.description}<br/>
                        <b>Subject:</b> ${searchResult.subject}<br/>
                        <b>Publisher:</b> ${searchResult.publisher}<br/>
                        <b>UDC notations:</b> ${searchResult.udcNotation}<br/>
                        <b>Creator:</b> ${searchResult.creator}<br/>
                        <b>Type:</b> ${searchResult.type}<br/>
                    </div><hr>
                </c:forEach>
            </c:when>
        </c:choose> 
        <%--  <br/> Allowed Languages:  ${allowedLang} --%>
    </body>
</html>
