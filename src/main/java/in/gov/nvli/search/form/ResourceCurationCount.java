package in.gov.nvli.search.form;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;

/**
 * Class is tend for Holding curation count of Resource.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 */
@SuppressWarnings("serial")
public class ResourceCurationCount implements Serializable {

    /**
     * Will hold Resource code
     */
    private String resourceTypeCode;
    /**
     * Will hold resource Name
     */
    private String resourceTypeName;
    /**
     * Map will hold Resource/institute wise curation count.
     * <p>
     * <li>Key: Resource code. </li>
     * <li>value: CurationCount @see CurationCount</li>
     * </p>
     *
     */
    private Map<String, CurationCount> userCurationCounts;

    /**
     * get value of resourceTypeCode property.
     *
     * @return
     */
    public String getResourceTypeCode() {
        return resourceTypeCode;
    }

    /**
     * set value of resourceTypeCode property.
     *
     * @param resourceTypeCode
     */
    public void setResourceTypeCode(String resourceTypeCode) {
        this.resourceTypeCode = resourceTypeCode;
    }

    /**
     * get value of resourceTypeName property.
     *
     * @return
     */
    public String getResourceTypeName() {
        return resourceTypeName;
    }

    /**
     * set value of resourceTypeName property.
     *
     * @param resourceTypeName
     */
    public void setResourceTypeName(String resourceTypeName) {
        this.resourceTypeName = resourceTypeName;
    }

    /**
     * get value of userCurationCounts property.
     *
     * @return
     */
    public Map<String, CurationCount> getUserCurationCounts() {
        return userCurationCounts;
    }

    /**
     * set value of userCurationCounts property.
     *
     * @param userCurationCounts
     */
    public void setUserCurationCounts(Map<String, CurationCount> userCurationCounts) {
        this.userCurationCounts = userCurationCounts;
    }

    /**
     * generate hash code of object based on resourceTypeCode property.
     *
     * @return hash code of object.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.resourceTypeCode);
        return hash;
    }

    /**
     * check equality of object based on resourceTypeCode property.
     *
     * @param obj
     * @return true if equals else false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResourceCurationCount other = (ResourceCurationCount) obj;
        if (!Objects.equals(this.resourceTypeCode, other.resourceTypeCode)) {
            return false;
        }
        return true;
    }

}
