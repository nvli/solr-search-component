package in.gov.nvli.search.form;

import java.io.Serializable;
import java.util.List;

/**
 * Form to communicate b/w search component and mit webservice.
 *
 * @author Hemant Anjana
 */
@SuppressWarnings("serial")
public class CurationForm implements Serializable {

    /**
     * Resource code
     */
    private String resourceCode;
    /**
     * user id
     */
    private Long userId;
    /**
     * page number
     */
    private String pageNo;
    /**
     * page window i.e per page
     */
    private String pageWin;
    /**
     * curated or uncurated status record
     */
    private CurationStatus status;
    /**
     * comma separated values of languages.
     */
    private List<String> languages;
    /**
     * list of SearchResult
     */
    private List<SearchResult> listOfResult;

    /**
     * size of result
     */
    private Long resultSize;

    /**
     * search term to filter results based on title
     */
    private String searchTerm;
    /**
     * List of subResource identifier or code
     */
    private List<String> subResourceIdentifiers;
    /**
     * List of all subResource identifier assigned to user.
     */
    private List<String> allSubResourceIdentifiers;

    /**
     * get value of allSubResourceIdentifiers property.
     *
     * @return
     */
    public List<String> getAllSubResourceIdentifiers() {
        return allSubResourceIdentifiers;
    }

    /**
     * set value of allSubResourceIdentifiers property.
     *
     * @param allSubResourceIdentifiers
     */
    public void setAllSubResourceIdentifiers(List<String> allSubResourceIdentifiers) {
        this.allSubResourceIdentifiers = allSubResourceIdentifiers;
    }

    /**
     * get value of subResourceIdentifiers property.
     *
     * @return
     */
    public List<String> getSubResourceIdentifiers() {
        return subResourceIdentifiers;
    }

    /**
     * set value of subResourceIdentifiers property.
     *
     * @param subResourceIdentifiers
     */
    public void setSubResourceIdentifiers(List<String> subResourceIdentifiers) {
        this.subResourceIdentifiers = subResourceIdentifiers;
    }

    /**
     * get value of searchTerm property.
     *
     * @return
     */
    public String getSearchTerm() {
        return searchTerm;
    }

    /**
     * set value of searchTerm property.
     *
     * @param searchTerm
     */
    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    /**
     * get value of resultSize property.
     *
     * @return
     */
    public Long getResultSize() {
        return resultSize;
    }

    /**
     * set value of resultSize property.
     *
     * @param resultSize
     */
    public void setResultSize(Long resultSize) {
        this.resultSize = resultSize;
    }

    /**
     * get value of listOfResult property.
     *
     * @return
     */
    public List<SearchResult> getListOfResult() {
        return listOfResult;
    }

    /**
     * set value of listOfResult property.
     *
     * @param listOfResult
     */
    public void setListOfResult(List<SearchResult> listOfResult) {
        this.listOfResult = listOfResult;
    }

    /**
     * get value of resourceCode property.
     *
     * @return
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * set value of resourceCode property.
     *
     * @param resourceCode
     */
    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    /**
     * get value of userId property.
     *
     * @return
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * set value of userId property.
     *
     * @param userId
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * get value of pageNo property.
     *
     * @return
     */
    public String getPageNo() {
        return pageNo;
    }

    /**
     * set value of pageNo property.
     *
     * @param pageNo
     */
    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * get value of pageWin property.
     *
     * @return
     */
    public String getPageWin() {
        return pageWin;
    }

    /**
     * set value of pageWin property.
     *
     * @param pageWin
     */
    public void setPageWin(String pageWin) {
        this.pageWin = pageWin;
    }

    /**
     * get value of languages property.
     *
     * @return
     */
    public List<String> getLanguages() {
        return languages;
    }

    /**
     * set value of languages property.
     *
     * @param languages
     */
    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    /**
     * get value of status property.
     *
     * @return
     */
    public CurationStatus getStatus() {
        return status;
    }

    /**
     * set value of status property.
     *
     * @param status
     */
    public void setStatus(CurationStatus status) {
        this.status = status;
    }
}
