package in.gov.nvli.search.form;

import java.io.Serializable;

/**
 * Wrapper that act as holder of assigned Count and curated Count.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@SuppressWarnings("serial")
public class CurationCount implements Serializable {

    /**
     * count of assigned count. Assigned count will be always greater than or
     * equal to curated count.
     */
    private Long assignedCount;
    /**
     * count of curated records
     */
    private Long curatedCount;

    /**
     * get value of assignedCount property.
     *
     * @return
     */
    public Long getAssignedCount() {
        return assignedCount;
    }

    /**
     * set value of assignedCount property.
     *
     * @param assignedCount
     */
    public void setAssignedCount(Long assignedCount) {
        this.assignedCount = assignedCount;
    }

    /**
     * get value of curatedCount property.
     *
     * @return
     */
    public Long getCuratedCount() {
        return curatedCount;
    }

    /**
     * set value of curatedCount property.
     *
     * @param curatedCount
     */
    public void setCuratedCount(Long curatedCount) {
        this.curatedCount = curatedCount;
    }

}
