package in.gov.nvli.search.form;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Class that act as search result holder.
 *
 * @author Hemant
 * @since 1
 * @version 1
 */
public class SearchResult implements Serializable {

    /**
     * recordIdentifier
     */
    private String recordIdentifier;
    /**
     * list of identifier
     */
    private List<String> identifier;
    /**
     * list of title
     */
    private List<String> title;
    /**
     * list of description
     */
    private List<String> description;
    /**
     * list of data
     */
    private List<String> data;
    /**
     * list of creator
     */
    private List<String> creator;
    /**
     * list of publisher
     */
    private List<String> publisher;
    /**
     * list of language
     */
    private List<String> language;
    /**
     * list of subject
     */
    private List<String> subject;
    /**
     * list of source
     */
    private List<String> source;
    /**
     * metaType
     */
    private String metaType;
    /**
     * list of contributor
     */
    private List<String> contributor;
    /**
     * list of udcNotation
     */
    private List<String> udcNotation;
    /**
     * list of type
     */
    private List<String> type;
    /**
     * recordLikeCount
     */
    private long recordLikeCount;
    /**
     * recordViewCount
     */
    private long recordViewCount;
    /**
     * recordNoOfDownloads
     */
    private long recordNoOfDownloads;
    /**
     * ratingCount
     */
    private long ratingCount;
    /**
     * recordNoOfShares
     */
    private long recordNoOfShares;
    /**
     * recordNoOfReview
     */
    private long recordNoOfReview;
    /**
     * crowdSourceFlag
     */
    private boolean crowdSourceFlag;
    /**
     * crowdSourceStageFlag
     */
    private boolean crowdSourceStageFlag;
    /**
     * updatedOn
     */
    private Date updatedOn;
    /**
     * list of customNotation
     */
    private List<String> customNotation;
    /**
     * ratioOfLikeView
     */
    private double ratioOfLikeView;
    /**
     * subResourceIdentifier
     */
    private String subResourceIdentifier;
    /**
     * resourceType
     */
    private String resourceType;
    /**
     * imagePath
     */
    private String imagePath;
    /**
     * nameToView
     */
    private String nameToView;
    /**
     * defaultScore
     */
    private float defaultScore;
    /**
     * nvliScore
     */
    private float nvliScore = 0;
    /**
     * ratingScore
     */
    private float ratingScore;

    /**
     * get value of ratingScore property.
     *
     * @return
     */
    public float getRatingScore() {
        return ratingScore;
    }

    /**
     * set value of ratingScore property.
     *
     * @param ratingScore
     */
    public void setRatingScore(float ratingScore) {
        this.ratingScore = ratingScore;
    }

    /**
     * get value of nvliScore property.
     *
     * @return
     */
    public float getNvliScore() {
        return nvliScore;
    }

    /**
     * set value of nvliScore property.
     *
     * @param nvliScore
     */
    public void setNvliScore(float nvliScore) {
        this.nvliScore = nvliScore;
    }

    /**
     * get value of defaultScore property.
     *
     * @return
     */
    public float getDefaultScore() {
        return defaultScore;
    }

    /**
     * set value of defaultScore property.
     *
     * @param defaultScore
     */
    public void setDefaultScore(float defaultScore) {
        this.defaultScore = defaultScore;
    }

    /**
     * get value of nameToView property.
     *
     * @return
     */
    public String getNameToView() {
        return nameToView;
    }

    /**
     * set value of nameToView property.
     *
     * @param nameToView
     */
    public void setNameToView(String nameToView) {
        this.nameToView = nameToView;
    }

    /**
     * get value of recordNoOfReview property.
     *
     * @return
     */
    public long getRecordNoOfReview() {
        return recordNoOfReview;
    }

    /**
     * set value of recordNoOfReview property.
     *
     * @param recordNoOfReview
     */
    public void setRecordNoOfReview(long recordNoOfReview) {
        this.recordNoOfReview = recordNoOfReview;
    }

    /**
     * get value of imagePath property.
     *
     * @return
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * set value of imagePath property.
     *
     * @param imagePath
     */
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    /**
     * get value of ratingCount property.
     *
     * @return
     */
    public long getRatingCount() {
        return ratingCount;
    }

    /**
     * set value of ratingCount property.
     *
     * @param ratingCount
     */
    public void setRatingCount(long ratingCount) {
        this.ratingCount = ratingCount;
    }

    /**
     * get value of recordNoOfShares property.
     *
     * @return
     */
    public long getRecordNoOfShares() {
        return recordNoOfShares;
    }

    /**
     * set value of recordNoOfShares property.
     *
     * @param recordNoOfShares
     */
    public void setRecordNoOfShares(long recordNoOfShares) {
        this.recordNoOfShares = recordNoOfShares;
    }

    /**
     * get value of resourceType property.
     *
     * @return
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * set value of resourceType property.
     *
     * @param resourceType
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * get value of subResourceIdentifier property.
     *
     * @return
     */
    public String getSubResourceIdentifier() {
        return subResourceIdentifier;
    }

    /**
     * set value of subResourceIdentifier property.
     *
     * @param subResourceIdentifier
     */
    public void setSubResourceIdentifier(String subResourceIdentifier) {
        this.subResourceIdentifier = subResourceIdentifier;
    }

    /**
     * get value of ratioOfLikeView property.
     *
     * @return
     */
    public double getRatioOfLikeView() {
        return ratioOfLikeView;
    }

    /**
     * set value of ratioOfLikeView property.
     *
     * @param ratioOfLikeView
     */
    public void setRatioOfLikeView(double ratioOfLikeView) {
        this.ratioOfLikeView = ratioOfLikeView;
    }

    /**
     * get value of customNotation property.
     *
     * @return
     */
    public List<String> getCustomNotation() {
        return customNotation;
    }

    /**
     * set value of customNotation property.
     *
     * @param customNotation
     */
    public void setCustomNotation(List<String> customNotation) {
        this.customNotation = customNotation;
    }

    /**
     * get value of crowdSourceFlag property.
     *
     * @return
     */
    public boolean isCrowdSourceFlag() {
        return crowdSourceFlag;
    }

    /**
     * set value of crowdSourceFlag property.
     *
     * @param crowdSourceFlag
     */
    public void setCrowdSourceFlag(boolean crowdSourceFlag) {
        this.crowdSourceFlag = crowdSourceFlag;
    }

    /**
     * get value of crowdSourceStageFlag property.
     *
     * @return
     */
    public boolean isCrowdSourceStageFlag() {
        return crowdSourceStageFlag;
    }

    /**
     * set value of crowdSourceStageFlag property.
     *
     * @param crowdSourceStageFlag
     */
    public void setCrowdSourceStageFlag(boolean crowdSourceStageFlag) {
        this.crowdSourceStageFlag = crowdSourceStageFlag;
    }

    /**
     * get value of recordLikeCount property.
     *
     * @return
     */
    public long getRecordLikeCount() {
        return recordLikeCount;
    }

    /**
     * set value of recordLikeCount property.
     *
     * @param recordLikeCount
     */
    public void setRecordLikeCount(long recordLikeCount) {
        this.recordLikeCount = recordLikeCount;
    }

    /**
     * get value of recordNoOfDownloads property.
     *
     * @return
     */
    public long getRecordNoOfDownloads() {
        return recordNoOfDownloads;
    }

    /**
     * set value of recordNoOfDownloads property.
     *
     * @param recordNoOfDownloads
     */
    public void setRecordNoOfDownloads(long recordNoOfDownloads) {
        this.recordNoOfDownloads = recordNoOfDownloads;
    }

    /**
     * get value of recordViewCount property.
     *
     * @return
     */
    public long getRecordViewCount() {
        return recordViewCount;
    }

    /**
     * set value of recordViewCount property.
     *
     * @param recordViewCount
     */
    public void setRecordViewCount(long recordViewCount) {
        this.recordViewCount = recordViewCount;
    }

    /**
     * get value of updatedOn property.
     *
     * @return
     */
    public Date getUpdatedOn() {
        return updatedOn;
    }

    /**
     * set value of updatedOn property.
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * get value of contributor property.
     *
     * @return
     */
    public List<String> getContributor() {
        return contributor;
    }

    /**
     * set value of contributor property.
     *
     * @param contributor
     */
    public void setContributor(List<String> contributor) {
        this.contributor = contributor;
    }

    /**
     * get value of creator property.
     *
     * @return
     */
    public List<String> getCreator() {
        return creator;
    }

    /**
     * set value of creator property.
     *
     * @param creator
     */
    public void setCreator(List<String> creator) {
        this.creator = creator;
    }

    /**
     * get value of data property.
     *
     * @return
     */
    public List<String> getData() {
        return data;
    }

    /**
     * set value of data property.
     *
     * @param data
     */
    public void setData(List<String> data) {
        this.data = data;
    }

    /**
     * get value of description property.
     *
     * @return
     */
    public List<String> getDescription() {
        return description;
    }

    /**
     * set value of description property.
     *
     * @param description
     */
    public void setDescription(List<String> description) {
        this.description = description;
    }

    /**
     * get value of identifier property.
     *
     * @return
     */
    public List<String> getIdentifier() {
        return identifier;
    }

    /**
     * set value of identifier property.
     *
     * @param identifier
     */
    public void setIdentifier(List<String> identifier) {
        this.identifier = identifier;
    }

    /**
     * get value of language property.
     *
     * @return
     */
    public List<String> getLanguage() {
        return language;
    }

    /**
     * set value of language property.
     *
     * @param language
     */
    public void setLanguage(List<String> language) {
        this.language = language;
    }

    /**
     * get value of metaType property.
     *
     * @return
     */
    public String getMetaType() {
        return metaType;
    }

    /**
     * set value of metaType property.
     *
     * @param metaType
     */
    public void setMetaType(String metaType) {
        this.metaType = metaType;
    }

    /**
     * get value of publisher property.
     *
     * @return
     */
    public List<String> getPublisher() {
        return publisher;
    }

    /**
     * set value of publisher property.
     *
     * @param publisher
     */
    public void setPublisher(List<String> publisher) {
        this.publisher = publisher;
    }

    /**
     * get value of recordIdentifier property.
     *
     * @return
     */
    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    /**
     * set value of recordIdentifier property.
     *
     * @param recordIdentifier
     */
    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    /**
     * get value of source property.
     *
     * @return
     */
    public List<String> getSource() {
        return source;
    }

    /**
     * set value of source property.
     *
     * @param source
     */
    public void setSource(List<String> source) {
        this.source = source;
    }

    /**
     * get value of subject property.
     *
     * @return
     */
    public List<String> getSubject() {
        return subject;
    }

    /**
     * set value of subject property.
     *
     * @param subject
     */
    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    /**
     * get value of title property.
     *
     * @return
     */
    public List<String> getTitle() {
        return title;
    }

    /**
     * set value of title property.
     *
     * @param title
     */
    public void setTitle(List<String> title) {
        this.title = title;
    }

    /**
     * get value of type property.
     *
     * @return
     */
    public List<String> getType() {
        return type;
    }

    /**
     * set value of type property.
     *
     * @param type
     */
    public void setType(List<String> type) {
        this.type = type;
    }

    /**
     * get value of udcNotation property.
     *
     * @return
     */
    public List<String> getUdcNotation() {
        return udcNotation;
    }

    /**
     * set value of udcNotation property.
     *
     * @param udcNotation
     */
    public void setUdcNotation(List<String> udcNotation) {
        this.udcNotation = udcNotation;
    }
}
