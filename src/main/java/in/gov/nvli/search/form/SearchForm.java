package in.gov.nvli.search.form;

import in.gov.nvli.search.util.FacetNameCountHolder;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Class that act as Search Form
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 * @see SearchResult
 */
public class SearchForm implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * search element
     */
    private String searchElement;
    /**
     * list of SearchResult
     */
    private List<SearchResult> listOfResult;
    /**
     * list of subResourceIdentifiers
     */
    private List<String> subResourceIdentifiers;
    /**
     * size of result
     */
    private Long resultSize;
    /**
     * result found or not
     */
    private boolean resultFound;
    /**
     * for holding faceting result
     */
    private Map<String, List<FacetNameCountHolder>> facetMap;
    /**
     * for holding faceting result
     */
    private Map<String, List<String>> filters;
    /**
     * all resources faceting
     */
    private Map<String, Long> allResources;
    /**
     * resource Code
     */
    private String resourceCode;

    /**
     * for holding specific faceting result
     */
    private Map<String, List<FacetNameCountHolder>> specificFacetMap;
    /**
     * for holding specific faceting filter result
     */
    private Map<String, List<String>> specificFilters;
    /**
     * for which ranking rule to apply
     */
    private String rankingRule;

    /**
     * To hold search is basic or advance
     */
    private SearchComplexityEnum searchComplexity;
    /**
     * advance search string
     */
    private String advanceSearchQueryString;

    /**
     * From date in string format
     */
    private String fromDate;
    /**
     * To date in string format
     */
    private String toDate;

    /**
     * get value of fromDate property.
     *
     * @return
     */
    public String getFromDate() {
        return fromDate;
    }

    /**
     * set value of fromDate property.
     *
     * @param fromDate
     */
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * get value of toDate property.
     *
     * @return
     */
    public String getToDate() {
        return toDate;
    }

    /**
     * set value of toDate property.
     *
     * @param toDate
     */
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    /**
     * get value of searchComplexity property.
     *
     * @return
     */
    public SearchComplexityEnum getSearchComplexity() {
        return searchComplexity;
    }

    /**
     * set value of searchComplexity property.
     *
     * @param searchComplexity
     */
    public void setSearchComplexity(SearchComplexityEnum searchComplexity) {
        this.searchComplexity = searchComplexity;
    }

    /**
     * get value of advanceSearchQueryString property.
     *
     * @return
     */
    public String getAdvanceSearchQueryString() {
        return advanceSearchQueryString;
    }

    /**
     * set value of advanceSearchQueryString property.
     *
     * @param advanceSearchQueryString
     */
    public void setAdvanceSearchQueryString(String advanceSearchQueryString) {
        this.advanceSearchQueryString = advanceSearchQueryString;
    }

    /**
     * get value of rankingRule property.
     *
     * @return
     */
    public String getRankingRule() {
        return rankingRule;
    }

    /**
     * set value of rankingRule property.
     *
     * @param rankingRule
     */
    public void setRankingRule(String rankingRule) {
        this.rankingRule = rankingRule;
    }

    /**
     * get value of specificFacetMap property.
     *
     * @return
     */
    public Map<String, List<FacetNameCountHolder>> getSpecificFacetMap() {
        return specificFacetMap;
    }

    /**
     * set value of specificFacetMap property.
     *
     * @param specificFacetMap
     */
    public void setSpecificFacetMap(Map<String, List<FacetNameCountHolder>> specificFacetMap) {
        this.specificFacetMap = specificFacetMap;
    }

    /**
     * get value of specificFilters property.
     *
     * @return
     */
    public Map<String, List<String>> getSpecificFilters() {
        return specificFilters;
    }

    /**
     * set value of specificFilters property.
     *
     * @param specificFilters
     */
    public void setSpecificFilters(Map<String, List<String>> specificFilters) {
        this.specificFilters = specificFilters;
    }

    /**
     * get value of searchElement property.
     *
     * @return
     */
    public String getSearchElement() {
        return searchElement;
    }

    /**
     * set value of searchElement property.
     *
     * @param searchElement
     */
    public void setSearchElement(String searchElement) {
        this.searchElement = searchElement;
    }

    /**
     * get value of listOfResult property.
     *
     * @return
     */
    public List<SearchResult> getListOfResult() {
        return listOfResult;
    }

    /**
     * set value of listOfResult property.
     *
     * @param listOfResult
     */
    public void setListOfResult(List<SearchResult> listOfResult) {
        this.listOfResult = listOfResult;
    }

    /**
     * get value of subResourceIdentifiers property.
     *
     * @return
     */
    public List<String> getSubResourceIdentifiers() {
        return subResourceIdentifiers;
    }

    /**
     * set value of subResourceIdentifiers property.
     *
     * @param subResourceIdentifiers
     */
    public void setSubResourceIdentifiers(List<String> subResourceIdentifiers) {
        this.subResourceIdentifiers = subResourceIdentifiers;
    }

    /**
     * get value of resultSize property.
     *
     * @return
     */
    public Long getResultSize() {
        return resultSize;
    }

    /**
     * set value of resultSize property.
     *
     * @param resultSize
     */
    public void setResultSize(Long resultSize) {
        this.resultSize = resultSize;
    }

    /**
     * get value of resultFound property.
     *
     * @return
     */
    public boolean isResultFound() {
        return resultFound;
    }

    /**
     * set value of resultFound property.
     *
     * @param resultFound
     */
    public void setResultFound(boolean resultFound) {
        this.resultFound = resultFound;
    }

    /**
     * get value of facetMap property.
     *
     * @return
     */
    public Map<String, List<FacetNameCountHolder>> getFacetMap() {
        return facetMap;
    }

    /**
     * set value of facetMap property.
     *
     * @param facetMap
     */
    public void setFacetMap(Map<String, List<FacetNameCountHolder>> facetMap) {
        this.facetMap = facetMap;
    }

    /**
     * get value of filters property.
     *
     * @return
     */
    public Map<String, List<String>> getFilters() {
        return filters;
    }

    /**
     * set value of filters property.
     *
     * @param filters
     */
    public void setFilters(Map<String, List<String>> filters) {
        this.filters = filters;
    }

    /**
     * get value of allResources property.
     *
     * @return
     */
    public Map<String, Long> getAllResources() {
        return allResources;
    }

    /**
     * set value of allResources property.
     *
     * @param allResources
     */
    public void setAllResources(Map<String, Long> allResources) {
        this.allResources = allResources;
    }

    /**
     * get value of resourceCode property.
     *
     * @return
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * set value of resourceCode property.
     *
     * @param resourceCode
     */
    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

}
