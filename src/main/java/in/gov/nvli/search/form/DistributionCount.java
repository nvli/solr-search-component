package in.gov.nvli.search.form;

import java.io.Serializable;
import java.util.Objects;

/**
 * Form to communicate b/w search component and mit webservice. Wrapper to hold
 * subResourceCode, totalCount, count.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@SuppressWarnings("serial")
public class DistributionCount implements Serializable {

    /**
     * subResourceCode
     */
    private String subResourceCode;
    /**
     * total count of records for given subResourceCode.
     */
    private Long totalCount;
    /**
     * count of undistributed records of given sub-resource code.
     */
    private Long count;

    /**
     * get value of subResourceCode property.
     *
     * @return
     */
    public String getSubResourceCode() {
        return subResourceCode;
    }

    /**
     * set value of subResourceCode property.
     *
     * @param subResourceCode
     */
    public void setSubResourceCode(String subResourceCode) {
        this.subResourceCode = subResourceCode;
    }

    /**
     * get value of totalCount property.
     *
     * @return
     */
    public Long getTotalCount() {
        return totalCount;
    }

    /**
     * set value of totalCount property.
     *
     * @param totalCount
     */
    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * get value of count property.
     *
     * @return
     */
    public Long getCount() {
        return count;
    }

    /**
     * set value of count property.
     *
     * @param count
     */
    public void setCount(Long count) {
        this.count = count;
    }

    /**
     * generate hash code of object based on subResourceCode property.
     *
     * @return hash code of object.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.subResourceCode);
        return hash;
    }

    /**
     * check equality of object based on subResourceCode property.
     *
     * @param obj
     * @return true if equals else false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DistributionCount other = (DistributionCount) obj;
        if (!Objects.equals(this.subResourceCode, other.subResourceCode)) {
            return false;
        }
        return true;
    }
}
