package in.gov.nvli.search.form;

/**
 * Enum to hold curation status for record.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public enum CurationStatus {

    /**
     * Curation status CURATED
     */
    CURATED,
    /**
     * Curation status UNCURATED
     */
    UNCURATED;
}
