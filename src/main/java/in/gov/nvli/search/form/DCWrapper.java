package in.gov.nvli.search.form;

import in.gov.nvli.search.domain.DCMetadataInformation;

/**
 * Class is used for taking the score of each DC document stored in Index.
 *
 * @author Saurabh Koriya
 * @since 1
 * @version 1
 */
public class DCWrapper {

    /**
     * score of document
     */
    private float __HSearch_Score;
    /**
     * original MARC21 objects.
     */
    private DCMetadataInformation __HSearch_This;

    /**
     * get value of __HSearch_Score property.
     *
     * @return
     */
    public float getHSearch_Score() {
        return __HSearch_Score;
    }

    /**
     * set value of __HSearch_Score property.
     *
     * @param __HSearch_Score
     */
    public void setHSearch_Score(float __HSearch_Score) {
        this.__HSearch_Score = __HSearch_Score;
    }

    /**
     * get value of __HSearch_This property.
     *
     * @return
     */
    public DCMetadataInformation getHSearch_This() {
        return __HSearch_This;
    }

    /**
     * set value of __HSearch_This property.
     *
     * @param __HSearch_This
     */
    public void setHSearch_This(DCMetadataInformation __HSearch_This) {
        this.__HSearch_This = __HSearch_This;
    }
}
