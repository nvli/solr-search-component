package in.gov.nvli.search.form;

/**
 * Enum to hold constant related for search.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public enum SearchComplexityEnum {

    /**
     * For Basic search
     */
    BASIC,
    /**
     * For advance search
     */
    ADVANCED;
}
