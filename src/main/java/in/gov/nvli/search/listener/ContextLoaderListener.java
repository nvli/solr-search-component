package in.gov.nvli.search.listener;

import in.gov.nvli.search.dao.*;
import in.gov.nvli.search.domain.*;
import in.gov.nvli.search.util.Helper;
import java.util.List;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Listener that run at time Context Loading.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 */
public class ContextLoaderListener implements ServletContextListener {

    /**
     * Log4j reference
     */
    private static final Logger logger = Logger.getLogger(ContextLoaderListener.class.getName());
    /**
     * Reference of WebApplicationContext.
     */
    private WebApplicationContext springContext;
    /**
     * Instance of IResourceTypeInfoDAO
     */
    private IResourceTypeInfoDAO resourceTypeInfoDAO;
    /**
     * Instance of IMappingInformationDao
     */
    private IMappingInformationDao mappingInformationDAO;
    /**
     * method called automatically at time context initializing. Does initialize
     * System prerequisites, Create index, load mapping information in map
     * ({@link Helper.keyValueMappings} and {@link Helper.valueKeyMappings).
     *
     * @param sce
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        springContext = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        resourceTypeInfoDAO = springContext.getBean(IResourceTypeInfoDAO.class);
        mappingInformationDAO = springContext.getBean(IMappingInformationDao.class);
        //inti all cdn related properties
        InitializeSystemPrerequisites initializeSystemPrerequisites = springContext.getBean(InitializeSystemPrerequisites.class);
        try {
            initializeSystemPrerequisites.initDefault();
        } catch (Exception ex) {
            logger.error(ex, ex);
        }
        //checking DC records and MARC21 records
        try {
            /**
             * setting resouce code once so that it can be used in every time.
             */
            ResourceTypeInfo resourceInformation = resourceTypeInfoDAO.list().get(0);
            Helper.RESOURCE_TYPE_CODE = resourceInformation.getResourceCode();
            Helper.RESOURCE_TYPE_NAME = resourceInformation.getResourceType();
            /**
             * setting the mapping fields
             */
            List<MappingInformation> mappingList = mappingInformationDAO.list();
            for (MappingInformation mappingInformation : mappingList) {
                Helper.keyValueMappings.put(mappingInformation.getOriginalFieldName(), mappingInformation.getReferenceName());
                Helper.valueKeyMappings.put(mappingInformation.getReferenceName(), mappingInformation.getOriginalFieldName());
            }
        } catch (Exception ex) {
            logger.error(ex, ex);
            ex.printStackTrace();
        }
    }

    /**
     * method called automatically at time context destroyed
     *
     * @param sce
     */
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
