package in.gov.nvli.search.listener;

import in.gov.nvli.search.util.Helper;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * class act as Component that Initialize System Prerequisites.
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 * @version 1
 * @since 1
 */
@Component
public class InitializeSystemPrerequisites {

    /**
     * location where index is created. It value will populated from Property
     * file. (@see WEB-INF/etc/conf/dispatcher-servlet.xml)
     */
    @Value("${nvli.index.folder}")
    private String indexFolder;

    /**
     * register CDN URls from cdn-setting.properties. And use it from map when
     * it is required.
     * @throws java.lang.Exception
     */
    public void initDefault() throws Exception {
        Properties cdnProperties = loadCDNProperties();
        List<String> cdnList = new ArrayList<>();
        for (final String name : cdnProperties.stringPropertyNames()) {
            cdnList.add(cdnProperties.getProperty(name));
        }
        Helper.LIST_CDN = cdnList;

    }

    /**
     * Loads properties form CDN-setting.properties.
     *
     * @return
     * @throws Exception
     */
    public Properties loadCDNProperties() throws Exception {
        Properties prop = null;
        InputStream input = null;
        String homeDir = System.getProperty("user.home");
        File config = new File(homeDir + File.separator + indexFolder + File.separator + "conf" + File.separator + "cdn-setting.properties");
        prop = new Properties();
        input = new FileInputStream(config);
        // load a properties file
        prop.load(input);
        //close the stream
        input.close();
        return prop;
    }
}
