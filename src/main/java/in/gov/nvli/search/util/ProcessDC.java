package in.gov.nvli.search.util;

import in.gov.nvli.search.dao.IDCMetadataInformationDAO;
import in.gov.nvli.search.domain.DCMetadataInformation;
import java.util.concurrent.Callable;

/**
 * Callable Task for processing DCMetadataInformation
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public class ProcessDC implements Callable<Void> {

    /**
     * Array of DCMetadataInformation.
     */
    private final DCMetadataInformation[] totalRecords;
    /**
     * start index to process in totalRecords[]
     */
    private final double from;
    /**
     * end index to process in totalRecords[]
     */
    private final double to;
    /**
     * user id to whom record need to assign
     */
    private final Long userId;
    /**
     * Reference of IDCMetadataInformationDAO
     */
    private final IDCMetadataInformationDAO dCMetadataInformationDAO;

    /**
     * Argument Contructor.
     *
     * @param totalRecords
     * @param from start index to process in totalRecords[]
     * @param to end index to process in totalRecords[]
     * @param userId user id to whom record need to assign
     * @param dCMetadataInformationDAO Reference of IDCMetadataInformationDAO
     */
    public ProcessDC(DCMetadataInformation[] totalRecords, double from, double to, Long userId, IDCMetadataInformationDAO dCMetadataInformationDAO) {
        this.totalRecords = totalRecords;
        this.from = from;
        this.to = to;
        this.userId = userId;
        this.dCMetadataInformationDAO = dCMetadataInformationDAO;
    }

    /**
     * call method to perform task by worker
     *
     * @return
     * @throws Exception
     */
    @Override
    public Void call() throws Exception {

        for (double i = from; i <= to; i++) {
            int index = (int) i - 1;
            DCMetadataInformation r = totalRecords[index];
            r.setAssignedTo(userId);
            dCMetadataInformationDAO.merge(r);
        }
        return null;
    }

}
