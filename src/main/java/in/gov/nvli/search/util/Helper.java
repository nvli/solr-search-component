package in.gov.nvli.search.util;

import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.form.SearchResult;
import in.gov.nvli.search.solr.authentication.SolrPreemptiveAuthentication;
import in.gov.nvli.search.solr.domain.SolrDCMetadataInformation;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.impl.HttpSolrClient;

/**
 * Helper Class containing all static methods.
 *
 * @author Saurabh koriya
 * @version 1
 * @since 1
 *
 */
public class Helper {

    /**
     * Logger reference
     */
    private static Logger logger = Logger.getLogger(Helper.class.getName());

    /**
     * Gets the current timestamp in SQL format.
     *
     * @return java.sql.Timestamp
     * @since 1
     */
    public static Timestamp rightNow() {
        return new Timestamp(new java.util.Date().getTime());
    }
    /**
     * Hold value of resource code for component. its value will be set once at
     * time of context loading
     */
    public static String RESOURCE_TYPE_CODE;
    /**
     * Hold value of resource Name for component. its value will be set once at
     * time of context loading
     */
    public static String RESOURCE_TYPE_NAME;
    /**
     * Hold the mappings fields as key -> value
     */
    public static Map<String, String> keyValueMappings = new LinkedHashMap<>();
    /**
     * Hold the mappings fields as value -> key
     */
    public static Map<String, String> valueKeyMappings = new LinkedHashMap<>();
    /**
     * CDN URls List
     */
    public static List<String> LIST_CDN;

    /**
     * give object of SearchResult from DCWrapper.
     *
     * @param dCWrapper
     * @param resourceTypeCode
     * @param imageServerPath
     * @param rulesToApply
     * @param ncaaBaseURL
     * @param youtubeURL
     * @return object of SearchResult if dc is not null else null.
     */
    public static SearchResult createSearchResultFromDC(SolrDCMetadataInformation dc, final String resourceTypeCode, final String imageServerPath, final String rulesToApply, final String ncaaBaseURL, final String youtubeURL) {
        SearchResult searchResult = null;
        if (dc != null) {
            String files[] = null;
            String fileNames = dc.getOriginalFileName();
            if (fileNames != null && !fileNames.isEmpty()) {
                files = fileNames.split(Constants.REMOVE_VALUE_SEPARATOR);
            }
            searchResult = new SearchResult();
            searchResult.setRecordIdentifier(dc.getRecordIdentifier().trim());
            searchResult.setIdentifier(getFormattedList(dc.getIdentifier()));
            searchResult.setTitle(getFormattedList(dc.getTitle()));
            searchResult.setDescription(removeDescriptionTag(getFormattedList(dc.getDescription())));
            searchResult.setData(getFormattedList(dc.getDate()));
            searchResult.setCreator(getFormattedList(dc.getCreator()));
            searchResult.setPublisher(getFormattedList(dc.getPublisher()));
            searchResult.setLanguage(getFormattedList(dc.getLanguages()));
            searchResult.setSubject(getFormattedList(dc.getSubject()));
            searchResult.setSource(getFormattedList(dc.getSource()));
            searchResult.setType(getFormattedList(dc.getType()));
            searchResult.setContributor(getFormattedList(dc.getContributor()));
            searchResult.setUdcNotation(getFormattedList(dc.getUdcNotation()));

            searchResult.setUpdatedOn(dc.getUpdatedOn());
            searchResult.setCustomNotation(getFormattedList(dc.getCustomNotation()));
            searchResult.setRecordLikeCount(dc.getRecordLikeCount());
            searchResult.setRecordViewCount(dc.getRecordViewCount());
            searchResult.setRecordNoOfDownloads(dc.getRecordNoOfDownloads());
            searchResult.setCrowdSourceFlag(dc.isCrowdSourceFlag());
            searchResult.setCrowdSourceStageFlag(dc.isCrowdSourceStageFlag());
            searchResult.setRatioOfLikeView(findRatio(dc.getRecordLikeCount(), dc.getRecordViewCount()));
            searchResult.setSubResourceIdentifier(dc.getSubResourceIdentifier());
            searchResult.setResourceType(resourceTypeCode);
            searchResult.setMetaType(Constants.DUBLIN_CORE);
            searchResult.setRatingCount(dc.getRatingCount());
            searchResult.setRecordNoOfShares(dc.getRecordNoOfShares());
            searchResult.setRecordNoOfReview(dc.getRecordNoOfReview());
            searchResult.setNameToView(getViewName(searchResult));
            searchResult.setDefaultScore((float)dc.getScore());
            searchResult.setRatingScore(dc.getRatingScore());
            if (dc.getOriginalFileName() != null) {
                searchResult.setImagePath(imageServerPath + dc.getLocation().replace("\\", "/") + "/" + dc.getOriginalFileName());
            } else {
                searchResult.setImagePath(null);
            }
            if (Constants.AUDIO_VISUAL.equalsIgnoreCase(resourceTypeCode)) {
                //CCRT local video avaliable 
                if (dc.isLocalObjectAvailable()) {
                    searchResult.setImagePath(imageServerPath + dc.getLocation().replace("\\", "/") + "/" + dc.getOriginalFileName());
                } else if (files != null) {
                    searchResult.setImagePath(ncaaBaseURL + files[0].replace("\\", "/"));
                } else {
                    searchResult.setImagePath(null);
                }
            } else if (Constants.NPTEL.equalsIgnoreCase(resourceTypeCode)) {
                if (dc.getLocation() != null && !dc.getLocation().isEmpty()) {
                    searchResult.setImagePath(youtubeURL + dc.getLocation());
                } else {
                    searchResult.setImagePath(null);
                }
            }
            float score = 0;
            if (Constants.NVLI_RL_1.equalsIgnoreCase(rulesToApply)) {
                //setting default
                searchResult.setNvliScore((float)dc.getScore());
            } else if (Constants.NVLI_RL_2.equalsIgnoreCase(rulesToApply)) {
                //setting NVLI ranking if UDC Tag is their set 1 then 0
                List<String> udcList = searchResult.getUdcNotation();
                if (udcList != null && !udcList.isEmpty()) {
                    score += 1;
                } else {
                    score += 0;
                }
                searchResult.setNvliScore(score);
            } else if (Constants.NVLI_RL_3.equalsIgnoreCase(rulesToApply)) {
                //setting NVLI ranking if UDC  Tag is their set 1 then 0
                List<String> udcList = searchResult.getUdcNotation();
                if (udcList != null && !udcList.isEmpty()) {
                    score += 1;
                } else {
                    score += 0;
                }
                //setting NVLI ranking if Custom Tag is their set 1 then 0
                List<String> customList = searchResult.getCustomNotation();
                if (customList != null && !customList.isEmpty()) {
                    score += 1;
                } else {
                    score += 0;
                }
                searchResult.setNvliScore(score);
            } else if (Constants.NVLI_RL_4.equalsIgnoreCase(rulesToApply)) {
                //setting NVLI ranking if UDC Tag is their set 1 then 0
                List<String> udcList = searchResult.getUdcNotation();
                if (udcList != null && !udcList.isEmpty()) {
                    score += 1;
                } else {
                    score += 0;
                }
                //setting NVLI ranking if Custom Tag is their set 1 then 0
                List<String> customList = searchResult.getCustomNotation();
                if (customList != null && !customList.isEmpty()) {
                    score += 1;
                } else {
                    score += 0;
                }
                //setting NVLI ranking if crowd source approved.
                boolean flag = searchResult.isCrowdSourceFlag();
                if (flag) {
                    score += 1;
                } else {
                    score += 0;
                }
                searchResult.setNvliScore(score);
            } else if (Constants.NVLI_RL_5.equalsIgnoreCase(rulesToApply)) {
                List<String> udcList = searchResult.getUdcNotation();
                if (udcList != null && !udcList.isEmpty()) {
                    score += 1;
                } else {
                    score += 0;
                }
                //setting NVLI ranking if UDC Tag is their set 1 then 0
                List<String> customList = searchResult.getCustomNotation();
                if (customList != null && !customList.isEmpty()) {
                    score += 1;
                } else {
                    score += 0;
                }
                //setting NVLI ranking if crowd source approved.
                boolean flag = searchResult.isCrowdSourceFlag();
                if (flag) {
                    score += 1;
                } else {
                    score += 0;
                }
                searchResult.setNvliScore(score + dc.getFinalScore());
            } else if (Constants.NVLI_RL_6.equalsIgnoreCase(rulesToApply)) {
                searchResult.setNvliScore((float)dc.getScore() + dc.getRatingScore());
            }

        }
        return searchResult;
    }

    /**
     * Method get view name from given searchResult. Checks title, subject and
     * publisher sequentially for view name. And return first string where found
     * first.
     *
     * @param searchResult {@link SearchResult}
     * @return viewName
     */
    public static String getViewName(SearchResult searchResult) {
        String returnValue = "Not Available";
        if (searchResult.getTitle() != null && !searchResult.getTitle().isEmpty()) {
            returnValue = getFirstElement(searchResult.getTitle());
        } else if (searchResult.getSubject() != null && !searchResult.getSubject().isEmpty()) {
            returnValue = getFirstElement(searchResult.getSubject());
        } else if (searchResult.getPublisher() != null && !searchResult.getPublisher().isEmpty()) {
            returnValue = getFirstElement(searchResult.getPublisher());
        } else {
            returnValue = searchResult.getRecordIdentifier();
        }
        return returnValue;
    }

    /**
     * get first element from List of String.
     *
     * @param list List of String.
     * @return String if available else null.
     */
    public static String getFirstElement(List<String> list) {
        String returnValue = null;
        if (list != null && !list.isEmpty()) {
            returnValue = list.get(0);
        }
        return returnValue;
    }

    /**
     * Method give formatted list from given string. As we are keeping dc
     * metadata tag as string. in case if same tag is available several time, we
     * keeping as string separated by VALUE_SEPARATOR ('&|&'). So to format Such
     * string, we process such string and generate list of string.
     *
     * @param str
     * @return List of String if argument is not null or not empty else null.
     */
    public static List<String> getFormattedList(String str) {
        if (str == null || str.trim().isEmpty()) {
            return null;
        }
        List<String> list = Arrays.asList(str.split(Constants.REMOVE_VALUE_SEPARATOR));
        List<String> formattedList = new ArrayList<>();
        for (String s : list) {
            if (!s.trim().isEmpty()) {
                formattedList.add(s.trim().replaceAll("(\\r\\n|\\n)", ""));
            }
        }
        if (formattedList.isEmpty()) {
            return null;
        }
        return formattedList;
    }

    /**
     * finding the ratio of like count and view count.
     *
     * @param likeCount
     * @param viewCount
     * @return
     */
    public static double findRatio(long likeCount, long viewCount) {
        double returnValue = 0.0;
        try {
            double ratio = likeCount / (double) viewCount;
            if (Double.isNaN(ratio)) {
                return 0.0000;
            }
            DecimalFormat ratioFormat = new DecimalFormat("#.####");
            returnValue = Double.parseDouble(ratioFormat.format(ratio));
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return returnValue;
    }

    /**
     * Method remove following string ('Description::-',
     * 'Ingredients::-','Method::-') from given list of string
     *
     * @param strList
     * @return list of string after removing given terms.
     */
    public static List<String> removeDescriptionTag(List<String> strList) {
        if (strList == null) {
            return null;
        }
        List<String> formattedList = new ArrayList<>();
        for (String s : strList) {
            s = s.replaceFirst("Description::-", "").replaceFirst("Ingredients::-", "").replaceFirst("Method::-", "");
            formattedList.add(s);
        }
        return formattedList;
    }

    /**
     * factory method for creating {@link SearchResult} from
     * {@link DCMetadataInformation}
     *
     * @param dc
     * @return {@link SearchResult } object if dc is not null else null.
     */
    public static SearchResult createCurationSearchResultFromDC(DCMetadataInformation dc) {
        SearchResult searchResult = null;
        if (dc != null) {
            searchResult = new SearchResult();
            searchResult.setRecordIdentifier(dc.getRecordIdentifier().trim());
            searchResult.setTitle(getFormattedList(dc.getTitle()));
            searchResult.setPublisher(getFormattedList(dc.getPublisher()));
            searchResult.setSubject(getFormattedList(dc.getSubject()));
            searchResult.setNameToView(getViewName(searchResult));
        }
        return searchResult;
    }

    /**
     * Getting CDN Path
     *
     * @return
     */
    public static String getCDNPath() {
        return Helper.LIST_CDN.get(getRandomNumberInRange(0, Helper.LIST_CDN.size() - 1));
    }

    /**
     * Getting random number in between 0-3.
     *
     * @param min
     * @param max
     * @return
     */
    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
    /**
     * Getting SOLR session with username and password.  
     * @param solrUrl
     * @param solrUserName
     * @param solrPassword
     * @return 
     */
    public static HttpSolrClient getSolrSession(String solrUrl,String solrUserName,String solrPassword){
        HttpSolrClient solr = new HttpSolrClient.Builder(solrUrl).withHttpClient(SolrPreemptiveAuthentication.createHttpClient(solrUrl, solrUserName, solrPassword)).build();
        return solr;
    }
     /**
     * Getting SolrDCDocument
     * @param dc
     * @param dcSolrObject 
     * @param newOrUpdate 
     * @return 
     */
    public static SolrDCMetadataInformation gettingSolrDocumentFormDCMetadataInformation(DCMetadataInformation dc,SolrDCMetadataInformation dcSolrObject,boolean newOrUpdate){
        if(newOrUpdate){
            dcSolrObject=new SolrDCMetadataInformation();
        }
        dcSolrObject.setId(dc.getId());
        dcSolrObject.setRecordIdentifier(dc.getRecordIdentifier());
        dcSolrObject.setOriginalFileName(dc.getOriginalFileName());
        dcSolrObject.setTitle(dc.getTitle());
        dcSolrObject.setDescription(dc.getDescription());
        dcSolrObject.setRights(dc.getRights());
        dcSolrObject.setDate(dc.getDate());
        dcSolrObject.setFormat(dc.getFormat());
        dcSolrObject.setRelation(dc.getRelation());
        dcSolrObject.setCoverage(dc.getCoverage());
        dcSolrObject.setIdentifier(dc.getIdentifier());
        dcSolrObject.setSubject(dc.getSubject());
        dcSolrObject.setCreator(dc.getCreator());
        dcSolrObject.setPublisher(dc.getPublisher());
        dcSolrObject.setSource(dc.getSource());
        dcSolrObject.setLanguages(dc.getLanguages());
        dcSolrObject.setAccrualMethod(dc.getAccrualMethod());
        dcSolrObject.setAccrualPeriodicity(dc.getAccrualPeriodicity());
        dcSolrObject.setAudience(dc.getAudience());
        dcSolrObject.setProvenance(dc.getProvenance());
        dcSolrObject.setType(dc.getType());
        dcSolrObject.setContributor(dc.getContributor());
        dcSolrObject.setIsbn(dc.getIsbn());
        dcSolrObject.setLccn(dc.getLccn());
        dcSolrObject.setEdition(dc.getEdition());
        dcSolrObject.setIssn(dc.getIssn());
        dcSolrObject.setNotes(dc.getNotes());
        dcSolrObject.setLocation(dc.getLocation());
        dcSolrObject.setUpdatedOn(dc.getUpdatedOn());
        dcSolrObject.setCreatedOn(dc.getCreatedOn());
        dcSolrObject.setJsonView(dc.getJsonView());
        dcSolrObject.setRecordLikeCount(dc.getRecordLikeCount());
        dcSolrObject.setRecordViewCount(dc.getRecordViewCount());
        dcSolrObject.setRecordNoOfDownloads(dc.getRecordNoOfDownloads());
        dcSolrObject.setCrowdSourceFlag(dc.isCrowdSourceFlag());
        dcSolrObject.setCrowdSourceStageFlag(dc.isCrowdSourceStageFlag());
        dcSolrObject.setIndexedFlag(dc.isIndexedFlag());
        dcSolrObject.setRatingCount(dc.getRatingCount());
        dcSolrObject.setRecordNoOfShares(dc.getRecordNoOfShares());
        dcSolrObject.setLinkedWithArticleCount(dc.getLinkedWithArticleCount());
        dcSolrObject.setRatingScore(dc.getRatingScore());
        dcSolrObject.setFinalScore(dc.getFinalScore());
        dcSolrObject.setUdcNotation(dc.getUdcNotation());
        dcSolrObject.setCustomNotation(dc.getCustomNotation());
        dcSolrObject.setRootElement(dc.getRootElement());
        dcSolrObject.setPrefix(dc.getPrefix());
        dcSolrObject.setNamespace(dc.getNamespace());
        dcSolrObject.setSubResourceIdentifier(dc.getSubResourceIdentifier());
        dcSolrObject.setF1(dc.getF1());
        dcSolrObject.setF2(dc.getF2());
        dcSolrObject.setF3(dc.getF3());
        dcSolrObject.setF4(dc.getF4());
        dcSolrObject.setF5(dc.getF5());
        dcSolrObject.setF6(dc.getF6());
        dcSolrObject.setF7(dc.getF7());
        dcSolrObject.setF8(dc.getF8());
        dcSolrObject.setF9(dc.getF9());
        dcSolrObject.setF10(dc.getF10());
        dcSolrObject.setRecordNoOfReview(dc.getRecordNoOfReview());
        dcSolrObject.setPeriodStart(dc.getPeriodStart());
        dcSolrObject.setAllowedLanguages(dc.getAllowedLanguages());
        dcSolrObject.setAssignedTo(dc.getAssignedTo());
        dcSolrObject.setCurationStatus(dc.getCurationStatus().toString());
        dcSolrObject.setLocalObjectAvailable(dc.isLocalObjectAvailable());
        dcSolrObject.setOtherTagJson(dc.getOtherTagJson());
        dcSolrObject.setOtherTagValues(dc.getOtherTagValues());
        dcSolrObject.setMarkTaggingJson(dc.getMarkTaggingJson());
        dcSolrObject.setMetadataStandard(dc.getMetadataStandard());
    return dcSolrObject;
    }
}
