package in.gov.nvli.search.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class for keeping project wise Constants.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public class Constants {

    /**
     * Constant for table prefix.
     */
    public static final String MIT_TABLE_PREFIX = "mit_";
    /**
     * Meta-data type constant for Marc21.
     */
    public final static String MARC_21 = "Marc21";
    /**
     * Meta-data type constant for Dublin Core (DC).
     */
    public final static String DUBLIN_CORE = "Dublin Core";
    /**
     * Constant for Removing Value Separator(i.e value separator '&|&').
     */
    public static final String REMOVE_VALUE_SEPARATOR = "&\\|&";
    /**
     * filters keywords
     */
    public final static String CREATOR = "creator";
    public final static String PUBLISHER = "publisher";
    public final static String CONTRIBUTOR = "contributor";
    public final static String FORMAT = "format";
    public final static String LANGUAGE = "languages";
    public final static String DATE = "date";
    public final static String INSTITUTE = "institution";
    /**
     * List of string that need to exclude from facets
     */
    public static final List<String> EXCLUDE_FROM_FACET = new ArrayList<>(Arrays.asList("", "null"));
    /**
     * Specific faceting fields values
     */
    public final static String F1 = "Field-1";
    public final static String F2 = "Field-2";
    public final static String F3 = "Field-3";
    public final static String F4 = "Field-4";
    public final static String F5 = "Field-5";
    public final static String F6 = "Field-6";
    public final static String F7 = "Field-7";
    public final static String F8 = "Field-8";
    public final static String F9 = "Field-9";
    public final static String F10 = "Field-10";
    /**
     * ranking rules
     */
    public final static String NVLI_RL_1 = "nvli_rl_1";
    public final static String NVLI_RL_2 = "nvli_rl_2";
    public final static String NVLI_RL_3 = "nvli_rl_3";
    public final static String NVLI_RL_4 = "nvli_rl_4";
    public final static String NVLI_RL_5 = "nvli_rl_5";
    public final static String NVLI_RL_6 = "nvli_rl_6";
    /**
     * constants for curation map. If you are changing these value, please also
     * change in NVLI Project.
     */
    public final static String DS_FROM_USER_IDS = "from-userids";
    public final static String DS_TO_USER_IDS = "to-userids";
    public final static String AUDIO_VISUAL="AVAR";
    public static final String NPTEL = "NPTEL";
    
    public final static String SOLR_SEPRATOR = ":";
    public final static String SOLR_OR = "OR";
    public final static String SOLR_AND = "AND";
    
    public final static String SUGGEST_TITLE = "title";
    public final static String SUGGEST_SUBJECT = "subject";
    public final static String SUGGEST_CREATOR = "creator";
    public final static String SUGGEST_DESCRIPTION = "description";
    public final static String SUGGEST_PUBLISHER = "publisher";
    
    public final static String SUGGEST_F1 = "field-1";
    public final static String SUGGEST_F2 = "field-2";
    public final static String SUGGEST_F3 = "field-3";
    public final static String SUGGEST_F4 = "field-4";
    public final static String SUGGEST_F5 = "field-5";
    public final static String SUGGEST_F6 = "field-6";
    public final static String SUGGEST_F7 = "field-7";
    public final static String SUGGEST_F8 = "field-8";
    public final static String SUGGEST_F9 = "field-9";
    public final static String SUGGEST_F10 = "field-10";
}
