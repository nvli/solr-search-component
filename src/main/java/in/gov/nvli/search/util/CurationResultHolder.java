package in.gov.nvli.search.util;

import java.util.Collections;
import java.util.List;

/**
 * Class needed for holding the returned results from db and their count related
 * to Curation.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public class CurationResultHolder {

    /**
     * for holding results
     */
    private final List results;
    /**
     * for holding results count
     */
    private final Long resultSize;

    /**
     * Arguement constructor
     *
     * @param results list of results
     * @param resultSize size of results list
     */
    public CurationResultHolder(List results, Long resultSize) {
        this.results = Collections.unmodifiableList(results);
        this.resultSize = resultSize;
    }

    /**
     * Give size of results.
     *
     * @return size of results
     */
    public Long getResultSize() {
        return resultSize;
    }

    /**
     * Give list of results
     *
     * @return results
     */
    public List getResults() {
        return results;
    }
}
