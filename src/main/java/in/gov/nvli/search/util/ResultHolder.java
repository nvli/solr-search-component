package in.gov.nvli.search.util;

import in.gov.nvli.search.solr.domain.SolrDCMetadataInformation;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Class needed for holding the returned results from db and their count.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public class ResultHolder {

    /**
     * for holding results for DCMetadatainformation
     */
    private final List<SolrDCMetadataInformation> dcResults;

    /**
     * for holding results count
     */
    private final Long resultSize;
    /**
     * for holding faceting result
     */
    private final LinkedHashMap<String, List<FacetNameCountHolder>> facetMap;
    /**
     * for holding specific faceting result
     */
    private final LinkedHashMap<String, List<FacetNameCountHolder>> specificFacetMap;

    /**
     * Argument constructor
     *
     * @param dcResults
     * @param resultSize
     * @param facetMap
     * @param specificFacetMap
     */
    public ResultHolder(List<SolrDCMetadataInformation> dcResults, Long resultSize, LinkedHashMap<String, List<FacetNameCountHolder>> facetMap, LinkedHashMap<String, List<FacetNameCountHolder>> specificFacetMap) {
        this.dcResults = dcResults;
        this.resultSize = resultSize;
        this.facetMap = facetMap;
        this.specificFacetMap = specificFacetMap;
    }

    /**
     * getter of specificFacetMap
     *
     * @return
     */
    public LinkedHashMap<String, List<FacetNameCountHolder>> getSpecificFacetMap() {
        return specificFacetMap;
    }

    /**
     * give result size
     *
     * @return
     */
    public Long getResultSize() {
        return resultSize;
    }

    /**
     * give list of dcResults
     *
     * @return
     */
    public List<SolrDCMetadataInformation> getDcResults() {
        return dcResults;
    }

    /**
     * give map of facet
     *
     * @return
     */
    public LinkedHashMap<String, List<FacetNameCountHolder>> getFacetMap() {
        return facetMap;
    }

}
