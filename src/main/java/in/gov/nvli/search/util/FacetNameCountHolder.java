package in.gov.nvli.search.util;

import java.io.Serializable;

/**
 * Class needed for holding the returned Facet from db and their count.
 *
 * @author Saurabh Koriya
 * @since 1
 * @version 1
 */
@SuppressWarnings("serial")
public class FacetNameCountHolder implements Serializable{

    /**
     * Name Value of Facet
     */
    private String name;
    /*
     * Count of the records
     */
    private int count;

    /**
     * Getter of count
     *
     * @return
     */
    public int getCount() {
        return count;
    }

    /**
     * setter of count
     *
     * @param count
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Getter of name
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Setter of name
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}
