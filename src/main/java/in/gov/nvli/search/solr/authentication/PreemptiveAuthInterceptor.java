/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.gov.nvli.search.solr.authentication;

import java.io.IOException;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.protocol.HttpCoreContext;

/**
 * This Class uses  PreemptiveAuth to implements HttpRequestInterceptor and authenticate client to access SOLR server by HTTPClient.
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 * @version 1
 * @since 1
 * @see org.apache.http.HttpRequestInterceptor
 */
public class PreemptiveAuthInterceptor implements HttpRequestInterceptor {
    /**
     * This Method do authentication processing.
     * @param request
     * @param context
     * @throws HttpException
     * @throws IOException 
     */
    @Override
    public void process(HttpRequest request, org.apache.http.protocol.HttpContext context) throws HttpException, IOException {
        final AuthState authState = (AuthState) context.getAttribute(HttpClientContext.TARGET_AUTH_STATE);
        if (authState.getAuthScheme() == null) {
            final CredentialsProvider credsProvider = (CredentialsProvider) context
                    .getAttribute(HttpClientContext.CREDS_PROVIDER);
            final HttpHost targetHost = (HttpHost) context.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
            final Credentials creds = credsProvider.getCredentials(new AuthScope(targetHost.getHostName(),
                    targetHost.getPort()));
            if (creds == null) {
                throw new HttpException("No creds provided for preemptive auth.");
            }
            authState.update(new BasicScheme(), creds);
        }
    }
}
