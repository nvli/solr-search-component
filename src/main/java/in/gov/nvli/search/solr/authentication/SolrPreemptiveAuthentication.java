package in.gov.nvli.search.solr.authentication;

import java.net.URI;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Class is used by to authenticate client to access SOLR server by HTTPClient.
 *
 * @author Saurabh Koriya <ksaurabh@cdac.in>
 * @version 1
 * @since 1
 * @see solr preemptive authentication
 */
public class SolrPreemptiveAuthentication {
    /**
     * This Method will return HttpClientBuilder with PreemptiveAuthentication
     * @param uri
     * @param username
     * @param password
     * @return HttpClientBuilder
     */
    public static HttpClient createHttpClient(String uri, String username, String password) {
        final URI scopeUri = URI.create(uri);
        final BasicCredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(new AuthScope(scopeUri.getHost(), scopeUri.getPort()), new UsernamePasswordCredentials(username, password));
        final HttpClientBuilder builder = HttpClientBuilder.create()
                .setMaxConnTotal(2048)
                .setMaxConnPerRoute(512)
                .setDefaultRequestConfig(RequestConfig.copy(RequestConfig.DEFAULT).setRedirectsEnabled(true).build())
                .setDefaultCredentialsProvider(credentialsProvider).addInterceptorFirst(new PreemptiveAuthInterceptor());
        return builder.build();
    }  
}