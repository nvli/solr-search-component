package in.gov.nvli.search.domain;

import in.gov.nvli.search.domain.generic.BaseEntity;
import in.gov.nvli.search.util.Constants;
import java.util.Date;
import javax.persistence.*;

/**
 * Hold the producer database Information. Entity for mapping_information.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 */
@Entity
@AttributeOverrides({
    @AttributeOverride(name = "id", column
            = @Column(name = "id"))
})
@Table(name = Constants.MIT_TABLE_PREFIX + "mapping_information")
@SuppressWarnings("serial")
public class MappingInformation extends BaseEntity {

    /**
     * When Created.
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdOn;
    /**
     * When updated
     */
    @Column(columnDefinition = "TIMESTAMP DEFAULT  current_timestamp on update current_timestamp", insertable = false, updatable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updatedOn;

    /**
     * Original field name
     */
    @Column(name = "original_field_name")
    private String originalFieldName;
    /**
     * Mapped field name
     */
    @Column(name = "mapped_field_name")
    private String mappedFieldName;
    /**
     * Reference name
     */
    @Column(name = "reference_name")
    private String referenceName;

    /**
     * give value of createdOn property
     *
     * @return
     */
    public Date getCreatedOn() {
        return createdOn;
    }

    /**
     * set value of createdOn property
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * give value of mappedFieldName property
     *
     * @return
     */
    public String getMappedFieldName() {
        return mappedFieldName;
    }

    /**
     * set value of mappedFieldName property
     *
     * @param mappedFieldName
     */
    public void setMappedFieldName(String mappedFieldName) {
        this.mappedFieldName = mappedFieldName;
    }

    /**
     * give value of originalFieldName property
     *
     * @return
     */
    public String getOriginalFieldName() {
        return originalFieldName;
    }

    /**
     * set value of originalFieldName property
     *
     * @param originalFieldName
     */
    public void setOriginalFieldName(String originalFieldName) {
        this.originalFieldName = originalFieldName;
    }

    /**
     * give value of referenceName property
     *
     * @return
     */
    public String getReferenceName() {
        return referenceName;
    }

    /**
     * set value of referenceName property
     *
     * @param referenceName
     */
    public void setReferenceName(String referenceName) {
        this.referenceName = referenceName;
    }

    /**
     * give value of updatedOn property
     *
     * @return
     */
    public Date getUpdatedOn() {
        return updatedOn;
    }

    /**
     * set value of updatedOn property
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
}
