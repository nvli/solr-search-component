package in.gov.nvli.search.domain;

import in.gov.nvli.search.domain.generic.CreatableEntity;
import in.gov.nvli.search.util.Constants;
import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.Type;

/**
 * Entity for resource_information table.
 *
 * @author Hemant Anjana
 * @serial 1
 * @version 1
 */
@Entity
@AttributeOverrides({
    @AttributeOverride(name = "id", column
            = @Column(name = "id")),
    @AttributeOverride(name = "createdOn", column
            = @Column(name = "createdOn", updatable = false, insertable = true, nullable = false)),
    @AttributeOverride(name = "createdBy", column
            = @Column(name = "createdBy"))
})
@Table(name = Constants.MIT_TABLE_PREFIX + "resource_information")
@SuppressWarnings("serial")
public class ResourceInfo extends CreatableEntity {

    /**
     * name of resource.
     */
    @Column(name = "resource_name", nullable = false, unique = true)
    private String name;
    /**
     * classification level of resource
     */
    @Column(name = "classification_level", nullable = true, columnDefinition = "varchar(255)")
    private String classificationLevel;
    /**
     * resource code
     */
    @Column(name = "resource_code", nullable = false, unique = true)
    private String resourceCode;
    /**
     * establish year
     */
    @Column(name = "establish_year")
    private String establishYear;
    /**
     * count of records
     */
    @Column(name = "record_count")
    private String totalCount;
    /**
     * website
     */
    @Column(name = "website")
    private String website;
    /**
     * languages
     */
    @Column(name = "languages")
    private String languages;
    /**
     * items collected
     */
    @Type(type = "text")
    @Column(name = "items_collected")
    private String itemsCollected;
    /**
     * description
     */
    @Type(type = "text")
    @Column(name = "description")
    private String description;
    /**
     * address of organization
     */
    @Type(type = "text")
    @Column(name = "address")
    private String address;
    /**
     * organization name
     */
    @Column(name = "organization")
    private String organization;
    /**
     * contact person
     */
    @Column(name = "contact_person")
    private String contactPerson;
    /**
     * designation of contact person
     */
    @Column(name = "designation")
    private String designation;
    /**
     * contact number of contact person
     */
    @Column(name = "contact_no")
    private String contactNo;
    /**
     * email id of contact person.
     */
    @Column(name = "email_id")
    private String emailId;
    /**
     * content flag: whether has content or not
     */
    @Column(name = "has_content", columnDefinition = "bit(1) default false")
    private boolean contentFlag;
    /**
     * count of records received
     */
    @Column(name = "received_count")
    private long receivedCount;
    /**
     * updated on: to keep track of last updated on
     */
    @Column(columnDefinition = "TIMESTAMP DEFAULT  current_timestamp on update current_timestamp", insertable = false, updatable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updatedOn;
    /**
     * used for enable/active the resource
     */
    @Column(name = "has_activiated", columnDefinition = "bit(1) default false")
    private boolean activationFlag;
    /**
     * used for publishing the resource
     */
    private boolean publishFlag;
    /**
     * for crowdSourcing.
     */
    private boolean crowdSource;
    /**
     * thematic classification.
     */
    @Column(name = "thematic_classification")
    private String thematicClassification;

    /**
     * give value of publishFlag property
     *
     * @return
     */
    public boolean isPublishFlag() {
        return publishFlag;
    }

    /**
     * set value of publishFlag property
     *
     * @param publishFlag
     */
    public void setPublishFlag(boolean publishFlag) {
        this.publishFlag = publishFlag;
    }

    /**
     * give value of crowdSource property
     *
     * @return
     */
    public boolean isCrowdSource() {
        return crowdSource;
    }

    /**
     * set value of crowdSource property
     *
     * @param crowdSource
     */
    public void setCrowdSource(boolean crowdSource) {
        this.crowdSource = crowdSource;
    }

    /**
     * give value of activationFlag property
     *
     * @return
     */
    public boolean isActivationFlag() {
        return activationFlag;
    }

    /**
     * set value of activationFlag property
     *
     * @param activationFlag
     */
    public void setActivationFlag(boolean activationFlag) {
        this.activationFlag = activationFlag;
    }

    /**
     * give value of address property
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     * set value of address property
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * give value of contactNo property
     *
     * @return
     */
    public String getContactNo() {
        return contactNo;
    }

    /**
     * set value of contactNo property
     *
     * @param contactNo
     */
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    /**
     * give value of contactPerson property
     *
     * @return
     */
    public String getContactPerson() {
        return contactPerson;
    }

    /**
     * set value of contactPerson property
     *
     * @param contactPerson
     */
    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    /**
     * give value of contentFlag property
     *
     * @return
     */
    public boolean isContentFlag() {
        return contentFlag;
    }

    /**
     * set value of contentFlag property
     *
     * @param contentFlag
     */
    public void setContentFlag(boolean contentFlag) {
        this.contentFlag = contentFlag;
    }

    /**
     * give value of description property
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * set value of description property
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * give value of designation property
     *
     * @return
     */
    public String getDesignation() {
        return designation;
    }

    /**
     * set value of designation property
     *
     * @param designation
     */
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    /**
     * give value of emailId property
     *
     * @return
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * set value of emailId property
     *
     * @param emailId
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * give value of establishYear property
     *
     * @return
     */
    public String getEstablishYear() {
        return establishYear;
    }

    /**
     * set value of establishYear property
     *
     * @param establishYear
     */
    public void setEstablishYear(String establishYear) {
        this.establishYear = establishYear;
    }

    /**
     * give value of itemsCollected property
     *
     * @return
     */
    public String getItemsCollected() {
        return itemsCollected;
    }

    /**
     * set value of itemsCollected property
     *
     * @param itemsCollected
     */
    public void setItemsCollected(String itemsCollected) {
        this.itemsCollected = itemsCollected;
    }

    /**
     * give value of languages property
     *
     * @return
     */
    public String getLanguages() {
        return languages;
    }

    /**
     * set value of languages property
     *
     * @param languages
     */
    public void setLanguages(String languages) {
        this.languages = languages;
    }

    /**
     * give value of name property
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * set value of name property
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * give value of organization property
     *
     * @return
     */
    public String getOrganization() {
        return organization;
    }

    /**
     * set value of organization property
     *
     * @param organization
     */
    public void setOrganization(String organization) {
        this.organization = organization;
    }

    /**
     * give value of receivedCount property
     *
     * @return
     */
    public long getReceivedCount() {
        return receivedCount;
    }

    /**
     * set value of receivedCount property
     *
     * @param receivedCount
     */
    public void setReceivedCount(long receivedCount) {
        this.receivedCount = receivedCount;
    }

    /**
     * give value of resourceCode property
     *
     * @return
     */
    public String getResourceCode() {
        return resourceCode;
    }

    /**
     * set value of resourceCode property
     *
     * @param resourceCode
     */
    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    /**
     * give value of classificationLevel property
     *
     * @return
     */
    public String getClassificationLevel() {
        return classificationLevel;
    }

    /**
     * set value of classificationLevel property
     *
     * @param classificationLevel
     */
    public void setClassificationLevel(String classificationLevel) {
        this.classificationLevel = classificationLevel;
    }

    /**
     * give value of thematicClassification property
     *
     * @return
     */
    public String getThematicClassification() {
        return thematicClassification;
    }

    /**
     * set value of thematicClassification property
     *
     * @param thematicClassification
     */
    public void setThematicClassification(String thematicClassification) {
        this.thematicClassification = thematicClassification;
    }

    /**
     * give value of totalCount property
     *
     * @return
     */
    public String getTotalCount() {
        return totalCount;
    }

    /**
     * set value of totalCount property
     *
     * @param totalCount
     */
    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * give value of updatedOn property
     *
     * @return
     */
    public Date getUpdatedOn() {
        return updatedOn;
    }

    /**
     * set value of updatedOn property
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    /**
     * give value of website property
     *
     * @return
     */
    public String getWebsite() {
        return website;
    }

    /**
     * set value of website property
     *
     * @param website
     */
    public void setWebsite(String website) {
        this.website = website;
    }

    /**
     * override toString to return only name
     *
     * @return
     */
    @Override
    public String toString() {
        return name;
    }
}
