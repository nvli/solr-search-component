package in.gov.nvli.search.domain;

import in.gov.nvli.search.domain.generic.CreatableEntity;
import in.gov.nvli.search.util.Constants;
import javax.persistence.*;

/**
 * Entity for resource_type_information table.
 *
 * @author saurabh koriya
 * @version 1
 * @see 1
 */
@Entity
@AttributeOverrides({
    @AttributeOverride(name = "id", column
            = @Column(name = "id"))
})
@Table(name = Constants.MIT_TABLE_PREFIX + "resource_type_information")
@SuppressWarnings("serial")
public class ResourceTypeInfo extends CreatableEntity {

    /**
     * Resource Type
     */
    @Column(name = "resource_type")
    private String resourceType;
    /**
     * Resource Code
     */
    @Column(name = "resource_type_code")
    private String resourceTypeCode;
    /**
     * for crowdSourcing.
     */
    @Column(name = "has_crowdSource", columnDefinition = "bit(1) default false")
    private boolean crowdSource;

    /**
     * give value of crowdSource property
     *
     * @return
     */
    public boolean isCrowdSource() {
        return crowdSource;
    }

    /**
     * set value of crowdSource property
     *
     * @param crowdSource
     */
    public void setCrowdSource(boolean crowdSource) {
        this.crowdSource = crowdSource;
    }

    /**
     * give value of resourceType property
     *
     * @return
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * set value of resourceType property
     *
     * @param resourceType
     */
    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * give value of resourceTypeCode property
     *
     * @return
     */
    public String getResourceCode() {
        return resourceTypeCode;
    }

    /**
     * set value of resourceTypeCode property
     *
     * @param resourceCode
     */
    public void setResourceCode(String resourceCode) {
        this.resourceTypeCode = resourceCode;
    }
}
