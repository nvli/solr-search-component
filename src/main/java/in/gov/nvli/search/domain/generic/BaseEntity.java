package in.gov.nvli.search.domain.generic;

import in.gov.nvli.search.util.Constants;
import java.io.Serializable;
import javax.persistence.*;

/**
 * Generic Entity for auto increment id creation
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@MappedSuperclass
@SuppressWarnings("serial")
public class BaseEntity implements Identifiable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "mitSeqGenerator")
    @TableGenerator(name = "mitSeqGenerator", allocationSize = 1, initialValue = 1, table = Constants.MIT_TABLE_PREFIX + "sequence")
    private Long id;

    /**
     * give value of id property
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * set value of id property
     *
     * @param id
     */
    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
