package in.gov.nvli.search.domain;

import in.gov.nvli.search.domain.generic.BaseEntity;
import in.gov.nvli.search.util.Constants;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Entity for user_statistics table.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@Entity
@Table(name = Constants.MIT_TABLE_PREFIX + "user_statistics")
@SuppressWarnings("serial")
public class UserStatistics extends BaseEntity {

    /**
     * user id.
     */
    @Column(name = "user_id", columnDefinition = "bigint(20)")
    private long userId;
    /**
     * assigned count
     */
    @Column(name = "assigned_count", columnDefinition = "bigint(20) default 0")
    private long assignedCount;
    /**
     * processed count
     */
    @Column(name = "processed_count", columnDefinition = "bigint(20) default 0")
    private long processedCount;
    /**
     * sub-resource identifier
     */
    @Column(name = "subResourceIdentifier")
    private String subResourceidentifier;

    /**
     * give value of subResourceidentifier property
     *
     * @return
     */
    public String getSubResourceidentifier() {
        return subResourceidentifier;
    }

    /**
     * set value of subResourceidentifier property
     *
     * @param subResourceidentifier
     */
    public void setSubResourceidentifier(String subResourceidentifier) {
        this.subResourceidentifier = subResourceidentifier;
    }

    /**
     * give value of userId property
     *
     * @return
     */
    public long getUserId() {
        return userId;
    }

    /**
     * set value of userId property
     *
     * @param userId
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * give value of assignedCount property
     *
     * @return
     */
    public long getAssignedCount() {
        return assignedCount;
    }

    /**
     * set value of assignedCount property
     *
     * @param assignedCount
     */
    public void setAssignedCount(long assignedCount) {
        this.assignedCount = assignedCount;
    }

    /**
     * give value of processedCount property
     *
     * @return
     */
    public long getProcessedCount() {
        return processedCount;
    }

    /**
     * set value of processedCount property
     *
     * @param processedCount
     */
    public void setProcessedCount(long processedCount) {
        this.processedCount = processedCount;
    }

    /**
     * generate hash code based on user id field.
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (int) (this.userId ^ (this.userId >>> 32));
        return hash;
    }

    /**
     * check equality of object based on user id field.
     *
     * @param obj
     * @return true if equals else false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserStatistics other = (UserStatistics) obj;
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

}
