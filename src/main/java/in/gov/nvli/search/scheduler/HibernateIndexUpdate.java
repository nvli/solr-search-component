package in.gov.nvli.search.scheduler;

import in.gov.nvli.search.dao.IDCMetadataInformationDAO;
import in.gov.nvli.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * This class is used for updated the index when new records is added to system
 *
 * @author Saurabh Koriya
 * @since 1
 * @version 1
 */
@Component(value = "hibernateIndexUpdate")
public class HibernateIndexUpdate {

    /**
     * Autowired IDCMetadataInformationDAO Reference
     */
    @Autowired
    private IDCMetadataInformationDAO dCMetadataInformationDAO;
    /**
     * Autowired {@link SearchService}.
     */
    @Autowired
    private SearchService searchService;
    /**
     * Logger reference
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(HibernateIndexUpdate.class.getName());

    /**
     * first time index flag is changed to true
     */
    public void firstTimeupdateRecordIndexColumn() {
        try {
            boolean dcFlag = dCMetadataInformationDAO.changeIndexFlag(false);
            if (dcFlag) {
                logger.info("DC MetaData Records updated first time");
            }
        } catch (Exception ex) {
            logger.error(ex, ex);
        }

    }

    /**
     * Scheduler runs every 30 min and update indexing for newly added records.
     */
    @Scheduled(cron = "0 0/30 * * * ?")
    public void updateIndex() {
        try {
            //checking records is Type of DC and updating index.
            searchService.dcIndexUpdation();
        } catch (Exception ex) {
            logger.error(ex, ex);
        }
    }
}
