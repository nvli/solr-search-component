package in.gov.nvli.search.service;

import in.gov.nvli.search.dao.IDCMetadataInformationDAO;
import in.gov.nvli.search.dao.IResourceInfoDAO;
import in.gov.nvli.search.dao.IResourceTypeInfoDAO;
import in.gov.nvli.search.dao.IUserStatisticsDAO;
import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.domain.ResourceTypeInfo;
import in.gov.nvli.search.domain.UserStatistics;
import in.gov.nvli.search.form.*;
import in.gov.nvli.search.util.Constants;
import in.gov.nvli.search.util.CurationResultHolder;
import in.gov.nvli.search.util.Helper;
import in.gov.nvli.search.util.ProcessDC;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service for Curation
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@Service("curationService")
public class CurationService {

    /**
     * logger for log the error
     */
    private static final Logger LOG = Logger.getLogger(CurationService.class);
    /**
     * reference of IResourceTypeInfoDAO
     */
    @Autowired
    private IResourceTypeInfoDAO resourceTypeInfoDAO;
    /**
     * reference of IResourceInfoDAO
     */
    @Autowired
    private IResourceInfoDAO resourceInfoDAO;
    /**
     * reference of IUserStatisticsDAO
     */
    @Autowired
    private IUserStatisticsDAO userStatisticsDAO;
    /**
     * reference of IDCMetadataInformationDAO
     */
    @Autowired
    private IDCMetadataInformationDAO dCMetadataInformationDAO;

    /**
     * Method to get distribution count ({@link DistributionCount}) for given
     * sub-resource code. It gives total count and count of records available
     * for distribution for given sub-resource code.
     * <p>
     * Uses this method
     * <u>{@link IDCMetadataInformationDAO.getTotalCountForCuration()}</u>
     * <u>{@link IUserStatisticsDAO.getTotalAssignedCount()}</u>
     * </p
     *
     * @param subResourceCode whose record count for distribution need to find.
     * @return {@link DistributionCount}
     */
    public DistributionCount getCountAvailableForDistribution(String subResourceCode) {

        DistributionCount distributionCount = new DistributionCount();
        distributionCount.setSubResourceCode(subResourceCode);
        long count = 0;
        Long totalCount = 0l;
        try {
            totalCount = dCMetadataInformationDAO.getTotalCountForCuration(subResourceCode);
            if (totalCount == null || totalCount <= 0) {
                totalCount = 0l;
            }

            Long assignedCount = userStatisticsDAO.getTotalAssignedCount(subResourceCode);
            if (assignedCount == null) {
                assignedCount = 0l;
            }
            count = totalCount - assignedCount;
            if (count < 0) {
                count = 0;
            }
        } catch (Exception ex) {
            LOG.error(ex, ex);
        }

        distributionCount.setCount(count);
        distributionCount.setTotalCount(totalCount);
        return distributionCount;
    }

    /**
     * Method to distribute records of given sub-resource code among given list
     * of users ids.
     *
     * @param subResourceCode sub-resource code whose records need to
     * distributed.
     * @param userIds List of user ids among whom records need to be
     * distributed.
     * @return
     */
    public Boolean distribute(String subResourceCode, List<Long> userIds) {
        Boolean flag = false;
        try {
            List<DCMetadataInformation> dcList = dCMetadataInformationDAO.getList_UNDISTRU_UNCURA(subResourceCode);
            flag = distrubtionProcess(subResourceCode, userIds, dcList);
        } catch (Exception ex) {
            flag = false;
            LOG.error(ex, ex);
            ex.printStackTrace();
        }
        return flag;
    }

    /**
     * Method to redistribute records of given sub-resource code among given
     * userMap. Method redistribute records that were assigned to user ids
     * identified by 'from-userids'(in userMap) to users identified by
     * 'to-userids' (in userMap).
     *
     * @param subResourceCode sub-resource code whose records need to
     * distributed.
     * @param userMap map containing two entries: 'from-userids' and
     * 'to-userids' with value long ids.
     *
     * userMap will have two entries with following keys.
     * <p>
     * key: 'from-userids':: value: list of long ids</p>
     * <p>
     * key: 'to-userids':: value: list long ids </p>
     * @return true if successful else false.
     */
    public Boolean redistribute(String subResourceCode, Map<String, List<Long>> userMap) {
        Boolean flag = false;
        if (userMap != null && !userMap.isEmpty() && userMap.containsKey(Constants.DS_FROM_USER_IDS) && userMap.containsKey(Constants.DS_TO_USER_IDS)) {
            //getting take from users and assign to users
            try {
                List<Long> fromUserIds = userMap.get(Constants.DS_FROM_USER_IDS);
                List<Long> toUserIds = userMap.get(Constants.DS_TO_USER_IDS);

                if (fromUserIds != null && !fromUserIds.isEmpty() && toUserIds != null && !toUserIds.isEmpty()) {
                    List<DCMetadataInformation> dcList = dCMetadataInformationDAO.getList_DISTRI_UNCURA(subResourceCode, fromUserIds);
                    if (dcList != null && !dcList.isEmpty()) {
                        unAssignProcessDC(dcList);
                    }
                    flag = distrubtionProcess(subResourceCode, toUserIds, dcList);
                }

            } catch (Exception ex) {
                flag = false;
                LOG.error(ex, ex);
                ex.printStackTrace();
            }
        }
        return flag;
    }

    /**
     * Method give curation statistics of given user id.
     *
     * @param userId user id whose curation statistics need to find.
     * @return {@link ResourceCurationCount}
     */
    public ResourceCurationCount getUserStats(Long userId) {
        if (Helper.RESOURCE_TYPE_CODE == null || Helper.RESOURCE_TYPE_CODE.isEmpty() || Helper.RESOURCE_TYPE_NAME == null || Helper.RESOURCE_TYPE_NAME.isEmpty()) {
            ResourceTypeInfo resourceInformation = resourceTypeInfoDAO.list().get(0);
            Helper.RESOURCE_TYPE_CODE = resourceInformation.getResourceCode();
            Helper.RESOURCE_TYPE_NAME = resourceInformation.getResourceType();
        }

        Map<String, CurationCount> userCurationCounts = new HashMap<>();
        try {
            List<Long> userIds = new ArrayList<>();
            userIds.add(userId);
            List<UserStatistics> userStatisticsList = userStatisticsDAO.getUserStatisticsByUserIds(userIds);
            if (userStatisticsList != null && !userStatisticsList.isEmpty()) {
                for (UserStatistics userStatistics : userStatisticsList) {
                    CurationCount curationCount = new CurationCount();
                    curationCount.setAssignedCount(userStatistics.getAssignedCount());
                    curationCount.setCuratedCount(userStatistics.getProcessedCount());
                    userCurationCounts.put(userStatistics.getSubResourceidentifier(), curationCount);
                }
            }
        } catch (Exception ex) {
            LOG.error(ex, ex);
            ex.printStackTrace();
        }

        ResourceCurationCount resourceCurationCount = new ResourceCurationCount();
        resourceCurationCount.setResourceTypeCode(Helper.RESOURCE_TYPE_CODE);
        resourceCurationCount.setResourceTypeName(Helper.RESOURCE_TYPE_NAME);
        resourceCurationCount.setUserCurationCounts(userCurationCounts);
        return resourceCurationCount;
    }

    /**
     * Method give Curation records list using {@link CurationForm}.
     *
     * @param curationForm {@link CurationForm} contains parameter for list
     * records.
     * @return {@link CurationForm}
     */
    @SuppressWarnings("unchecked")
    public CurationForm getUserRecords(CurationForm curationForm) {
        Long resultSize = 0l;
        List<SearchResult> searchResults = null;
        boolean resultFound = false;
        try {
            CurationStatus curationStatus = curationForm.getStatus();
            if (curationStatus == null) {
                curationStatus = CurationStatus.UNCURATED;
                curationForm.setStatus(curationStatus);
            }
            Long userId = curationForm.getUserId();
            if (userId == null || userId <= 0) {
                userId = -1l;
            }
            int pageNum = 1;
            int pageWin = 25;
            try {
                pageNum = Integer.parseInt(curationForm.getPageNo());
                pageWin = Integer.parseInt(curationForm.getPageWin());
            } catch (NumberFormatException ex) {
                LOG.error(ex, ex);
                curationForm.setPageNo(pageNum + "");
                curationForm.setPageWin(pageWin + "");
            }
            List<String> allSubResourceIdentifiers = userStatisticsDAO.getAssingedSubResouceIdentifers(userId);
            curationForm.setAllSubResourceIdentifiers(allSubResourceIdentifiers);
            List<String> subResourceIdentifiers = null;
            if (curationForm.getSubResourceIdentifiers() == null || curationForm.getSubResourceIdentifiers().isEmpty()) {
                subResourceIdentifiers = allSubResourceIdentifiers;
            } else {
                subResourceIdentifiers = curationForm.getSubResourceIdentifiers();
            }
            CurationResultHolder curationResultHolder = null;
            curationResultHolder = dCMetadataInformationDAO.getListByStatus_userId(curationStatus, userId, pageNum, pageWin, curationForm.getLanguages(), curationForm.getSearchTerm(), subResourceIdentifiers);
            searchResults = new ArrayList<>();

            if (curationResultHolder != null) {
                resultSize = curationResultHolder.getResultSize();
                if (resultSize == null || resultSize <= 0) {
                    resultFound = false;
                } else {
                    resultFound = true;
                    List<DCMetadataInformation> dcList = curationResultHolder.getResults();
                    if (dcList != null && !dcList.isEmpty()) {
                        SearchResult searchResult = null;
                        for (DCMetadataInformation dc : dcList) {
                            searchResult = Helper.createCurationSearchResultFromDC(dc);
                            if (searchResult != null) {
                                searchResults.add(searchResult);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            LOG.error(ex, ex);
        }
        curationForm.setListOfResult(searchResults);
        curationForm.setResultSize(resultSize);
        return curationForm;
    }

    /**
     * Method does distribution process. Method distribute records available in
     * given 'dcList' among user id available in 'toUserIds' for given
     * subResourceIdentifer.
     *
     * @param subResourceIdentifer
     * @param toUserIds users ids to whom records need to assign
     * @param dcList dc record list available for distribution
     * @return
     */
    public Boolean distrubtionProcess(String subResourceIdentifer, List<Long> toUserIds, List<DCMetadataInformation> dcList) {
        Boolean flag = false;
        if ((dcList != null && !dcList.isEmpty()) || (toUserIds != null && !toUserIds.isEmpty())) {
            Set<Long> userIdSet = new HashSet<>(toUserIds);
            Set<UserStatistics> userSet = new HashSet<>();
            for (Long userId : userIdSet) {
                if (userId != null && !userId.equals(0L)) {
                    try {
                        UserStatistics userStatistics = userStatisticsDAO.getUserStatisticsByUserId_SubRes(subResourceIdentifer, userId);
                        if (userStatistics == null) {
                            userStatistics = new UserStatistics();
                            userStatistics.setUserId(userId);
                            userStatistics.setAssignedCount(0);
                            userStatistics.setProcessedCount(0);
                            userStatistics.setSubResourceidentifier(subResourceIdentifer);
                            userStatisticsDAO.createNew(userStatistics);
                        }
                        userSet.add(userStatistics);
                    } catch (Exception ex) {
                        LOG.error(ex, ex);
                        ex.printStackTrace();
                    }
                }
            }
            try {
                List<UserStatistics> userList = new ArrayList<>(userSet);
                if (dcList != null && !dcList.isEmpty()) {
                    assignProcessDC(userList, dcList);
                }
                flag = true;
            } catch (Exception ex) {
                flag = false;
                LOG.error(ex, ex);
                ex.printStackTrace();
            }
        }
        return flag;
    }

    /**
     * Method assign given records 'dcList' among users 'usersList'. It uses
     * ExecutorService to process assigning task concurrently. Generate Fixed
     * Thread pool of size of userList.
     *
     * @param usersList list of {@link UserStatistics}
     * @param dcList list of {@link DCMetadataInformation}
     */
    public void assignProcessDC(List<UserStatistics> usersList, List<DCMetadataInformation> dcList) {
        int maxSize = usersList.size();
        ExecutorService executor = Executors.newFixedThreadPool(maxSize);
        List<Future<Void>> listProcess = new ArrayList<>();

        double fondToDistribute = ((double) dcList.size()) / ((double) maxSize);
        double blocksize = Math.ceil(fondToDistribute);
        DCMetadataInformation[] total = dcList.toArray(new DCMetadataInformation[dcList.size()]);
        UserStatistics[] users = usersList.toArray(new UserStatistics[usersList.size()]);
        if (blocksize <= 0) {
            blocksize = 1;
        }
        double toId = blocksize;
        double fromId = 1;
        for (int i = 1; i <= maxSize; i++) {
            toId = fromId + blocksize;

            if (toId > dcList.size()) {
                toId = dcList.size();
                System.out.println("\nStarting : Thread" + i);
                System.out.println("from--" + fromId + "To----" + toId);
                Callable<Void> worker = new ProcessDC(total, fromId, toId, users[i - 1].getUserId(), dCMetadataInformationDAO);
                Future<Void> submit = executor.submit(worker);
                listProcess.add(submit);
                break;
            } else {
                System.out.println("\nStarting : Thread" + i);
                System.out.println("from--" + fromId + "To----" + toId);
                Callable<Void> worker = new ProcessDC(total, fromId, toId, users[i - 1].getUserId(), dCMetadataInformationDAO);
                Future<Void> submit = executor.submit(worker);
                listProcess.add(submit);
                fromId = toId + 1;
            }
        }

        System.out.println("list size----" + listProcess.size());
        for (Future<Void> future : listProcess) {
            try {
                future.get();

            } catch (Exception e) {
                LOG.error(e);
                e.printStackTrace();
            }
        }
        if (executor != null) {
            executor.shutdown();
        }
    }

    /**
     * Method un-assign given records 'dcList'. It uses ExecutorService to
     * process assigning task concurrently. Generate Fixed Thread pool of size
     * 10.
     *
     * @param dcList list of {@link DCMetadataInformation}
     */
    public void unAssignProcessDC(List<DCMetadataInformation> dcList) {
        int maxSize = 10;
        ExecutorService executor = Executors.newFixedThreadPool(maxSize);
        List<Future<Void>> listProcess = new ArrayList<>();

        double fondToDistribute = ((double) dcList.size()) / ((double) maxSize);
        double blocksize = Math.ceil(fondToDistribute);
        DCMetadataInformation[] total = dcList.toArray(new DCMetadataInformation[dcList.size()]);

        if (blocksize <= 0) {
            blocksize = 1;
        }
        double toId = blocksize;
        double fromId = 1;
        for (int i = 1; i <= maxSize; i++) {
            toId = fromId + blocksize;

            if (toId > dcList.size()) {
                toId = dcList.size();
                System.out.println("\nStarting : Thread" + i);
                System.out.println("from--" + fromId + "To----" + toId);
                Callable<Void> worker = new ProcessDC(total, fromId, toId, 0l, dCMetadataInformationDAO);
                Future<Void> submit = executor.submit(worker);
                listProcess.add(submit);
                break;
            } else {
                System.out.println("\nStarting : Thread" + i);
                System.out.println("from--" + fromId + "To----" + toId);
                Callable<Void> worker = new ProcessDC(total, fromId, toId, 0l, dCMetadataInformationDAO);
                Future<Void> submit = executor.submit(worker);
                listProcess.add(submit);
                fromId = toId + 1;
            }
        }

        System.out.println("list size----" + listProcess.size());
        for (Future<Void> future : listProcess) {
            try {
                future.get();

            } catch (Exception e) {
                LOG.error(e);
                e.printStackTrace();
            }
        }
        if (executor != null) {
            executor.shutdown();
        }
    }

    /**
     * give list of distinct allowed languages.
     *
     * @return
     */
    public Set<String> getAvailableLanguages() {
        Set<String> langSet = new HashSet<>();
        try {
            List<String> langList = dCMetadataInformationDAO.getDistinctLang();
            if (langList != null && !langList.isEmpty()) {
                for (String str : langList) {
                    String strary[] = str.split(Constants.REMOVE_VALUE_SEPARATOR);
                    if (strary != null) {
                        for (String valueString : strary) {
                            if (valueString != null && !valueString.trim().isEmpty() && !langSet.contains(valueString)) {
                                langSet.add(valueString);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            LOG.error(ex, ex);
            ex.printStackTrace();
        }
        return langSet;
    }

    /**
     * Method give curation statistics of given subResourceCode.
     *
     * @param subResourceIdentifer whose curation statistics need to find.
     * @return Map containing key as user id and value as curation statistics of
     * that user id.
     */
    public Map<Long, CurationCount> getSubResourceStats(String subResourceIdentifer) {
        Map<Long, CurationCount> map = new HashMap<>();
        try {
            List<UserStatistics> userStatisticsList = userStatisticsDAO.getStatisticsBySubResourceCode(subResourceIdentifer);
            if (userStatisticsList != null && !userStatisticsList.isEmpty()) {
                for (UserStatistics userStatistics : userStatisticsList) {
                    CurationCount curationCount = new CurationCount();
                    curationCount.setAssignedCount(userStatistics.getAssignedCount());
                    curationCount.setCuratedCount(userStatistics.getProcessedCount());
                    map.put(userStatistics.getUserId(), curationCount);
                }
            }
        } catch (Exception ex) {
            LOG.error(ex, ex);
            ex.printStackTrace();
        }
        return map;
    }
}
