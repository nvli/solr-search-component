package in.gov.nvli.search.service;

import in.gov.nvli.search.controller.SearchController;
import in.gov.nvli.search.dao.IDCMetadataInformationDAO;
import in.gov.nvli.search.dao.IResourceInfoDAO;
import in.gov.nvli.search.dao.IResourceTypeInfoDAO;
import in.gov.nvli.search.dao.ISearchDAO;
import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.domain.ResourceInfo;
import in.gov.nvli.search.form.SearchForm;
import in.gov.nvli.search.form.SearchResult;
import in.gov.nvli.search.solr.domain.SolrDCMetadataInformation;
import in.gov.nvli.search.util.Constants;
import in.gov.nvli.search.util.FacetNameCountHolder;
import in.gov.nvli.search.util.Helper;
import in.gov.nvli.search.util.ResultHolder;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.SpellCheckResponse;
import org.apache.solr.client.solrj.response.SuggesterResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hemant Anjana
 */
@Service
public class SearchService {

    /**
     * logger instance
     */
    private static final Logger logger = Logger.getLogger(SearchController.class.getName());
    /**
     * reference of ISearchDAO
     */
    @Autowired
    private ISearchDAO searchDAO;
    /**
     * reference of IResourceInfoDAO
     */
    @Autowired
    private IResourceInfoDAO resourceInfoDAO;
    /**
     * reference of IResourceTypeInfoDAO
     */
    @Autowired
    private IResourceTypeInfoDAO resourceTypeInfoDAO;
    /**
     * Autowired {@link IDCMetadataInformationDAO}.
     */
    @Autowired
    private IDCMetadataInformationDAO dCMetadataInformationDAO;
    /**
     * CDN path.
     */
    private String cdnPath;
    /**
     * location where index is created. It value will populated from Property
     * file. (@see WEB-INF/etc/conf/dispatcher-servlet.xml)
     */
    @Value("${nvli.index.folder}")
    private String indexFolder;

    @Value("${ncaa.base.url}")
    private String ncaaBaseURL;

    @Value("${youtube.url}")
    private String youtubeURL;

    @Value("${solr.url}")
    private String solrUrl;

    @Value("${solr.username}")
    private String solrUserName;

    @Value("${solr.password}")
    private String solrPassword;

    /**
     * Basic Search function to return the search result
     *
     * @param searchForm {@link SearchForm} contains parameters related to
     * search.
     * @param pageNumber page number for pagination.
     * @param pageWindow page window for pagination.
     * @param enableExactWordSearch flag to enable or disable exact word search.
     * @param facetRequired default facet required or not.
     * @return {@link SearchForm} with search results.
     * @throws org.apache.lucene.queryParser.ParseException
     * @throws java.sql.SQLException
     */
    public SearchForm doSearch(SearchForm searchForm, String pageNumber, String pageWindow, boolean enableExactWordSearch, boolean facetRequired) throws Exception {
        int pageNum = Integer.parseInt(pageNumber);
        int pageWin = Integer.parseInt(pageWindow);

        //getSubresourceIdentifer based on subResources Name
        Map<String, List<String>> filters = searchForm.getFilters();
        List<String> oldInstitues = null;
        if (filters != null && !filters.isEmpty()) {
            oldInstitues = filters.get(Constants.INSTITUTE);
            if (oldInstitues != null && !oldInstitues.isEmpty()) {
                List<String> newInstitutes = new ArrayList<>();
                for (String name : oldInstitues) {
                    try {
                        ResourceInfo subResourceInformation = resourceInfoDAO.getResourceInfobyName(name);
                        if (subResourceInformation != null) {
                            newInstitutes.add(subResourceInformation.getResourceCode());
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        logger.error(ex, ex);
                    }
                }

                if (!newInstitutes.isEmpty()) {
                    filters.put(Constants.INSTITUTE, newInstitutes);
                }
            }
        }

        searchForm.setFilters(filters);
        if (Helper.RESOURCE_TYPE_CODE == null || Helper.RESOURCE_TYPE_CODE.isEmpty()) {
            Helper.RESOURCE_TYPE_CODE = resourceTypeInfoDAO.list().get(0).getResourceCode();
        }
        searchForm.setResourceCode(Helper.RESOURCE_TYPE_CODE);
        ResultHolder resultHolder = searchDAO.search(searchForm, pageNum, pageWin, enableExactWordSearch, facetRequired);
        List<SolrDCMetadataInformation> dcResults = resultHolder.getDcResults();
        long resultSize = resultHolder.getResultSize();

        List<SearchResult> searchResults = new ArrayList<>();
        if (dcResults != null && !dcResults.isEmpty()) {
            for (SolrDCMetadataInformation dCWrapper : dcResults) {
                cdnPath = Helper.getCDNPath();
                SearchResult searchResult = Helper.createSearchResultFromDC(dCWrapper, searchForm.getResourceCode(), cdnPath, searchForm.getRankingRule(), ncaaBaseURL, youtubeURL);
                searchResults.add(searchResult != null ? searchResult : null);
            }
        }
        //set the values and return it
        searchForm.setResultSize(resultHolder.getResultSize());
        searchForm.setResultFound(resultSize != 0);
        searchForm.setListOfResult(searchResults);
        if (filters != null && !filters.isEmpty() && filters.containsKey(Constants.INSTITUTE)) {
            if (oldInstitues != null && !oldInstitues.isEmpty()) {
                filters.put(Constants.INSTITUTE, oldInstitues);
            }
        }

        searchForm.setFilters(filters);
        //iterate facetmap to give subresource information name instead of its identifier
        if (resultHolder.getFacetMap() != null) {
            List<FacetNameCountHolder> list = resultHolder.getFacetMap().get("facet_institution");
            List<FacetNameCountHolder> listNew = new ArrayList<>();
            if (list != null && !list.isEmpty()) {
                for (FacetNameCountHolder facetNameCountHolder : list) {
                    String subResourceIdentifier = facetNameCountHolder.getName();
                    if (subResourceIdentifier != null && !subResourceIdentifier.isEmpty()) {
                        try {
                            ResourceInfo subResourceInformation = resourceInfoDAO.getResourceInfobyidentifier(subResourceIdentifier);
                            if (subResourceInformation != null) {
                                FacetNameCountHolder facetNameCountHolderNew = new FacetNameCountHolder();
                                facetNameCountHolderNew.setCount(facetNameCountHolder.getCount());
                                facetNameCountHolderNew.setName(subResourceInformation.getName());
                                listNew.add(facetNameCountHolderNew);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            logger.error(ex, ex);
                        }
                    }
                }
            }
            resultHolder.getFacetMap().put("facet_institution", listNew);
        }
        searchForm.setFacetMap(resultHolder.getFacetMap());
        searchForm.setSpecificFacetMap(resultHolder.getSpecificFacetMap());
        return searchForm;
    }

    /**
     * UDC Search method to return the search result.
     *
     * @param searchForm SearchForm.searchElement will contain udc notation.
     * @param pageNumber page number for pagination.
     * @param pageWindow page window for pagination.
     * @return {@link SearchForm} with search results.
     * @throws java.sql.SQLException
     */
    public SearchForm doUDCSearch(SearchForm searchForm, String pageNumber, String pageWindow) throws Exception {
        int pageNum = Integer.parseInt(pageNumber);
        int pageWin = Integer.parseInt(pageWindow);
        if (Helper.RESOURCE_TYPE_CODE == null || Helper.RESOURCE_TYPE_CODE.isEmpty()) {
            Helper.RESOURCE_TYPE_CODE = resourceTypeInfoDAO.list().get(0).getResourceCode();
        }
        searchForm.setResourceCode(Helper.RESOURCE_TYPE_CODE);
        ResultHolder resultHolder = searchDAO.udcSearch(searchForm, pageNum, pageWin);
        List<SolrDCMetadataInformation> dcResults = resultHolder.getDcResults();
        long resultSize = resultHolder.getResultSize();

        List<SearchResult> searchResults = new ArrayList<>();
        if (dcResults != null && !dcResults.isEmpty()) {
            for (SolrDCMetadataInformation dCWrapper : dcResults) {
                cdnPath = Helper.getCDNPath();
                SearchResult searchResult = Helper.createSearchResultFromDC(dCWrapper, searchForm.getResourceCode(), cdnPath, searchForm.getRankingRule(), ncaaBaseURL, youtubeURL);
                searchResults.add(searchResult != null ? searchResult : null);
            }
        }
        //set the values and return it
        searchForm.setResultSize(resultHolder.getResultSize());
        searchForm.setResultFound(resultSize != 0);
        searchForm.setListOfResult(searchResults);
        return searchForm;
    }

    /**
     * Give suggestion based on keywords in DC and MARC meta data.
     *
     * @param searchTerm
     * @return
     */
    public List<String> getSuggestions(final String searchTerm) {
        List<String> suggestions = null;
        QueryResponse response = null;
        try {
            HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
            //Preparing Solr query 
            SolrQuery query = new SolrQuery();
            query.setRequestHandler("/suggest");
            query.set("suggest", "true");
            query.set("suggest.dictionary", "nvliSuggester");
            query.set("suggest.q", searchTerm);
            try {
                response = solr.query(query);
            } catch (Exception e) {
                System.out.println("responce is null");
            }
            if (response != null) {
                SuggesterResponse suggesterResponse = response.getSuggesterResponse();
                if (suggesterResponse != null) {
                    Map<String, List<String>> suggestedTerms = suggesterResponse.getSuggestedTerms();
                    if (suggestedTerms != null && !suggestedTerms.isEmpty()) {
                        suggestions = suggestedTerms.get("nvliSuggester");
                    }
                }
            } else {
                suggestions = new ArrayList<>();
                suggestions.add("Not Avaliable");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Set<String> temp = new HashSet<>(suggestions);
        suggestions.clear();
        suggestions.addAll(temp);
        return suggestions;
    }

    /**
     * Gives Date facet in map. Method give map contains facet name , and list
     * of FacetNameCountHolder.
     *
     * @return Map
     * @throws java.lang.Exception
     */
    public Map<String, List<FacetNameCountHolder>> getDateFacets() throws Exception {
        return searchDAO.getDATEFacet();
    }

    /**
     * Give specific suggestion based on keywords in DC and MARC meta data.
     *
     * @param searchField
     * @param searchTerm
     * @return List of suggestions
     */
    public List<String> getFacetSuggestions(final String searchField, final String searchTerm) {
        List<String> suggestions = null;
        String dictionaryName = "nvliSuggester";
        QueryResponse response = null;
        try {
            HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
            switch (searchField.toLowerCase()) {
                case Constants.SUGGEST_TITLE:
                    dictionaryName = "nvliTitle";
                    break;
                case Constants.SUGGEST_SUBJECT:
                    dictionaryName = "nvliSubject";
                    break;
                case Constants.SUGGEST_CREATOR:
                    dictionaryName = "nvliCreator";
                    break;
                case Constants.SUGGEST_DESCRIPTION:
                    dictionaryName = "nvliDescription";
                    break;
                case Constants.SUGGEST_PUBLISHER:
                    dictionaryName = "nvliPublisher";
                    break;
                case Constants.SUGGEST_F1:
                    dictionaryName = "nvliF1";
                    break;
                case Constants.SUGGEST_F2:
                    dictionaryName = "nvliF2";
                    break;
                case Constants.SUGGEST_F3:
                    dictionaryName = "nvliF3";
                    break;
                case Constants.SUGGEST_F4:
                    dictionaryName = "nvliF4";
                    break;
                case Constants.SUGGEST_F5:
                    dictionaryName = "nvliF5";
                    break;
                case Constants.SUGGEST_F6:
                    dictionaryName = "nvliF6";
                    break;
                case Constants.SUGGEST_F7:
                    dictionaryName = "nvliF7";
                    break;
                case Constants.SUGGEST_F8:
                    dictionaryName = "nvliF8";
                    break;
                case Constants.SUGGEST_F9:
                    dictionaryName = "nvliF9";
                    break;
                case Constants.SUGGEST_F10:
                    dictionaryName = "nvliF10";
                    break;
                default:
                    dictionaryName = "nvliSuggester";
            }
            //Preparing Solr query
            SolrQuery query = new SolrQuery();
            query.setRequestHandler("/suggest");
            query.set("suggest", "true");
            query.set("suggest.dictionary", dictionaryName);
            query.set("suggest.q", searchTerm);
            try {
                response = solr.query(query);
            } catch (Exception e) {
                System.out.println("responce is null");
            }
            if (response != null) {
                SuggesterResponse suggesterResponse = response.getSuggesterResponse();
                if (suggesterResponse != null) {
                    Map<String, List<String>> suggestedTerms = suggesterResponse.getSuggestedTerms();
                    if (suggestedTerms != null && !suggestedTerms.isEmpty()) {
                        suggestions = suggestedTerms.get(dictionaryName);
                    }
                }
            } else {
                suggestions = new ArrayList<>();
                suggestions.add("Not Avaliable");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Set<String> temp = new HashSet<>(suggestions);
        suggestions.clear();
        suggestions.addAll(temp);
        return suggestions;
    }

    /**
     * Give Similar Words based on keywords in DC and MARC meta data.
     *
     * @param searchTerm
     * @return List of suggestions
     */
    public List<String> getDidYouMean(final String searchTerm) {
        List<String> suggestions = null;
        QueryResponse response = null;
        try {
            HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
            //Preparing Solr query
            SolrQuery query = new SolrQuery();
            query.setRequestHandler("/spell");
            query.set("q", searchTerm);
            query.set("spellcheck", "on");
            try {
                response = solr.query(query);
            } catch (Exception e) {
                System.out.println("responce is null");
            }
            if (response != null) {
                SpellCheckResponse spellCheckResponse = response.getSpellCheckResponse();
                if (spellCheckResponse != null) {
                    List<SpellCheckResponse.Suggestion> suggList = spellCheckResponse.getSuggestions();
                    if (suggList != null && !suggList.isEmpty()) {
                        SpellCheckResponse.Suggestion sugg = suggList.get(0);
                        suggestions = sugg.getAlternatives();
                    }
                }
            } else {
                suggestions = new ArrayList<>();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (suggestions == null || suggestions.isEmpty()) {
            suggestions = new ArrayList<>();
        }
        Set<String> temp = new HashSet<>(suggestions);
        suggestions.clear();
        suggestions.addAll(temp);
        return suggestions;
    }

    /**
     * deleting the records from MYSQL and index from SOLR
     *
     * @param recordIdentifier
     * @return
     */
    public boolean deleteIndex(String recordIdentifier) {
        boolean flag = false;
        try {
            //getting the record rom mysql
            List<DCMetadataInformation> dcList = dCMetadataInformationDAO.getRecordByIdentifier(recordIdentifier);
            if (dcList != null && !dcList.isEmpty()) {
                //now deleteing from SOLR
                HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
                solr.setParser(new XMLResponseParser());
                UpdateResponse rsp = solr.deleteById(recordIdentifier);
                solr.commit();
                if (rsp != null && rsp.getStatus() == 0) {
                    //deleting from MYSQL
                    dCMetadataInformationDAO.delete(dcList.get(0));
                    flag = true;
                } else {
                    flag = false;
                }
            } else {
                flag = false;
            }
        } catch (Exception ex) {
            logger.error(ex, ex);
        }
        return flag;
    }

    /**
     * Method do SOLR indexing of {@link DCMetadataInformation} records which
     * are not indexed.
     *
     * @return true if successful else false.
     */
    public boolean dcIndexUpdation() {
        boolean falg = false;
        try {
            List<DCMetadataInformation> dclist = dCMetadataInformationDAO.getRecordsByIndexFlag(false);
            
            System.out.println("Total records avaliable---"+dclist.size());
            if (dclist != null && !dclist.isEmpty()) {
                logger.info("DC records is going to indexed");
                SolrDCMetadataInformation dcSolrObject = null;
                SolrDCMetadataInformation updated = null;
                for (DCMetadataInformation dCMetadataInformation : dclist) {
                    //first check index is avaliable or not in SOLR
                    HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
                    solr.setParser(new XMLResponseParser());
                    SolrDocument solrDc = solr.getById(dCMetadataInformation.getRecordIdentifier());
                    if (solrDc != null) {
                        //update the index  
                        System.out.println("updating new blog---");
                        DocumentObjectBinder binder = new DocumentObjectBinder();
                        SolrDCMetadataInformation goingToupdate= binder.getBean(SolrDCMetadataInformation.class, solrDc);
                        updated = Helper.gettingSolrDocumentFormDCMetadataInformation(dCMetadataInformation,goingToupdate, false);
                        UpdateRequest updateRequest = new UpdateRequest();
                        updateRequest.setAction(UpdateRequest.ACTION.COMMIT, false, false);
                        SolrInputDocument finalUpdatedDocument = binder.toSolrInputDocument(updated);
                        updateRequest.add(finalUpdatedDocument, Boolean.TRUE);
                        UpdateResponse rsp = updateRequest.process(solr);
                        if (rsp != null && rsp.getStatus() == 0) {
                            dCMetadataInformation.setIndexedFlag(true);
                            dCMetadataInformationDAO.merge(dCMetadataInformation);
                            falg = true;
                        }
                    } else {
                        //create the index
                        System.out.println("creating new blog");
                        dcSolrObject = Helper.gettingSolrDocumentFormDCMetadataInformation(dCMetadataInformation, dcSolrObject, true);
                        HttpSolrClient solrSession = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
                        solrSession.setParser(new XMLResponseParser());
                        solrSession.addBean(dcSolrObject);
                        solrSession.commit();
                        dCMetadataInformation.setIndexedFlag(true);
                        dCMetadataInformationDAO.merge(dCMetadataInformation);
                        falg = true;
                    }

                }

            } else {
                logger.info("no records is avaliable for indexed DC");
            }
        } catch (Exception ex) {
            logger.error(ex, ex);
        }
        return falg;
    }
}
