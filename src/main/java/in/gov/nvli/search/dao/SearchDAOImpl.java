package in.gov.nvli.search.dao;

import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.form.SearchComplexityEnum;
import in.gov.nvli.search.form.SearchForm;
import in.gov.nvli.search.solr.domain.SolrDCMetadataInformation;
import in.gov.nvli.search.util.Constants;
import in.gov.nvli.search.util.FacetNameCountHolder;
import in.gov.nvli.search.util.Helper;
import in.gov.nvli.search.util.ResultHolder;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.FacetField.Count;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of Search related function.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 * @see ISearchDAO
 */
@Repository
public class SearchDAOImpl implements ISearchDAO {

    /**
     * Setter method for session factory and it is autowired.
     *
     * @param sessionFactory {@link SessionFactory}
     */
    @Autowired(required = true)
    private SessionFactory sessionFactory;
    /**
     * Logger instance.
     */
    private static final Logger logger = Logger.getLogger(SearchDAOImpl.class.getName());
    /**
     * Constant used for HIGHLIGHTER (start tag)
     */
    public final static String HIGHLIGHTER_CSS_START_TAG = "<mark>";
    /**
     * Constant used for HIGHLIGHTER (end tag)
     */
    public final static String HIGHLIGHTER_CSS_END_TAG = "</mark>";
    /**
     * location where index is created. It value will populated from Property
     * file. (@see WEB-INF/etc/conf/dispatcher-servlet.xml)
     */
    @Value("${nvli.index.folder}")
    private String indexFolder;
    /**
     * Prefix Length constant for fuzzy Query.
     */
    //  @Value("${searchPortal.fuzzyQuery.prefixLength}")
    private final int fuzzyQueryPrefixLength = 0;
    /**
     * Threshold constant for fuzzy Query for basic search.
     */
    //  @Value("${searchPortal.fuzzyQuery.thresholdLimit.basic}")
    private final float fuzzyQueryThresholdBasic = 0.5f;
    /**
     * Threshold constant for fuzzy Query for advance search.
     */
    // @Value("${searchPortal.fuzzyQuery.thresholdLimit.advanced}")
    private final float fuzzyQueryThresholdAdv = 0.8f;
    /**
     * Max Threshold constant for Query.
     */
//    @Value("${searchPortal.fuzzyQuery.thresholdMaxLimit}")
    private final float fuzzyQueryMaxThreshold = 0.999f;

    @Value("${solr.url}")
    private String solrUrl;

    @Value("${solr.username}")
    private String solrUserName;

    @Value("${solr.password}")
    private String solrPassword;

    /**
     * do basic search. Method does basis search or advance search based on
     * {@link SearchComplexityEnum} in {@link  SearchForm}.
     *
     * IF Basic then does keyword or phrase search .If SearchForm.searchElement
     * is double quoted, then does phrase query.
     *
     * @param searchForm {@link  SearchForm}
     * @param pageNumber page number for pagination
     * @param pageWindow page window for pagination
     * @param enableExactWordSearch set true if word exact search in basic
     * search.
     * @return {@link ResultHolder} containing count and results.
     * @throws HibernateException
     * @throws SQLException
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public ResultHolder search(SearchForm searchForm, int pageNumber, int pageWindow, boolean enableExactWordSearch, boolean facetRequired) throws Exception {
        int from = ((pageNumber - 1) * pageWindow);
        int to = pageWindow;
        long resultSize = 0;

        //getting solr server connection
        HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
        solr.setParser(new XMLResponseParser());
        //Preparing Solr query 
        SolrQuery query = new SolrQuery();
        query.setStart(from);
        query.setRows(to);
        query.setHighlight(true);
        query.setHighlightSimplePre(HIGHLIGHTER_CSS_START_TAG);
        query.setHighlightSimplePost(HIGHLIGHTER_CSS_END_TAG);
        query.setIncludeScore(true);
        query.setHighlightFragsize(0);
        query.setHighlightSnippets(10);
        query.set("hl.fl", SolrDCMetadataInformation.getSearchResultHighlightFields());
        
        float thresholdValue;
        int prefixLengh = fuzzyQueryPrefixLength;
        if (enableExactWordSearch) {
            thresholdValue = fuzzyQueryMaxThreshold;
        } else {
            thresholdValue = fuzzyQueryThresholdBasic;
        }

        boolean isPhrase = checkToEnablePhrase(searchForm.getSearchElement());
        //if exact is false compalusary to make exact
        if (!isPhrase) {
            isPhrase = true;
        }

        if (searchForm.getSearchComplexity().equals(SearchComplexityEnum.BASIC)) {
            if (searchForm.getSearchElement() != null && searchForm.getSearchElement().length() > 3) {
                prefixLengh = 3;
            }
        } else {
            prefixLengh = 3;
        }

        List<String> subResourceIdentifiers = searchForm.getSubResourceIdentifiers();
        Map<String, List<String>> filters = searchForm.getFilters();
        Map<String, List<String>> specificFilters = searchForm.getSpecificFilters();
        String fromDateStr = searchForm.getFromDate();
        String toDateStr = searchForm.getToDate();
        Long fromDate = null;
        Long toDate = null;
        if (fromDateStr != null && toDateStr != null && !fromDateStr.trim().isEmpty() && !toDateStr.trim().isEmpty()) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
            try {
                fromDate = sdf.parse(fromDateStr.trim()).getTime();
                toDate = sdf.parse(toDateStr.trim()).getTime();

            } catch (Exception ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
        //Query for dc
        if (searchForm.getSearchComplexity().equals(SearchComplexityEnum.ADVANCED)) {
              query.set("q",getFinalQuery(searchForm.getAdvanceSearchQueryString()));
               if (!facetRequired && filters != null && !filters.isEmpty()) {
                for (String key : filters.keySet()) {
                    List<String> values = filters.get(key);
                    if (Constants.DATE.equalsIgnoreCase(key)) {
                        query.addFilterQuery(getORQuery("facet_date", values));
                    }
                }
            }
        } else if (searchForm.getSearchComplexity().equals(SearchComplexityEnum.BASIC)) {
            if (searchForm.getSearchElement() == null || searchForm.getSearchElement().trim().isEmpty()) {
                query.setQuery("*:*");
            } else if (isPhrase) {
                /**
                 * If phrase enabled, do phrase searching
                 */
                /*"title", "description", "date", "format", "identifier", "subject", "creator", "publisher", "languages"
                 "lccn", "isbn", "source", "type", "contributor", "udcNotation", "customNotation", "otherTagValues", "markTaggingJson", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10"*/
//                Query dcQuery = dcQueryBuilder.phrase().onField("title").andField("").andField("description").andField("date").andField("format").andField("identifier").
//                        andField("subject").andField("creator").andField("publisher").andField("languages").andField("lccn").andField("isbn").andField("source").
//                        andField("type").andField("contributor").andField("udcNotation").andField("customNotation").andField("otherTagValues").andField("markTaggingJson").andField("f1")
//                        .andField("f2").andField("f3").andField("f4").andField("f5").andField("f6").andField("f7").andField("f8").andField("f9").andField("f10").
//                        sentence(searchForm.getSearchElement().toLowerCase()).createQuery();
//                dcFinalQuery = dcQueryBuilder.bool().must(dcQuery).createQuery();
                  String term=removeQuotes(searchForm.getSearchElement());
                 query.set("q",getORQueryMatch(term.toLowerCase(),SolrDCMetadataInformation.getSearchFields(),true));
            } else {
                /**
                 * If phrase disabled, do keyword searching
                 */
//                Query dcQuery = dcQueryBuilder.keyword().fuzzy().withPrefixLength(prefixLengh).withThreshold(thresholdValue).onFields(DCMetadataInformation.getSearchFields()).
//                        matching(searchForm.getSearchElement().toLowerCase()).createQuery();
//                dcFinalQuery = dcQueryBuilder.bool().must(dcQuery).createQuery();
                query.set("q",getORQueryMatch(searchForm.getSearchElement().toLowerCase(),SolrDCMetadataInformation.getSearchFields(),false));
            }
        }

        if (fromDate != null && toDate != null) {
//            Filter periodStartNumericRangeFilter = NumericRangeFilter.newLongRange("periodStart", fromDate, toDate, true, true);
//            FilteredQuery filteredQuery = new FilteredQuery(dcFinalQuery, periodStartNumericRangeFilter);
//            dcFullTextQuery = fullTextSession.createFullTextQuery(filteredQuery, DCMetadataInformation.class);
              query.addFilterQuery("periodStart:["+fromDate+" "+"TO"+" "+toDate+"]");
        } else {
            //dcFullTextQuery = fullTextSession.createFullTextQuery(dcFinalQuery, DCMetadataInformation.class);
        }
        //dcFullTextQuery.setProjection(FullTextQuery.THIS, FullTextQuery.SCORE);

        //adding for subResources
        if (subResourceIdentifiers != null && !subResourceIdentifiers.isEmpty()) {
             System.out.println("inside subresorce=====");
             query.addFilterQuery(getORQuery("subResourceIdentifier", subResourceIdentifiers));
            //dcFullTextQuery.enableFullTextFilter("dcSubResourceIdentifierFilter").setParameter("subResourceIdentifiers", subResourceIdentifiers);
        }

        //variable to hold  previous facet with its name.
        Map<String, List<Count>> previousFacetMap = new LinkedHashMap<>();
        //variable to hold  specific previous facet with its name.
        Map<String, List<Count>> specificPreviousFacetMap = new LinkedHashMap<>();
        if (facetRequired) {
            System.out.println("setting facet Query..");
            //setting facet is true
            query.setFacet(true);
            //facet limit
            query.setFacetLimit(20);
            // less than zero facet value is exclude
            query.setFacetMinCount(1);
            // adding general field
            query.addFacetField(SolrDCMetadataInformation.getFacetFields());
            //adding specific field
            query.addFacetField(SolrDCMetadataInformation.getSpecificFacetFields());
            
            //adding general filters
            if (filters != null && !filters.isEmpty()) {
                System.out.println("aplying general filter ");
                query = applyGeneralFilter(query, filters);
            }
            //adding specific filters
            if (specificFilters != null && !specificFilters.isEmpty()) {
                System.out.println("aplying specific filter ");
                query = applySpecificFilter(query,specificFilters);
            }
            //getting previous facet list
            QueryResponse facetResponse = solr.query(query);
            List<FacetField> ffList = facetResponse.getFacetFields();
            for (FacetField ff : ffList) {
                String ffname = ff.getName();
                int ffcount = ff.getValueCount();
                if (ffcount > 0) {
                    boolean flag = Arrays.asList(SolrDCMetadataInformation.getFacetFields()).contains(ffname);
                    if (flag) {
                        previousFacetMap.put(ffname, ff.getValues());
                    } else {
                        specificPreviousFacetMap.put(ffname, ff.getValues());
                    }

                }
            }
        }

        //adding the general filters
        //Holder genralHolder = applyGeneralFilter(dcQueryBuilder, dcFullTextQuery, previousFacetMap, filters);
        //dcFullTextQuery = genralHolder.getFullTextQuery();
        //previousFacetMap = genralHolder.getPreviousFacetMap();

        //variable to hold  specific previous facet with its name.
        //Map<String, List<Facet>> specificPreviousFacetMap = new LinkedHashMap<>(16);
        //adding the filters
        //Holder specificHolder = applySpecificFilter(dcQueryBuilder, dcFullTextQuery, specificPreviousFacetMap, specificFilters);
        //dcFullTextQuery = specificHolder.getFullTextQuery();
        //specificPreviousFacetMap = specificHolder.getPreviousFacetMap();

        LinkedHashMap<String, List<FacetNameCountHolder>> facetMap = new LinkedHashMap<>();;
        LinkedHashMap<String, List<FacetNameCountHolder>> specificFacetMap = new LinkedHashMap<>();;
        //checking default faceting required or not.
//        if (facetRequired) {
//            //Getting the faceting for selected fields.    
//            for (String fieldName : DCMetadataInformation.getFacetFields()) {
//                List<Facet> dcFacetList = getDiscreteFaceting(dcQueryBuilder, dcFullTextQuery, fieldName);
//                //add previous facet if available otherwise add new facets
//                if (previousFacetMap.containsKey(fieldName)) {
//                    dcFacetList = previousFacetMap.get(fieldName);
//                }
//                if (dcFacetList != null && !dcFacetList.isEmpty()) {
//                    facetMap.put(fieldName, getFacetNameCountHolders(dcFacetList));
//                }
//            }
//
//            //Getting the specific faceting for selected fields.
//            Map<String, String> keyValueMappings = Helper.keyValueMappings;
//            for (String fieldName : DCMetadataInformation.getSpecificFacetFields()) {
//                List<Facet> dcFacetList = getDiscreteFaceting(dcQueryBuilder, dcFullTextQuery, fieldName);
//                //getting the original name from specific faceting map
//                String speciifcValue = keyValueMappings.get(fieldName);
//                //add previous facet if available otherwise add new facets
//                if (specificPreviousFacetMap.containsKey(fieldName)) {
//                    dcFacetList = specificPreviousFacetMap.get(fieldName);
//                }
//                if (dcFacetList != null && !dcFacetList.isEmpty()) {
//                    specificFacetMap.put(speciifcValue, getFacetNameCountHolders(dcFacetList));
//                }
//            }
//        }
        //fire query
        QueryResponse queryResponse = solr.query(query);
        //Storing the results of the query 
        SolrDocumentList docs = queryResponse.getResults();
        List<SolrDCMetadataInformation> foundDocuments = queryResponse.getBeans(SolrDCMetadataInformation.class);

        //List<DCWrapper> dCWrapperResults = dcFullTextQuery.setFirstResult(from).setMaxResults(to).setResultTransformer(Transformers.aliasToBean(DCWrapper.class)).list();
        //dCWrapperResults = highlightDCSearchResult(dCWrapperResults, dcFinalQuery);
        //resultSize = resultSize + dcFullTextQuery.getResultSize();
        //ResultHolder resultHolder = new ResultHolder(dCWrapperResults, Long.valueOf(resultSize), facetMap, specificFacetMap);
        if (facetRequired) {
            List<FacetField> ffList = queryResponse.getFacetFields();
            List<Count> dcFacetList = null;
            for (FacetField ff : ffList) {
                String ffname = ff.getName();
                int ffcount = ff.getValueCount();
                if (ffcount > 0) {
                    boolean flag = Arrays.asList(SolrDCMetadataInformation.getFacetFields()).contains(ffname);
                    if (flag) {
                        if (previousFacetMap.containsKey(ffname)) {
                            dcFacetList = previousFacetMap.get(ffname);
                        }
                        if (dcFacetList != null && !dcFacetList.isEmpty()) {
                            facetMap.put(ffname, getSolrFacetNameCountHolders(dcFacetList));
                        }
                    } else {
                        Map<String, String> keyValueMappings = Helper.keyValueMappings;
                        //getting the original name from specific faceting map
                        String speciifcValue = keyValueMappings.get(ffname);
                        if (specificPreviousFacetMap.containsKey(ffname)) {
                            dcFacetList = specificPreviousFacetMap.get(ffname);
                        }
                        if (dcFacetList != null && !dcFacetList.isEmpty()) {
                            specificFacetMap.put(speciifcValue, getSolrFacetNameCountHolders(dcFacetList));
                        }
                    }

                }
            }
        }
        resultSize = resultSize + docs.getNumFound();
        foundDocuments = highlightDCSearchResult(foundDocuments, queryResponse);
        ResultHolder resultHolder = new ResultHolder(foundDocuments, resultSize, facetMap, specificFacetMap);
        return resultHolder;
    }

    
    private SolrQuery applyGeneralFilter(SolrQuery query, final Map<String, List<String>> filters) throws Exception {
        //adding the filters
        if (filters != null && !filters.isEmpty()) {
            for (String key : filters.keySet()) {
                List<String> values = filters.get(key);
                if (Constants.INSTITUTE.equalsIgnoreCase(key)) {
                    //Holder holder = applyFilter(query, solr, values, "facet_institution", previousFacetMap);
                    //query = holder.getQuery();
                    //previousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("facet_institution", values));
                } else if (Constants.CREATOR.equalsIgnoreCase(key)) {
                    //Holder holder = applyFilter(query, solr, values, "facet_creator", previousFacetMap);
                    //query = holder.getQuery();
                    //previousFacetMap = holder.getPreviousFacetMap();
                     query.addFilterQuery(getORQuery("facet_creator", values));
                } else if (Constants.PUBLISHER.equalsIgnoreCase(key)) {
                    //Holder holder = applyFilter(query, solr, values, "facet_publisher", previousFacetMap);
                    //query = holder.getQuery();
                    //previousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("facet_publisher", values));
                } else if (Constants.CONTRIBUTOR.equalsIgnoreCase(key)) {
                    //Holder holder = applyFilter(query, solr, values, "facet_contributor", previousFacetMap);
                    //query = holder.getQuery();
                    //previousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("facet_contributor", values));
                } else if (Constants.FORMAT.equalsIgnoreCase(key)) {
                    //Holder holder = applyFilter(query, solr, values, "facet_format", previousFacetMap);
                    //query = holder.getQuery();
                    //previousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("facet_format", values));
                } else if (Constants.LANGUAGE.equalsIgnoreCase(key)) {
                    //Holder holder = applyFilter(query, solr, values, "facet_languages", previousFacetMap);
                    //query = holder.getQuery();
                    //previousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("facet_languages", values));
                } else if (Constants.DATE.equalsIgnoreCase(key)) {
                    //Holder holder = applyFilter(query, solr, values, "facet_date", previousFacetMap);
                    //query = holder.getQuery();
                    //previousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("facet_date", values));
                }
            }
        }
        //Holder holderToReturn = new Holder(query, previousFacetMap);
        return query;
    }

    /**
     * Method apply specific filter.
     *
     * @param dcQueryBuilder
     * @param dcFullTextQuery
     * @param specificPreviousFacetMap
     * @param specificFilters
     * @return
     */
    private SolrQuery applySpecificFilter(SolrQuery query,final Map<String, List<String>> specificFilters) throws Exception {
        if (specificFilters != null && !specificFilters.isEmpty()) {
            Map<String, String> valueKeyMappings = Helper.valueKeyMappings;
            for (String key : specificFilters.keySet()) {
                List<String> values = specificFilters.get(key.trim());
                //getting the original name from mapping map.
                String specifcKey = valueKeyMappings.get(key.trim());
                if (Constants.F1.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-1", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-1", values));
                } else if (Constants.F2.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-2", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-2", values));
                } else if (Constants.F3.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-3", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-3", values));
                } else if (Constants.F4.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-4", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-4", values));
                } else if (Constants.F5.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-5", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-5", values));
                } else if (Constants.F6.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-6", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-6", values));
                } else if (Constants.F7.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-7", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-7", values));
                } else if (Constants.F8.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-8", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-8", values));
                } else if (Constants.F9.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-9", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-9", values));
                } else if (Constants.F10.equalsIgnoreCase(specifcKey)) {
                    //Holder holder = applyFilter(query, solr, values, "Field-10", specificPreviousFacetMap);
                    //query = holder.getQuery();
                    //specificPreviousFacetMap = holder.getPreviousFacetMap();
                    query.addFilterQuery(getORQuery("Field-10", values));
                }
            }
        }
        //Holder holderToReturn = new Holder(query, specificPreviousFacetMap);
        return query;
    }
    /**
     * do udc notation search. udc notation to search is taken from
     * {@link SearchForm}
     *
     * @param searchForm {@link SearchForm}
     * @param pageNumber for pagination
     * @param pageWindow for pagination
     * @return {@link ResultHolder}
     * @throws HibernateException
     * @throws SQLException
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public ResultHolder udcSearch(SearchForm searchForm, int pageNumber, int pageWindow) throws Exception {
        int from = ((pageNumber - 1) * pageWindow);
        int to = pageWindow;
        long resultSize = 0;
        //getting solr server connection
        HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
        solr.setParser(new XMLResponseParser());
        //Preparing Solr query 
        SolrQuery query = new SolrQuery();
        query.setStart(from);
        query.setRows(to);
        String[] fields = {"udcNotation"};
        query.set("q",getORQueryMatch(searchForm.getSearchElement().toLowerCase(),fields,true));
        //fire query
        QueryResponse queryResponse = solr.query(query);
        //Storing the results of the query 
        SolrDocumentList docs = queryResponse.getResults();
        List<SolrDCMetadataInformation> foundDocuments = queryResponse.getBeans(SolrDCMetadataInformation.class);
        resultSize = resultSize + docs.getNumFound();
        ResultHolder resultHolder = new ResultHolder(foundDocuments, resultSize, null, null);
        return resultHolder;
    }
    /**
     * To highlight DCMetadataInformation SearchResult.
     * @param foundDocuments List of DCMetadataInformation which are to highlighted
     * @param queryResponse Query used in searching operation, which should be
     * considered for highlighting.
     * @return 
     */
    private List<SolrDCMetadataInformation> highlightDCSearchResult(List<SolrDCMetadataInformation> foundDocuments, QueryResponse queryResponse) {
         if (foundDocuments != null && !foundDocuments.isEmpty()) {
             try {
                 for (SolrDCMetadataInformation dCWrapper : foundDocuments) {
                        SolrDCMetadataInformation dcMetadataInformation=dCWrapper;
                    /*
                     * Iterate over every field that is to be highlighted in
                     * SearchResults. NOTE: Every field should have it's setter
                     * and getter mehtods defined. 
                     * We are using Reflection to get getter and setter method
                     * for every field
                     */
                    for (String field : DCMetadataInformation.getSearchResultHighlightFields()) {
                        /*
                         * Getting getter and setter methods for field.
                         */
                        java.lang.reflect.Method getterMethod = null;
                        java.lang.reflect.Method setterMethod = null;
                        try {
                            getterMethod = dcMetadataInformation.getClass().getMethod("get" + field.toUpperCase().substring(0, 1) + field.substring(1));
                            setterMethod = dcMetadataInformation.getClass().getMethod("set" + field.toUpperCase().substring(0, 1) + field.substring(1), String.class);
                        } catch (SecurityException e) {
                            logger.error("Security exception while trying to fetch getter and setter method of field using reflection. : " + e.getMessage());
                            e.printStackTrace();
                        } catch (NoSuchMethodException e) {
                            logger.error("NoSuchMethodException while trying to fetch getter and setter method of field using reflection. : " + e.getMessage());
                            e.printStackTrace();
                        }
                        /*
                         * Get the text using getter method.
                         */
                        String textTobeHighlighted = null;
                        try {
                            textTobeHighlighted = (String) getterMethod.invoke(dcMetadataInformation);
                        } catch (SecurityException e) {
                            logger.error("SecurityException while trying to execute getter method for field: " + field + " : " + e.getMessage());
                            e.printStackTrace();
                        }
                        if (textTobeHighlighted != null) {
                             List<String> highightSnippets=queryResponse.getHighlighting().get(dcMetadataInformation.getRecordIdentifier()).get(field);
                            if (highightSnippets!=null && !highightSnippets.isEmpty()) {
                                /*
                                 * Set the highlighted text using setter mehtod.
                                 */
                                try {
                                    setterMethod.invoke(dcMetadataInformation, highightSnippets.get(0));
                                } catch (SecurityException e) {
                                    logger.error("SecurityException while trying to execute setter method for field: " + field + " : " + e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        }
                        
                    }
                 }
             } catch (Exception ex) {
                logger.error("Exception in SearchResultDAOImpl.highlightSearchResult() " + ex.getMessage());
                ex.printStackTrace();
           }
         }
         return foundDocuments;
    }
    
    /**
     * Getting the Faceting result of fieldName.
     *
     * @param builder
     * @param fullTextQuery
     * @param fieldName
     * @param startIndex
     * @param maxFacetListCount
     * @return
     */
//    private List<Facet> getDiscreteFaceting(QueryBuilder builder, FullTextQuery fullTextQuery, String fieldName) {
//        List<Facet> facetList = getFacetList(builder, fullTextQuery, fieldName);
//        //this is done to exclude some terms from facets
//        for (Iterator<Facet> iter = facetList.iterator(); iter.hasNext();) {
//            Facet f = iter.next();
//            if (Constants.EXCLUDE_FROM_FACET.contains(f.getValue().trim())) {
//                //if contains then remove from facet list
//                iter.remove();
//            }
//        }
//        return facetList;
//    }

    /**
     * give list of facets for given 'fieldName'
     *
     * @param builder
     * @param fullTextQuery
     * @param fieldName
     * @param startIndex
     * @param maxFacetListCount
     * @return
     */
//    private List<Facet> getFacetList(QueryBuilder builder, FullTextQuery fullTextQuery, String fieldName) {
//
//        //start of the facetting
//        FacetManager facetManager = fullTextQuery.getFacetManager();
//        //get facet list of medium
//        FacetingRequest mediumFacetingRequest = builder.facet().name(fieldName).onField(fieldName).discrete().orderedBy(FacetSortOrder.COUNT_DESC).maxFacetCount(20).includeZeroCounts(false).createFacetingRequest();
//        //enabling the faceting
//        facetManager.enableFaceting(mediumFacetingRequest);
//        List<Facet> facetList = facetManager.getFacets(fieldName);
//        return facetList;
//    }

    /**
     * give list of matching facet for given 'fieldName' based on valueList.
     * Return empty list if no matching facet is found.
     *
     * @param builder QueryBuilder
     * @param fullTextQuery FullTextQuery
     * @param fieldName
     * @param startIndex
     * @param maxFacetListCount
     * @param valueList list of possible values
     * @return
     */
//    private List<Facet> getFacetListHavingValue(QueryBuilder builder, FullTextQuery fullTextQuery, String fieldName, List<String> valueList) {
//        List<Facet> facetList = getFacetList(builder, fullTextQuery, fieldName);
//        List<Facet> requiredFacet = new ArrayList<>();
//        for (String facetValue : valueList) {
//            for (Facet facet : facetList) {
//                if (facet.getValue().trim().equalsIgnoreCase(facetValue.trim())) {
//                    requiredFacet.add(facet);
//                    break;
//                }
//            }
//        }
//        return requiredFacet;
//    }
    /**
     * give list of {@link FacetNameCountHolder} from given facet list. if facet
     * list is null or empty, return empty list of FacetNameCountHolder.
     * @param facetList list of {@link Facet}
     * @return @return list of {@link facetNameCountList}
     */
    private List<FacetNameCountHolder> getSolrFacetNameCountHolders(List<Count> facetList) {
        FacetNameCountHolder facetNameCountHolder = null;
        List<FacetNameCountHolder> facetNameCountList = new ArrayList<>();
        for (Count f : facetList) {
            //this is done to exclude some terms from facets
            if (!Constants.EXCLUDE_FROM_FACET.contains(f.getName().trim())) {
                facetNameCountHolder = new FacetNameCountHolder();
                facetNameCountHolder.setName(f.getName().trim());
                facetNameCountHolder.setCount((int) f.getCount());
                facetNameCountList.add(facetNameCountHolder);
            }
        }
        return facetNameCountList;
    }
    /**
     * give list of {@link FacetNameCountHolder} from given facet list. if facet
     * list is null or empty, return empty list of FacetNameCountHolder.
     *
     * @param facetList list of {@link Facet}
     * @return list of {@link FacetNameCountHolder}
     */
//    private List<FacetNameCountHolder> getFacetNameCountHolders(List<Facet> facetList) {
//        FacetNameCountHolder facetNameCountHolder = null;
//        List<FacetNameCountHolder> facetNameCountList = new ArrayList<>();
//        for (Facet f : facetList) {
//            //this is done to exclude some terms from facets
//            if (!Constants.EXCLUDE_FROM_FACET.contains(f.getValue().trim())) {
//                facetNameCountHolder = new FacetNameCountHolder();
//                facetNameCountHolder.setName(f.getValue().trim());
//                facetNameCountHolder.setCount(f.getCount());
//                facetNameCountList.add(facetNameCountHolder);
//            }
//        }
//        return facetNameCountList;
//    }

    /**
     * Method process given searchQuery and generate search Query suitable for
     * {@link QueryParser}.
     *
     * <p>
     * Process steps:
     * <u>1. Split given search query by space</u>
     * <u>2. Iterate output of step 1 and check whether current term is not
     * among these{AND, OR, NOT}, then lower case it, do fuzzy and add space.
     * Otherwise don't add fuzzy sign. </u>
     * <u>3. replace operator with LUCENC operator </u>
     * </p>
     *
     * @param searchQuery on which processing need to be done.
     * @return
     */
    public String getFinalQuery(String searchQuery) {
        StringBuilder searchQueryBuilder = new StringBuilder();
        for (String queryTerm : searchQuery.split("\\s+")) {
            if (!queryTerm.trim().isEmpty()) {
                if (!queryTerm.equals("AND") && !queryTerm.equals("OR") && !queryTerm.equals("NOT")) {
                    searchQueryBuilder.append(queryTerm.toLowerCase());
                    //searchQueryBuilder.append("~ "); don't need fuzzy
                    searchQueryBuilder.append(" ");
                } else {
                    searchQueryBuilder.append(queryTerm.toLowerCase());
                    searchQueryBuilder.append(" ");
                }
            }
        }

        searchQuery = searchQueryBuilder.toString().trim().toLowerCase();
        //searchQuery = searchQuery.replace(":", ":(");
        searchQuery = searchQuery.replaceAll("&lt;and&gt;", " AND "); //operators must be in capital
        searchQuery = searchQuery.replaceAll("&lt;or&gt;", " OR ");
        searchQuery = searchQuery.replaceAll("&lt;not&gt;", " NOT ");
        searchQuery = searchQuery.replaceAll("<and>", " AND "); //operators must be in capital
        searchQuery = searchQuery.replaceAll("<or>", " OR ");
        searchQuery = searchQuery.replaceAll("<not>", " NOT ");
        searchQuery = searchQuery.replaceAll("udcnotation", "udcNotation");
        searchQuery = searchQuery.replaceAll("field-1", "Field-1");
        searchQuery = searchQuery.replaceAll("field-2", "Field-2");
        searchQuery = searchQuery.replaceAll("field-3", "Field-3");
        searchQuery = searchQuery.replaceAll("field-4", "Field-4");
        searchQuery = searchQuery.replaceAll("field-5", "Field-5");
        searchQuery = searchQuery.replaceAll("field-6", "Field-6");
        searchQuery = searchQuery.replaceAll("field-7", "Field-7");
        searchQuery = searchQuery.replaceAll("field-8", "Field-8");
        searchQuery = searchQuery.replaceAll("field-9", "Field-9");
        searchQuery = searchQuery.replaceAll("field-10", "Field-10");
        //searchQuery = searchQuery + ")";
        return searchQuery;
    }

    /**
     * give date facets from {@link DCMetadataInformation}.
     *
     * @return Map contain only one entry with key : facet_date and value: list
     * of {@link FacetNameCountHolder}
     * @throws java.lang.Exception
     */
    @Override
    public Map<String, List<FacetNameCountHolder>> getDATEFacet() throws Exception{
        Map<String, List<FacetNameCountHolder>> facetMap = new LinkedHashMap<>();
        HttpSolrClient solr = Helper.getSolrSession(solrUrl, solrUserName, solrPassword);
        solr.setParser(new XMLResponseParser());
        //Preparing Solr query 
        SolrQuery query = new SolrQuery();
        //setting facet is true
        query.setFacet(true);
        //facet limit
        query.setFacetLimit(30);
        // less than zero facet value is exclude
        query.setFacetMinCount(1);
        // adding general field
        query.addFacetField("facet_date");
        query.setQuery("*:*");
        //fire query
        QueryResponse queryResponse = solr.query(query);
        //Storing the results of the query 
        List<FacetField> ffList = queryResponse.getFacetFields();
        if(ffList!=null && !ffList.isEmpty()){
           facetMap.put("facet_date", getSolrFacetNameCountHolders(ffList.get(0).getValues()));
        }
//        Session session = sessionFactory.getCurrentSession();
//        FullTextSession fullTextSession = Search.getFullTextSession(session);
//        FullTextQuery dcFullTextQuery = null;
//        Query dcQuery = null;
//        QueryBuilder dcQueryBuilder = fullTextSession.getSearchFactory().buildQueryBuilder().forEntity(DCMetadataInformation.class).get();
//        //Query for dc
//        dcQuery = dcQueryBuilder.all().createQuery();
//        dcFullTextQuery = fullTextSession.createFullTextQuery(dcQuery, DCMetadataInformation.class);
//        List<Facet> facetList = getDiscreteFaceting(dcQueryBuilder, dcFullTextQuery, "facet_date");
        
        return facetMap;
    }
    /**
     * check whether given string is quoted. Check only start- with and
     * ends-with for double quote.
     *
     * @param searchTerm in which quote is performed
     * @return
     */
    public boolean checkToEnablePhrase(String searchTerm) {
        if (searchTerm == null || searchTerm.trim().isEmpty()) {
            return false;
        } else {
            String quote = "\"";
            return searchTerm.startsWith(quote) && searchTerm.endsWith(quote);
        }
    }
    
      public String removeQuotes(String searchTerm) {
            String quote = "\"";
            if(searchTerm.startsWith(quote) && searchTerm.endsWith(quote)){
               searchTerm=searchTerm.replaceAll(quote,"");
            }
             return searchTerm;
    }

    /**
     * Method apply filter on given filterName using facetName
     *
     * @param dcQueryBuilder
     * @param dcFullTextQuery
     * @param values
     * @param facetName
     * @param filterName
     * @param previousFacetMap
     * @return
     */
    private Holder applyFilter(SolrQuery query, HttpSolrClient solr, List<String> values, final String facetName, Map<String, List<Count>> previousFacetMap) throws Exception {
        List<Count> facets = getFacetListHavingValue(query, solr, facetName, values);
        if (facets != null && !facets.isEmpty()) {
            //holding previous facet
            List<Count> previousFacet = getDiscreteFaceting(query, solr, facetName);
            if (previousFacet != null && !previousFacet.isEmpty()) {
                previousFacetMap.put(facetName, previousFacet);
            }
            query.addFilterQuery(getORQuery(facetName, values));
        }
        Holder holder = new Holder(query, previousFacetMap);
        return holder;

    }
    /**
     * Getting the Faceting result of fieldName.
     *
     * @param builder
     * @param fullTextQuery
     * @param fieldName
     * @param startIndex
     * @param maxFacetListCount
     * @return
     */
    private List<Count> getDiscreteFaceting(SolrQuery query, HttpSolrClient solr, String fieldName) throws Exception {
        List<Count> facetList = getFacetList(query, solr, fieldName);
        //this is done to exclude some terms from facets
        if (facetList != null && !facetList.isEmpty()) {
            for (Iterator<Count> iter = facetList.iterator(); iter.hasNext();) {
                Count f = iter.next();
                if (Constants.EXCLUDE_FROM_FACET.contains(f.getName().trim())) {
                    //if contains then remove from facet list
                    iter.remove();
                }
            }
        }
        return facetList;
    }
    private String getORQuery(String filterName, List<String> values) {
        String query = null;
        if (values != null && !values.isEmpty()) {
            query = filterName + Constants.SOLR_SEPRATOR + "(";
            StringBuffer sb = new StringBuffer();
            sb.append(query);
            for (String v : values) {
                sb.append("\"" + v + "\"" + " " + Constants.SOLR_OR);
            }
            query = sb.toString();
            query = query.substring(0, query.length() - 2);
            query = query + " )";
        }
        return query;
    }
    
    private String getORQueryMatch(String searchTerm,String[] searchFields,boolean isExact){
        String query = null;
        if (searchTerm != null && !searchTerm.isEmpty()) {
            query ="(";
            StringBuffer sb = new StringBuffer();
            sb.append(query);
            for(String field:searchFields){
              if(isExact){  
                sb.append(field+Constants.SOLR_SEPRATOR+"\"" + searchTerm.trim() + "\"" + " " + Constants.SOLR_OR+" " );
              }else{
                sb.append(field+Constants.SOLR_SEPRATOR+"\"" +searchTerm.trim()+"\""+"~10"+" " + Constants.SOLR_OR+ " ");
              }
            }
            query = sb.toString();
            query = query.substring(0, query.length() - 3);
            query = query + ")";
        }
        System.out.println("Query---"+query);
        return query;
    }
    
    /**
     * give list of matching facet for given 'fieldName' based on valueList.
     * Return empty list if no matching facet is found.
     *
     * @param builder QueryBuilder
     * @param fullTextQuery FullTextQuery
     * @param fieldName
     * @param startIndex
     * @param maxFacetListCount
     * @param valueList list of possible values
     * @return
     */
    private List<Count> getFacetListHavingValue(SolrQuery query, HttpSolrClient solr, String fieldName, List<String> valueList) throws Exception {
        List<Count> facetList = getFacetList(query, solr, fieldName);
        List<Count> requiredFacet = new ArrayList<>();
        for (String facetValue : valueList) {
            for (Count facet : facetList) {
                if (facet.getName().trim().equalsIgnoreCase(facetValue.trim())) {
                    requiredFacet.add(facet);
                    break;
                }
            }
        }
        return requiredFacet;
    }
    
    /**
     * give list of facets for given 'fieldName'
     *
     * @param builder
     * @param fullTextQuery
     * @param fieldName
     * @param startIndex
     * @param maxFacetListCount
     * @return
     */
    private List<Count> getFacetList(SolrQuery query, HttpSolrClient solr, String fieldName) throws Exception {
        List<Count> facetList = null;
//         if(oneTimeFacets!=null && !oneTimeFacets.isEmpty()){
//           facetList=oneTimeFacets.get(fieldName);
//         }
//        
//        //start of the facetting
        query.setFacet(true);
        //facet limit
        query.setFacetLimit(20);
        // less than zero facet value is exclude
        query.setFacetMinCount(1);
        //seeting facet field
        query.addFacetField(fieldName);
        QueryResponse facetResponse = solr.query(query);
        List<FacetField> ffList = facetResponse.getFacetFields();
        for (FacetField ff : ffList) {
            facetList = ff.getValues();
        }

        return facetList;
    }

    /**
     * Method apply filter on given filterName using facetName
     *
     * @param dcQueryBuilder
     * @param dcFullTextQuery
     * @param values
     * @param facetName
     * @param filterName
     * @param previousFacetMap
     * @return
     */
//    private Holder applyFilter(QueryBuilder dcQueryBuilder, FullTextQuery dcFullTextQuery, List<String> values, final String facetName, final String filterName, Map<String, List<Facet>> previousFacetMap) {
//        List<Facet> facets = getFacetListHavingValue(dcQueryBuilder, dcFullTextQuery, facetName, values);
//        if (facets != null && !facets.isEmpty()) {
//            //holding previous facet
//            List<Facet> previousFacet = getDiscreteFaceting(dcQueryBuilder, dcFullTextQuery, facetName);
//            if (previousFacet != null && !previousFacet.isEmpty()) {
//                previousFacetMap.put(facetName, previousFacet);
//            }
//            dcFullTextQuery.enableFullTextFilter(filterName).setParameter("facets", facets);
//        }
//        Holder holder = new Holder(dcFullTextQuery, previousFacetMap);
//        return holder;
//
//    }

    /**
     * Method apply general filters.
     *
     * @param dcQueryBuilder
     * @param dcFullTextQuery
     * @param previousFacetMap
     * @param filters
     * @return
     */
//    private Holder applyGeneralFilter(final QueryBuilder dcQueryBuilder, FullTextQuery dcFullTextQuery, Map<String, List<Facet>> previousFacetMap, final Map<String, List<String>> filters) {
//
//        //adding the filters
//        if (filters != null && !filters.isEmpty()) {
//            for (String key : filters.keySet()) {
//                List<String> values = filters.get(key);
//                if (Constants.INSTITUTE.equalsIgnoreCase(key)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "facet_institution", "dcSearchWithSearchFilter", previousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    previousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.CREATOR.equalsIgnoreCase(key)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "facet_creator", "dcCreatorFilter", previousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    previousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.PUBLISHER.equalsIgnoreCase(key)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "facet_publisher", "dcPublisherFilter", previousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    previousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.CONTRIBUTOR.equalsIgnoreCase(key)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "facet_contributor", "dcContributorFilterFilter", previousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    previousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.FORMAT.equalsIgnoreCase(key)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "facet_format", "dcFormatFilter", previousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    previousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.LANGUAGE.equalsIgnoreCase(key)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "facet_languages", "dcLanguageFilter", previousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    previousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.DATE.equalsIgnoreCase(key)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "facet_date", "dcDateFilter", previousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    previousFacetMap = holder.getPreviousFacetMap();
//                }
//            }
//        }
//        Holder holderToReturn = new Holder(dcFullTextQuery, previousFacetMap);
//        return holderToReturn;
//    }

    /**
     * Method apply specific filter.
     *
     * @param dcQueryBuilder
     * @param dcFullTextQuery
     * @param specificPreviousFacetMap
     * @param specificFilters
     * @return
     */
//    private Holder applySpecificFilter(final QueryBuilder dcQueryBuilder, FullTextQuery dcFullTextQuery, Map<String, List<Facet>> specificPreviousFacetMap, final Map<String, List<String>> specificFilters) {
//        if (specificFilters != null && !specificFilters.isEmpty()) {
//            Map<String, String> valueKeyMappings = Helper.valueKeyMappings;
//            for (String key : specificFilters.keySet()) {
//                List<String> values = specificFilters.get(key.trim());
//                //getting the original name from mapping map.
//                String specifcKey = valueKeyMappings.get(key.trim());
//                if (Constants.F1.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-1", "dcField-1Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F2.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-2", "dcField-2Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F3.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-3", "dcField-3Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F4.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-4", "dcField-4Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F5.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-5", "dcField-5Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F6.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-6", "dcField-6Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F7.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-7", "dcField-7Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F8.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-8", "dcField-8Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F9.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-9", "dcField-9Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                } else if (Constants.F10.equalsIgnoreCase(specifcKey)) {
//                    Holder holder = applyFilter(dcQueryBuilder, dcFullTextQuery, values, "Field-10", "dcField-10Filter", specificPreviousFacetMap);
//                    dcFullTextQuery = holder.getFullTextQuery();
//                    specificPreviousFacetMap = holder.getPreviousFacetMap();
//                }
//            }
//        }
//        Holder holderToReturn = new Holder(dcFullTextQuery, specificPreviousFacetMap);
//        return holderToReturn;
//    }

    /**
     * Class to hold full text query and previousFacetMap.
     *
     */
    private class Holder {

        /**
         * FullTextQuery.
         */
        private final SolrQuery solrQuery;
        /**
         * Facet Map.
         */
        private final Map<String, List<Count>> previousFacetMap;

        public Holder(SolrQuery solrQuery, Map<String, List<Count>> previousFacetMap) {
            this.solrQuery = solrQuery;
            this.previousFacetMap = previousFacetMap;
        }

        public SolrQuery getQuery() {
            return solrQuery;
        }

        public Map<String, List<Count>> getPreviousFacetMap() {
            return previousFacetMap;
        }

    }
}
