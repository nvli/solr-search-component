package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.GenericDAOImpl;
import in.gov.nvli.search.domain.ResourceTypeInfo;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * DAO implementation for ResourceTypeInfo.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 * @see GenericDAOImpl
 * @see ResourceTypeInfo
 * @see IResourceTypeInfoDAO
 */
@Repository
public class ResourceTypeInfoDAOImpl extends GenericDAOImpl<ResourceTypeInfo, Long> implements IResourceTypeInfoDAO {

    /**
     * Setter method for session factory and it is autowired.
     *
     * @param sessionFactory {@link SessionFactory}
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
