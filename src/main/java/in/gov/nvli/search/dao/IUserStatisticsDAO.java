package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.IGenericDAO;
import in.gov.nvli.search.domain.UserStatistics;
import java.util.List;

/**
 *
 * Interface of function declaration of UserStatistics related.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 * @see IGenericDAO
 * @see IUserStatisticsDAO
 * @see UserStatistics
 */
public interface IUserStatisticsDAO extends IGenericDAO<UserStatistics, Long> {

    /**
     * give count to assigned record in given subResourceCode.
     *
     * @param subResourceCode whose count need to find.
     * @return count of assigned records.
     * @throws Exception
     */
    public Long getTotalAssignedCount(final String subResourceCode) throws Exception;

    /**
     * Method give {@link UserStatistics} for given sub-resource code and given
     * user id.
     *
     * @param subResourceCode whose statistics need to find.
     * @param userId whose statistics need to find.
     * @return {@link UserStatistics}
     * @throws Exception
     */
    public UserStatistics getUserStatisticsByUserId_SubRes(final String subResourceCode, Long userId) throws Exception;

    /**
     * Method give list of {@link UserStatistics} for given userIDs.
     *
     * @param userIds whose
     * @return list of {@link UserStatistics}
     * @throws Exception
     */
    public List<UserStatistics> getUserStatisticsByUserIds(List<Long> userIds) throws Exception;

    /**
     * Method give list of sub-resource assigned to given user id.
     *
     * @param userId whose assigned sub-resource need to find.
     * @return list of sub-resource code
     * @throws Exception
     */
    public List<String> getAssingedSubResouceIdentifers(Long userId) throws Exception;

    /**
     * Method give list of {@link UserStatistics} for given sub-resource code.
     *
     * @param subResourceCode whose statistics need to be find.
     * @return list of {@link UserStatistics}
     * @throws Exception
     */
    public List<UserStatistics> getStatisticsBySubResourceCode(final String subResourceCode) throws Exception;
}
