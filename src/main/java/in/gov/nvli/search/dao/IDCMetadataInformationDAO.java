package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.IGenericDAO;
import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.form.CurationStatus;
import in.gov.nvli.search.util.CurationResultHolder;
import java.util.List;

/**
 * Interface of function declaration of DCMetadataInformation related.
 *
 * @author Saurabh Koriya
 * @see DCMetadataInformation
 * @see IGenericDAO
 * @version 1
 * @since 1
 */
public interface IDCMetadataInformationDAO extends IGenericDAO<DCMetadataInformation, Long> {

    /**
     * Method can be used to bulk update 'indexedFlag' field of
     * {@link DCMetadataInformation} to given flag argument. It Uses Criteria
     * API of hibernate and fires query.
     *
     * @param flag: true if want to find records that are indexed else false.
     * @return true if successful else false.
     * @throws Exception
     */
    public Boolean changeIndexFlag(boolean flag) throws Exception;

    /**
     * Method give list of {@link DCMetadataInformation} based on given flag
     * against indexedFlag of {@link DCMetadataInformation}. Using this method,
     * can get list of unindexed records as well as indexed records.
     *
     * @param flag true : want to find indexed records; false: want to find
     * unindexed records.
     * @return List of {@link DCMetadataInformation}
     * @throws Exception
     */
    public List<DCMetadataInformation> getRecordsByIndexFlag(boolean flag) throws Exception;


    /**
     * give count of undistributed uncurated records of given sub-resource Code.
     *
     * @param subResourceCode
     * @return count of undistributed uncurated records
     * @throws Exception
     */
    public List<DCMetadataInformation> getList_UNDISTRU_UNCURA(final String subResourceCode) throws Exception;

    /**
     * get list of uncurated records assinged to given userids for given
     * sub-resource code. Method gives list of uncurated records that were
     * assigned to given userids of given sub-resource.
     *
     * @param subResourceCode
     * @param userIds list of user ids
     * @return List of {@link DCMetadataInformation} records
     * @throws Exception
     */
    public List<DCMetadataInformation> getList_DISTRI_UNCURA(final String subResourceCode, List<Long> userIds) throws Exception;

    /**
     * Method gives {@link  CurationResultHolder} containing results and total
     * count based on given parameters.
     *
     * @param curationStatus {@link CurationStatus}
     * @param userId whose records need to find
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @param languages
     * @param searchTerm
     * @param subResouceCodes
     * @return {@link  CurationResultHolder} containing results and total count.
     * @throws Exception
     */
    public CurationResultHolder getListByStatus_userId(CurationStatus curationStatus, Long userId, int pageNum, int pageWin, List<String> languages, String searchTerm, List<String> subResouceCodes) throws Exception;

    /**
     * give count of undistributed uncurated records of given sub-resource Code.
     *
     * @param subResouceCode
     * @return count of undistributed uncurated records
     * @throws Exception
     */
    public Long getCount_UNDISTRU_UNCURA(final String subResouceCode) throws Exception;

    /**
     * Method gives distinct languages available in database.
     *
     * @return list of distinct languages.
     * @throws Exception
     */
    public List<String> getDistinctLang() throws Exception;

    /**
     * give count of records that are curation enabled.
     *
     * @param subResourceCode
     * @return
     * @throws java.lang.Exception
     */
    public Long getTotalCountForCuration(final String subResourceCode) throws Exception;
    /**
     * Method gives list of {@link DCMetadataInformation} for given
     * recordIdentifier.
     *
     * @param recordIdentifier
     * @return list of {@link DCMetadataInformation}
     * @throws Exception
     */
    public List<DCMetadataInformation> getRecordByIdentifier(final String recordIdentifier) throws Exception;
}
