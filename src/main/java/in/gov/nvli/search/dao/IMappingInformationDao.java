package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.IGenericDAO;
import in.gov.nvli.search.domain.MappingInformation;

/**
 * Interface of Mapping Information
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 * @see IGenericDAO
 * @see MappingInformation
 */
public interface IMappingInformationDao extends IGenericDAO<MappingInformation, Long> {

}
