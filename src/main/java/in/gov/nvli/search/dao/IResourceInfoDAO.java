package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.IGenericDAO;
import in.gov.nvli.search.domain.ResourceInfo;

/**
 * Interface of DAO for ResourceInfo.
 *
 * @author Hemant Anjana
 * @see IGenericDAO
 * @see ResourceInfo
 * @version 1
 * @since 1
 */
public interface IResourceInfoDAO extends IGenericDAO<ResourceInfo, Long> {

    /**
     * Method gives {@link ResourceInfo} by given identifier. Method uses given
     * identifier to get ResourceInfo by matching against resourceCode of
     * {@link ResourceInfo}.
     *
     * @param identifier
     * @return {@link ResourceInfo}
     * @throws Exception
     */
    public ResourceInfo getResourceInfobyidentifier(String identifier) throws Exception;

    /**
     * Method gives {@link ResourceInfo} by given name. Method uses given
     * identifier to get ResourceInfo by matching against name of
     * {@link ResourceInfo}.
     *
     * @param name
     * @return {@link ResourceInfo}
     * @throws Exception
     */
    public ResourceInfo getResourceInfobyName(String name) throws Exception;
}
