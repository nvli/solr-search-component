package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.IGenericDAO;
import in.gov.nvli.search.domain.ResourceTypeInfo;

/**
 * Interface for ResourceTypeInfo DAO.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 * @see ResourceTypeInfo
 * @see IGenericDAO
 */
public interface IResourceTypeInfoDAO extends IGenericDAO<ResourceTypeInfo, Long> {
}
