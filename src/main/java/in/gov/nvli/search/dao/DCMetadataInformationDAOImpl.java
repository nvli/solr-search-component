package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.GenericDAOImpl;
import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.form.CurationStatus;
import in.gov.nvli.search.util.CurationResultHolder;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.*;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of DCMetadataInformation related function.
 *
 * @author Saurabh Koriya
 * @see GenericDAOImpl
 * @see IDCMetadataInformationDAO
 * @see DCMetadataInformation
 * @version 1
 * @since 1
 */
@Repository
public class DCMetadataInformationDAOImpl extends GenericDAOImpl<DCMetadataInformation, Long> implements IDCMetadataInformationDAO {

    /**
     * Logger instance.
     */
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DCMetadataInformationDAOImpl.class.getName());

    /**
     * Setter method for session factory and it is autowired.
     *
     * @param sessionFactory {@link SessionFactory}
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    /**
     * Method can be used to bulk update 'indexedFlag' field of
     * {@link DCMetadataInformation} to given flag argument. It Uses Criteria
     * API of hibernate and fires query.
     *
     * @param flag: true if want to find records that are indexed else false.
     * @return true if successful else false.
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Boolean changeIndexFlag(boolean flag) throws Exception {
        Boolean index = false;
        ScrollableResults results = null;
        Session session = currentSession();
        Criteria dcMetadataInformationCriteria = session.createCriteria(DCMetadataInformation.class);
        results = dcMetadataInformationCriteria.add(Restrictions.eq("indexedFlag", flag)).scroll(ScrollMode.SCROLL_SENSITIVE);
        int count = 0;
        try {
            if (results != null && results.first()) {
                results.previous();
                while (results.next()) {
                    DCMetadataInformation record = (DCMetadataInformation) results.get(0);
                    record.setIndexedFlag(true);
                    session.merge(record);
                    if (++count % 500 == 0) {
                        //after 20 records flush the changes to database.
                        session.flush();
                        session.clear();
                    }

                }
                index = true;
            } else {
                index = false;
            }

        } catch (Exception e) {
            index = false;
            throw e;
        }
        return index;
    }

    /**
     * Method give list of {@link DCMetadataInformation} based on given flag
     * against indexedFlag of {@link DCMetadataInformation}. Using this method,
     * can get list of unindexed records as well as indexed records.
     *
     * @param flag true : want to find indexed records; false: want to find
     * unindexed records.
     * @return List of {@link DCMetadataInformation}
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public List<DCMetadataInformation> getRecordsByIndexFlag(boolean flag) throws Exception {
        List<DCMetadataInformation> list = null;
        Session session = currentSession();
        Criteria dcMetadataInformationCriteria = session.createCriteria(DCMetadataInformation.class);
        list = dcMetadataInformationCriteria.add(Restrictions.eq("indexedFlag", flag)).list();
        return list;
    }

   
    /**
     * give count of undistributed uncurated records of given sub-resource Code.
     *
     * @param subResouceCode
     * @return count of undistributed uncurated records
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Long getCount_UNDISTRU_UNCURA(final String subResouceCode) throws Exception {
        long resultCount = 0;
        Session session = currentSession();
        resultCount = (Long) session.createCriteria(DCMetadataInformation.class).
                add(Restrictions.eq("curationStatus", CurationStatus.UNCURATED)).
                add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                add(Restrictions.eq("assignedTo", 0l)).
                add(Restrictions.eq("subResourceIdentifier", subResouceCode)).
                setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();
        return resultCount;
    }

    /**
     * give count of records that are curation enabled for given sub-resource
     * code.
     *
     * @return give count of records that are curation enabled
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Long getTotalCountForCuration(final String subResourceCode) throws Exception {
        long resultCount = 0;
        Session session = currentSession();
        resultCount = (Long) session.createCriteria(DCMetadataInformation.class).
                add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                add(Restrictions.eq("subResourceIdentifier", subResourceCode)).
                setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();
        return resultCount;
    }

    /**
     * get list of unassigned and un-curated records for given sub-resource
     * code.
     *
     * @param subResourceCode
     * @return List of {@link DCMetadataInformation} records that are unassigned
     * and un-curated for given sub-resource.
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public List<DCMetadataInformation> getList_UNDISTRU_UNCURA(final String subResourceCode) throws Exception {
        Session session = currentSession();
        Criteria dcCriteria = session.createCriteria(DCMetadataInformation.class);
        List<DCMetadataInformation> dcList = dcCriteria.add(Restrictions.eq("curationStatus", CurationStatus.UNCURATED)).
                add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                add(Restrictions.eq("subResourceIdentifier", subResourceCode)).
                add(Restrictions.eq("assignedTo", 0l)).list();
        return dcList;
    }

    /**
     * get list of uncurated records assinged to given userids for given
     * sub-resource code. Method gives list of uncurated records that were
     * assigned to given userids of given sub-resource.
     *
     * @param subResourceCode
     * @param userIds list of user ids
     * @return List of {@link DCMetadataInformation} records
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public List<DCMetadataInformation> getList_DISTRI_UNCURA(final String subResourceCode, List<Long> userIds) throws Exception {
        Session session = currentSession();
        Criteria dcCriteria = session.createCriteria(DCMetadataInformation.class);
        List<DCMetadataInformation> dcList = dcCriteria.add(Restrictions.eq("curationStatus", CurationStatus.UNCURATED)).
                add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                add(Restrictions.eq("subResourceIdentifier", subResourceCode)).
                add(Restrictions.in("assignedTo", userIds)).list();
        return dcList;
    }

    /**
     * Method gives {@link  CurationResultHolder} containing results and total
     * count based on given parameters.
     *
     * @param curationStatus {@link CurationStatus}
     * @param userId whose records need to find
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @param languages
     * @param searchTerm
     * @param subResourceIdentifiers
     * @return {@link  CurationResultHolder} containing results and total count.
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public CurationResultHolder getListByStatus_userId(CurationStatus curationStatus, Long userId, int pageNum, int pageWin, List<String> languages, String searchTerm, List<String> subResourceIdentifiers) throws Exception {
        CurationResultHolder curationResultHolder = null;
        long resultCount = 0L;
        List list = null;
        int from = ((pageNum - 1) * pageWin);
        int to = pageWin;
        Session session = currentSession();
        Criteria dcCriteria = session.createCriteria(DCMetadataInformation.class);
        List<Criterion> restrictionsList = null;
        if (languages != null && !languages.isEmpty()) {
            restrictionsList = new ArrayList<>();
            for (String lang : languages) {
                restrictionsList.add(Restrictions.like("allowedLanguages", lang, MatchMode.ANYWHERE));
            }
        }
        List<Criterion> restrictionsListTerm = new ArrayList<>();
        if (searchTerm == null || searchTerm.trim().isEmpty()) {
            restrictionsListTerm.add(Restrictions.isNull("title"));
            restrictionsListTerm.add(Restrictions.isNotNull("title"));
        } else {
            restrictionsListTerm.add(Restrictions.ilike("title", searchTerm, MatchMode.ANYWHERE));
        }
        if (restrictionsList != null && !restrictionsList.isEmpty()) {
            list = dcCriteria.add(Restrictions.eq("curationStatus", curationStatus)).
                    add(Restrictions.eq("assignedTo", userId)).add(Restrictions.or(restrictionsList.toArray(new Criterion[restrictionsList.size()]))).
                    add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                    add(Restrictions.or(restrictionsListTerm.toArray(new Criterion[restrictionsListTerm.size()]))).
                    add(Restrictions.in("subResourceIdentifier", subResourceIdentifiers)).
                    setFirstResult(from).setMaxResults(to).list();

            resultCount = (Long) session.createCriteria(DCMetadataInformation.class).
                    add(Restrictions.eq("curationStatus", curationStatus)).add(Restrictions.eq("assignedTo", userId)).
                    add(Restrictions.or(restrictionsList.toArray(new Criterion[restrictionsList.size()]))).
                    add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                    add(Restrictions.or(restrictionsListTerm.toArray(new Criterion[restrictionsListTerm.size()]))).
                    add(Restrictions.in("subResourceIdentifier", subResourceIdentifiers)).
                    setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();
        } else {
            list = dcCriteria.add(Restrictions.eq("curationStatus", curationStatus)).
                    add(Restrictions.eq("assignedTo", userId)).
                    add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                    add(Restrictions.or(restrictionsListTerm.toArray(new Criterion[restrictionsListTerm.size()]))).
                    add(Restrictions.in("subResourceIdentifier", subResourceIdentifiers)).
                    setFirstResult(from).setMaxResults(to).list();

            resultCount = (Long) session.createCriteria(DCMetadataInformation.class).
                    add(Restrictions.eq("curationStatus", curationStatus)).
                    add(Restrictions.eq("assignedTo", userId)).
                    add(Restrictions.eq("crowdSourceFlag", Boolean.FALSE)).
                    add(Restrictions.or(restrictionsListTerm.toArray(new Criterion[restrictionsListTerm.size()]))).
                    add(Restrictions.in("subResourceIdentifier", subResourceIdentifiers)).
                    setProjection(org.hibernate.criterion.Projections.count("id")).uniqueResult();
        }
        curationResultHolder = new CurationResultHolder(list, resultCount);
        return curationResultHolder;
    }

    /**
     * Method gives distinct languages available in database.
     *
     * @return list of distinct languages.
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public List<String> getDistinctLang() throws Exception {
        List<String> list = null;
        Session session = currentSession();
        Criteria dcCriteria = session.createCriteria(DCMetadataInformation.class);
        list = dcCriteria.setProjection(Projections.distinct(Projections.property("allowedLanguages"))).list();
        return list;
    }


    /**
     * Method gives list of {@link DCMetadataInformation} for given
     * recordIdentifier.
     *
     * @param recordIdentifier
     * @return list of {@link DCMetadataInformation}
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<DCMetadataInformation> getRecordByIdentifier(String recordIdentifier) throws Exception {
        List<DCMetadataInformation> list = null;
        Session session = currentSession();
        Criteria dcMetadataInformationCriteria = session.createCriteria(DCMetadataInformation.class);
        list = dcMetadataInformationCriteria.add(Restrictions.eq("recordIdentifier", recordIdentifier)).list();
        return list;
    }
}
