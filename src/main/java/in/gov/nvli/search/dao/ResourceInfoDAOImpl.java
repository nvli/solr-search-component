package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.GenericDAOImpl;
import in.gov.nvli.search.domain.ResourceInfo;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO implementation for ResourceInfo.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 * @see ResourceInfo
 * @see GenericDAOImpl
 * @see IResourceInfoDAO
 */
@Repository
public class ResourceInfoDAOImpl extends GenericDAOImpl<ResourceInfo, Long> implements IResourceInfoDAO {

    /**
     * Setter method for session factory and it is autowired.
     *
     * @param sessionFactory {@link SessionFactory}
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    /**
     * Method gives {@link ResourceInfo} by given identifier. Method uses given
     * identifier to get ResourceInfo by matching against resourceCode of
     * {@link ResourceInfo}.
     *
     * @param identifier
     * @return {@link ResourceInfo}
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public ResourceInfo getResourceInfobyidentifier(String identifier) throws Exception {
        ResourceInfo subresourceInformation = null;
        Session session = currentSession();
        Criteria subResourceCriteria = session.createCriteria(ResourceInfo.class);
        @SuppressWarnings("unchecked")
        List<ResourceInfo> list = subResourceCriteria.add(Restrictions.eq("resourceCode", identifier.trim())).list();
        if (!list.isEmpty()) {
            subresourceInformation = list.get(0);
        }
        return subresourceInformation;
    }

    /**
     * Method gives {@link ResourceInfo} by given name. Method uses given
     * identifier to get ResourceInfo by matching against name of
     * {@link ResourceInfo}.
     *
     * @param name
     * @return {@link ResourceInfo}
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public ResourceInfo getResourceInfobyName(String name) throws Exception {
        ResourceInfo subresourceInformation = null;
        Session session = currentSession();
        Criteria subResourceCriteria = session.createCriteria(ResourceInfo.class);
        @SuppressWarnings("unchecked")
        List<ResourceInfo> list = subResourceCriteria.add(Restrictions.eq("name", name.trim())).list();
        if (!list.isEmpty()) {
            subresourceInformation = list.get(0);
        }
        return subresourceInformation;
    }
}
