package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.GenericDAOImpl;
import in.gov.nvli.search.domain.MappingInformation;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Implementation of MappingInformation related function.
 *
 * @author Saurabh Koriya
 * @version 1
 * @since 1
 * @see GenericDAOImpl
 * @see IMappingInformationDao
 */
@Repository
public class MappingInformationDAOImpl extends GenericDAOImpl<MappingInformation, Long> implements IMappingInformationDao {

    /**
     * Setter method for session factory and it is autowired.
     *
     * @param sessionFactory {@link SessionFactory}
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }
}
