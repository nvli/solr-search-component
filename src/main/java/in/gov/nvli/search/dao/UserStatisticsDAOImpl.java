package in.gov.nvli.search.dao;

import in.gov.nvli.search.dao.genric.GenericDAOImpl;
import in.gov.nvli.search.domain.UserStatistics;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * Implementation of {@link UserStatistics} related function.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 * @see GenericDAOImpl
 * @see IUserStatisticsDAO
 * @see UserStatistics
 */
@Repository
public class UserStatisticsDAOImpl extends GenericDAOImpl<UserStatistics, Long> implements IUserStatisticsDAO {

    /**
     * Setter method for session factory and it is autowired.
     *
     * @param sessionFactory {@link SessionFactory}
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    /**
     * give count to assigned record in given subResourceCode.
     *
     * @param subResourceCode whose count need to find.
     * @return count of assigned records.
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public Long getTotalAssignedCount(final String subResourceCode) throws Exception {
        Session session = currentSession();
        Criteria userStatisticsCriteria = session.createCriteria(UserStatistics.class);
        Long count = (Long) userStatisticsCriteria.add(Restrictions.eq("subResourceidentifier", subResourceCode)).setProjection(Projections.sum("assignedCount")).uniqueResult();
        return count;
    }

    /**
     * Method give {@link UserStatistics} for given sub-resource code and given
     * user id.
     *
     * @param subResourceCode whose statistics need to find.
     * @param userId whose statistics need to find.
     * @return {@link UserStatistics}
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public UserStatistics getUserStatisticsByUserId_SubRes(final String subResourceCode, Long userId) throws Exception {
        Session session = currentSession();
        UserStatistics userStatistics = null;
        Criteria userStatisticsCriteria = session.createCriteria(UserStatistics.class);

        List<UserStatistics> list = userStatisticsCriteria.
                add(Restrictions.eq("userId", userId)).
                add(Restrictions.eq("subResourceidentifier", subResourceCode)).list();

        if (list != null && !list.isEmpty()) {
            userStatistics = list.get(0);
        }
        return userStatistics;
    }

    /**
     * Method give list of {@link UserStatistics} for given userIDs.
     *
     * @param userIds whose
     * @return list of {@link UserStatistics}
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public List<UserStatistics> getUserStatisticsByUserIds(List<Long> userIds) throws Exception {
        Session session = currentSession();
        Criteria userStatisticsCriteria = session.createCriteria(UserStatistics.class);
        List<UserStatistics> list = userStatisticsCriteria.add(Restrictions.in("userId", userIds)).list();
        return list;
    }

    /**
     * Method give list of sub-resource assigned to given user id.
     *
     * @param userId whose assigned sub-resource need to find.
     * @return list of sub-resource code
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public List<String> getAssingedSubResouceIdentifers(Long userId) throws Exception {
        Session session = currentSession();
        Criteria userStatisticsCriteria = session.createCriteria(UserStatistics.class);
        List<String> list = userStatisticsCriteria.add(Restrictions.eq("userId", userId)).
                setProjection(Projections.property("subResourceidentifier")).list();
        return list;
    }

    /**
     * Method give list of {@link UserStatistics} for given sub-resource code.
     *
     * @param subResourceCode whose statistics need to be find.
     * @return list of {@link UserStatistics}
     * @throws Exception
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    @SuppressWarnings("unchecked")
    public List<UserStatistics> getStatisticsBySubResourceCode(final String subResourceCode) throws Exception {
        Session session = currentSession();
        Criteria userStatisticsCriteria = session.createCriteria(UserStatistics.class);
        List<UserStatistics> list = userStatisticsCriteria.add(Restrictions.eq("subResourceidentifier", subResourceCode)).list();
        return list;
    }
}
