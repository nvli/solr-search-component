package in.gov.nvli.search.dao;

import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.form.SearchForm;
import in.gov.nvli.search.util.FacetNameCountHolder;
import in.gov.nvli.search.util.ResultHolder;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;

/**
 ** Interface for Search Related DAO.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 * @see DCMetadataInformation
 */
public interface ISearchDAO {

    /**
     * do basic search.
     *
     * @param searchForm {@link SearchForm}
     * @param pageNumber for pagination
     * @param pageWindow for pagination
     * @param enableExactWordSearch for enabling exact word search
     * @param facetRequired default facet required or not
     * @return {@link ResultHolder}
     * @throws ParseException
     * @throws HibernateException
     * @throws SQLException
     */
    public ResultHolder search(SearchForm searchForm, int pageNumber, int pageWindow, boolean enableExactWordSearch,boolean facetRequired) throws Exception;

    /**
     * do udc notation search.
     *
     * @param searchForm {@link SearchForm}
     * @param pageNumber for pagination
     * @param pageWindow for pagination
     * @return
     * @throws HibernateException
     * @throws SQLException
     */
    public ResultHolder udcSearch(SearchForm searchForm, int pageNumber, int pageWindow) throws Exception;

    /**
     * give date facets
     *
     * @return Map key: name of facet, value: list of
     * {@link FacetNameCountHolder}
     */
    public Map<String, List<FacetNameCountHolder>> getDATEFacet()throws Exception;
}
