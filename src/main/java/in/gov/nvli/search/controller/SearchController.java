package in.gov.nvli.search.controller;

import in.gov.nvli.search.dao.IDCMetadataInformationDAO;
import in.gov.nvli.search.domain.DCMetadataInformation;
import in.gov.nvli.search.form.SearchComplexityEnum;
import in.gov.nvli.search.form.SearchForm;
import in.gov.nvli.search.service.SearchService;
import in.gov.nvli.search.util.FacetNameCountHolder;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * Controller for handling request related to search.
 *
 * @author Hemant Anjana
 */
@RestController
@Secured("ROLE_ADMIN")
public class SearchController {

    /**
     * Autowired {@link SearchService}.
     */
    @Autowired
    private SearchService searchService;
    /**
     * Autowired {@link IDCMetadataInformationDAO}.
     */
    @Autowired
    private IDCMetadataInformationDAO dCMetadataInformationDAO;
    /**
     * logger instance
     */
    private static final Logger logger = Logger.getLogger(SearchController.class.getName());

    /**
     * Basic Search function POST Method to return the search result
     *
     * @param searchForm {@link SearchForm} contains parameters related to
     * search.
     * @param pageNum page number for pagination.
     * @param pagewin page window for pagination.
     * @param enableExactWordSearch flag to enable or disable exact word search.
     * @param facetRequired default facet required or not.
     * @return {@link SearchForm} with search results.
     */
    @RequestMapping(value = "/basicSearch/{pageNum}/{pageWin}", method = RequestMethod.POST, consumes = "application/json")
    public SearchForm basicSearchResultPost(@RequestBody SearchForm searchForm, @PathVariable("pageNum") String pageNum, @PathVariable("pageWin") String pagewin, @RequestParam(required = false, value = "exactMatch") boolean enableExactWordSearch,@RequestParam(required = false, value = "facetRequired") boolean facetRequired) {
        try {
            boolean goForSearch = false;
            if (searchForm.getSearchComplexity().equals(SearchComplexityEnum.BASIC)) {
                goForSearch = true;
            } else if (searchForm.getSearchComplexity().equals(SearchComplexityEnum.ADVANCED) && searchForm.getAdvanceSearchQueryString() != null && !searchForm.getAdvanceSearchQueryString().isEmpty()) {
                goForSearch = true;
            } else {
                goForSearch = false;
            }
            if (goForSearch) {
                searchForm = searchService.doSearch(searchForm, pageNum, pagewin, enableExactWordSearch,facetRequired);
            } else {
                searchForm.setResultFound(false);
            }
        } catch (Exception ex) {
            searchForm.setResultFound(false);
            logger.error(ex, ex);
            ex.printStackTrace();
        }
        return searchForm;
    }

    /**
     * Basic Search function GET Method to return the search result
     *
     * @param searchForm {@link SearchForm} contains parameters related to
     * search.
     * @param pageNum page number for pagination.
     * @param pagewin page window for pagination.
     * @param enableExactWordSearch flag to enable or disable exact word search.
     * @param facetRequired default facet required or not.
     * @return {@link SearchForm} with search results.
     */
    @RequestMapping(value = "/basicSearch/{pageNum}/{pageWin}", method = RequestMethod.GET)
    public SearchForm basicSearchResultGet(@RequestBody SearchForm searchForm, @PathVariable("pageNum") String pageNum, @PathVariable("pageWin") String pagewin, @RequestParam("exactMatch") boolean enableExactWordSearch,@RequestParam(required = false, value = "facetRequired") boolean facetRequired) {
        return basicSearchResultPost(searchForm, pageNum, pagewin, enableExactWordSearch,facetRequired);
    }

    /**
     * UDC Search function POST Method to return the search result.
     *
     * @param searchForm SearchForm.searchElement will contain udc notation.
     * @param pageNum page number for pagination.
     * @param pagewin page window for pagination.
     * @return {@link SearchForm} with search results.
     */
    @RequestMapping(value = "/udcSearch/{pageNum}/{pageWin}", method = RequestMethod.POST)
    public SearchForm basicUDCSearchPost(@RequestBody SearchForm searchForm, @PathVariable("pageNum") String pageNum, @PathVariable("pageWin") String pagewin) {
        try {
            if (searchForm.getSearchElement() != null && !searchForm.getSearchElement().isEmpty()) {
                searchForm = searchService.doUDCSearch(searchForm, pageNum, pagewin);
            } else {
                searchForm.setResultFound(false);
            }
        } catch (Exception ex) {
            searchForm.setResultFound(false);
            logger.error(ex, ex);
            ex.printStackTrace();
        }
        return searchForm;
    }

    /**
     * UDC Search function GET Method to return the search result.
     *
     * @param searchForm SearchForm.searchElement will contain udc notation.
     * @param pageNum page number for pagination.
     * @param pagewin page window for pagination.
     * @return {@link SearchForm} with search results.
     */
    @RequestMapping(value = "/udcSearch/{pageNum}/{pageWin}", method = RequestMethod.GET)
    public SearchForm basicUDCSearchGet(@RequestBody SearchForm searchForm, @PathVariable("pageNum") String pageNum, @PathVariable("pageWin") String pagewin) {
        return basicUDCSearchPost(searchForm, pageNum, pagewin);
    }

    /**
     * Method gives list of suggestions of keywords based on user input.
     *
     * @param searchTerm
     * @return List of String
     */
    @RequestMapping(value = "/suggestions/{searchTerm}", method = RequestMethod.GET)
    public List<String> getSuggestions(@PathVariable("searchTerm") String searchTerm) {
        return searchService.getSuggestions(searchTerm);
    }

    /**
     * Gives Date facet in map. Method give map contains facet name , and list
     * of FacetNameCountHolder. Supports GET request.
     *
     * {@link  FacetNameCountHolder}.
     *
     * @return Map
     */
    @RequestMapping(value = "/date/facets", method = RequestMethod.GET)
    public Map<String, List<FacetNameCountHolder>> getDateFacetsGET() {
        Map<String, List<FacetNameCountHolder>> facetMap = null;
        try {
            facetMap = searchService.getDateFacets();
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            ex.printStackTrace();
        }
        return facetMap;
    }

    /**
     * Gives Date facet in map. Method give map contains facet name , and list
     * of FacetNameCountHolder. Supports POST request.
     *
     * {@link  FacetNameCountHolder}.
     *
     * @return Map of Key asString and value as List of
     * {@link FacetNameCountHolder}
     */
    @RequestMapping(value = "/date/facets", method = RequestMethod.POST)
    public Map<String, List<FacetNameCountHolder>> getDateFacetsPOST() {
        return getDateFacetsGET();
    }

    /**
     * Method gives list of specific field suggestions of keywords based on user
     * input.
     *
     * @param searchField
     * @param searchTerm
     * @return
     */
    @RequestMapping(value = "/facetsuggestions/{searchField}/{searchTerm}", method = RequestMethod.GET)
    public List<String> getFacetSuggestions(@PathVariable("searchField") String searchField, @PathVariable("searchTerm") String searchTerm) {
        return searchService.getFacetSuggestions(searchField, searchTerm);
    }

    /**
     * Method gives list of similar words of keywords based on user input
     * searchTerm.
     *
     * @param searchTerm
     * @return
     */
    @RequestMapping(value = "/similar/{searchTerm}", method = RequestMethod.GET)
    public List<String> getDidYouMean(@PathVariable("searchTerm") String searchTerm) {
        return searchService.getDidYouMean(searchTerm);
    }

    /**
     * Method for updating index.
     *
     * @return true if successful else false.
     */
    @RequestMapping(value = "/updateIndex/", method = RequestMethod.GET)
    public Boolean updateIndex() {
        return searchService.dcIndexUpdation();
    }

    /**
     * Method for deleting index for given record identifier.
     *
     * @param recordIdentifier
     * @return true if successful else false.
     */
    @RequestMapping(value = "/deleteIndex/{recordIdentifier:.*}", method = RequestMethod.GET)
    public Boolean deleteIndex(@PathVariable("recordIdentifier") String deleteIndex) {
        return searchService.deleteIndex(deleteIndex);
    }
}
