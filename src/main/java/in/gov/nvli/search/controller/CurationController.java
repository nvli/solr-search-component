package in.gov.nvli.search.controller;

import in.gov.nvli.search.form.CurationCount;
import in.gov.nvli.search.form.CurationForm;
import in.gov.nvli.search.form.DistributionCount;
import in.gov.nvli.search.form.ResourceCurationCount;
import in.gov.nvli.search.service.CurationService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for handling request related to curation identified by
 * <u>/curation</u>.
 *
 * @author Hemant Anjana
 */
@RestController
@RequestMapping("/curation")
@Secured("ROLE_ADMIN")
public class CurationController {

    /**
     * Autowired {@link SearchService}.
     */
    @Autowired
    private CurationService curationService;

    /**
     * Method to get distribution count for given sub-resource code. Handles
     * POST request identified by <u>/getCurationCount/{subrCode}</u>
     * It gives total count and count of records available for distribution for
     * given sub-resource code.
     *
     * @param subResourceCode sub-resource code whose distribution count need to
     * find.
     * @return {@link DistributionCount}
     */
    @RequestMapping(value = "/getCurationCount/{subrCode}", method = RequestMethod.POST)
    public DistributionCount getCountForDistrubution(@PathVariable("subrCode") String subResourceCode) {
        System.out.println("getCuration count called");
        return curationService.getCountAvailableForDistribution(subResourceCode);
    }

    /**
     * Method to get distribution count for given sub-resource code. Handles GET
     * request identified by <u>/getCurationCount/{subrCode}</u>
     * It gives total count and count of records available for distribution for
     * given sub-resource code.
     *
     * @param subResourceCode sub-resource code whose distribution count need to
     * find.
     * @return {@link DistributionCount}
     */
    @RequestMapping(value = "/getCurationCount/{subrCode}", method = RequestMethod.GET)
    public DistributionCount getCountForDistrubutionGET(@PathVariable("subrCode") String subResourceCode) {
        return getCountForDistrubution(subResourceCode);
    }

    /**
     * Method to distribute records of given sub-resource code among given list
     * of users ids. Handles POST request identified by
     * <u>/distribute/{subrCode}</u> and take Request body list of user
     * ids(long).
     *
     * @param subResourceCode sub-resource code whose records need to
     * distributed.
     * @param userIds List of user ids among whom records need to be
     * distributed.
     * @return true if successful else false.
     */
    @RequestMapping(value = "/distribute/{subrCode}", method = RequestMethod.POST)
    public Boolean distrubute(@PathVariable("subrCode") String subResourceCode, @RequestBody List<Long> userIds) {
        return curationService.distribute(subResourceCode, userIds);
    }

    /**
     * Method to distribute records of given sub-resource code among given list
     * of users ids. Handles GET request identified by
     * <u>/distribute/{subrCode}</u> and take Request body list of user
     * ids(long).
     *
     * @param subResourceCode sub-resource code whose records need to
     * distributed.
     * @param userIds List of user ids among whom records need to be
     * distributed.
     * @return true if successful else false.
     */
    @RequestMapping(value = "/distribute/{subrCode}", method = RequestMethod.GET)
    public Boolean distrubuteGET(@PathVariable("subrCode") String subResourceCode, @RequestBody List<Long> userIds) {
        return distrubute(subResourceCode, userIds);
    }

    /**
     * Method to redistribute records of given sub-resource code among given
     * userMap. Handles GET request identified by
     * <u>/redistribute/{subrCode}</u> and take Request body Map of ids(long).
     * Method redistribute records that were assigned to user ids identified by
     * 'from-userids'(in userMap) to users identified by 'to-userids' (in
     * userMap).
     *
     * @param subResourceCode sub-resource code whose records need to
     * distributed.
     * @param userMap map containing two entries: 'from-userids' and
     * 'to-userids' with value long ids.
     *
     * userMap will have two entries with following keys.
     * <p>
     * key: 'from-userids':: value: list of long ids</p>
     * <p>
     * key: 'to-userids':: value: list long ids </p>
     * @return true if successful else false.
     */
    @RequestMapping(value = "/redistribute/{subrCode}", method = RequestMethod.POST)
    public Boolean redistribute(@PathVariable("subrCode") String subResourceCode, @RequestBody Map<String, List<Long>> userMap) {
        return curationService.redistribute(subResourceCode, userMap);
    }

    /**
     * Method to redistribute records of given sub-resource code among given
     * userMap. Handles POST request identified by
     * <u>/redistribute/{subrCode}</u> and take Request body Map of ids(long).
     * Method redistribute records that were assigned to user ids identified by
     * 'from-userids'(in userMap) to users identified by 'to-userids' (in
     * userMap).
     *
     * @param subResourceCode sub-resource code whose records need to
     * distributed.
     * @param userMap map containing two entries: 'from-userids' and
     * 'to-userids' with value long ids.
     *
     * userMap will have two entries with following keys.
     * <p>
     * key: 'from-userids':: value: list of long ids</p>
     * <p>
     * key: 'to-userids':: value: list long ids </p>
     * @return true if successful else false.
     */
    @RequestMapping(value = "/redistribute/{subrCode}", method = RequestMethod.GET)
    public Boolean redistributeGET(@PathVariable("subrCode") String subResourceCode, @RequestBody Map<String, List<Long>> userMap) {
        return redistribute(subResourceCode, userMap);
    }

    /**
     * Method give curation statistics of given user id. Handles POST request
     * identified by <u>/stats/user/{userId}</u>
     *
     * @param userId user id whose curation statistics need to find.
     * @return {@link ResourceCurationCount}
     */
    @RequestMapping(value = "/stats/user/{userId}", method = RequestMethod.POST)
    public ResourceCurationCount getUserStats(@PathVariable("userId") Long userId) {
        return curationService.getUserStats(userId);
    }

    /**
     * Method give curation statistics of given user id. Handles GET request
     * identified by <u>/stats/user/{userId}</u>
     *
     * @param userId user id whose curation statistics need to find.
     * @return {@link ResourceCurationCount}
     */
    @RequestMapping(value = "/stats/user/{userId}", method = RequestMethod.GET)
    public ResourceCurationCount getUserStatsGET(@PathVariable("userId") Long userId) {
        return getUserStats(userId);
    }

    /**
     * Method give Curation records list using {@link CurationForm}. Handles
     * POST request identified by <u>/list</u> and takes {@link CurationForm} as
     * request body.
     *
     * @param curationForm {@link CurationForm} contains parameter for list
     * records.
     * @return {@link CurationForm}
     */
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    public CurationForm getUserRecords(@RequestBody CurationForm curationForm) {
        return curationService.getUserRecords(curationForm);
    }

    /**
     * Method give Curation records list using {@link CurationForm}. Handles GET
     * request identified by <u>/list</u> and takes {@link CurationForm} as
     * request body.
     *
     * @param curationForm {@link CurationForm} contains parameter for list
     * records.
     * @return {@link CurationForm}
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CurationForm getUserRecordsGET(@RequestBody CurationForm curationForm) {
        return getUserRecords(curationForm);
    }

    /**
     * Method give curation statistics of given subResourceCode. Handles POST
     * request identified by <u>/stats/{subrCode}</u>
     *
     * @param subResourceCode whose curation statistics need to find.
     * @return Map containing key as user id and value as curation statistics of
     * that user id.
     */
    @RequestMapping(value = "/stats/{subrCode}", method = RequestMethod.POST)
    public Map<Long, CurationCount> getStatsBySubResouceCode(@PathVariable("subrCode") String subResourceCode) {
        return curationService.getSubResourceStats(subResourceCode);
    }

    /**
     * Method give curation statistics of given subResourceCode. Handles GET
     * request identified by <u>/stats/{subrCode}</u>
     *
     * @param subResourceCode whose curation statistics need to find.
     * @return Map containing key as user id and value as curation statistics of
     * that user id.
     */
    @RequestMapping(value = "/stats/{subrCode}", method = RequestMethod.GET)
    public Map<Long, CurationCount> getStatsBySubResouceCodeGET(@PathVariable("subrCode") String subResourceCode) {
        return getStatsBySubResouceCode(subResourceCode);
    }
}
