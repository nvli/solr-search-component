
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import in.gov.nvli.search.form.SearchForm;
import java.net.URISyntaxException;
import java.util.Map;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.web.client.RestTemplate;

/**
 * Class to test Rest Service.
 *
 * @author Madhuri
 */
public class Main {

    public static void main(String[] args) {
        String plainCreds = "MetadataIntegratorUser:mit_user@123#";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        HttpHeaders headers = new HttpHeaders();
        headers.add("authorization", "Basic " + base64Creds);
        RestTemplate restTemplate = new RestTemplate();

        String url1 = "http://clia3.pune.cdac.in:9000/NVLIRestWebService/webservice/GOVWHOST";
        // String url="http://clia3.pune.cdac.in:9000/NVLIRestWebService/webservice/ENEW";
        HttpEntity requestpost = new HttpEntity(headers);
        ResponseEntity<Map> responseEntity = restTemplate.postForEntity(url1, requestpost, Map.class);

        System.out.println("------" + responseEntity.getBody().size());

    }

    public static void main1111(String[] args) throws URISyntaxException {
        String plainCreds = "MetadataIntegratorUser:mit_user@123#";
        byte[] plainCredsBytes = plainCreds.getBytes();
        byte[] base64CredsBytes = Base64.encode(plainCredsBytes);
        String base64Creds = new String(base64CredsBytes);
        HttpHeaders headers = new HttpHeaders();
        headers.add("authorization", "Basic " + base64Creds);
        RestTemplate restTemplate = new RestTemplate();

        String url = "http://clia3.pune.cdac.in:9000/NVLIRestWebService/webservice/basicSearch/1/20";

        //String url="http://clia1.pune.cdac.in:8000/NVLIRestWebService/webservice/basicSearch/1/20";
        SearchForm s = new SearchForm();
        s.setResourceCode("ENEW");
        s.setSearchElement("Times");
        HttpEntity requestpost = new HttpEntity(s, headers);
        ResponseEntity<SearchForm> responseEntity = restTemplate.postForEntity(url, requestpost, SearchForm.class);
        SearchForm f = responseEntity.getBody();
        Gson g = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        System.out.println(g.toJson(f));

//        String url = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/get/tags/CustomTagging/NVLI_MUOFIN_alh_ald-AM-MIN-1582-3516_1463204613709";
//         HttpEntity requestpost = new HttpEntity(headers);
//         ResponseEntity<TagDetails> b1 =restTemplate.exchange(url, org.springframework.http.HttpMethod.GET,requestpost,TagDetails.class);
//         TagDetails tagDetails=b1.getBody();
//        System.out.println("---"+tagDetails.getTagValue());
//
//        tagDetails.setTagValue("saasasassas");
//
//
//
//        url = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/add/tags";
//
//            requestpost = new HttpEntity(tagDetails, headers);
//            ResponseEntity<Boolean> b = restTemplate.postForEntity(new URI(url),requestpost,Boolean.class);
//            System.out.println("=========="+b.getBody());
//         String url = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/NVLI_MANUPT_ignca-sb8472-ms_1_1463209917029";
//         HttpEntity<String> requestpost = new HttpEntity(headers);
//         ResponseEntity<MARC21MetadataStandard> responseEntity = restTemplate.exchange(url, org.springframework.http.HttpMethod.GET,requestpost,MARC21MetadataStandard.class);
//         MARC21MetadataStandard d=responseEntity.getBody();
//         System.out.println(d.getRecordIdentifier());
//         System.out.println(d.getIsCloseForEdit());
//
//         d.setIsCloseForEdit("no");
//
//
//         String url1 = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/update/marc21";
//         HttpEntity  requestpost1 = new HttpEntity(d,headers);
//         ResponseEntity<Void> responseEntity1 = restTemplate.postForEntity(url1, requestpost1,Void.class);
//        url = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/NVLI_DLIIND_book-1_1463206062991";
//        HttpEntity<String> requestpost = new HttpEntity(headers);
//        ResponseEntity<DCMetadataStandard> responseEntity = restTemplate.exchange(url, org.springframework.http.HttpMethod.GET, requestpost, DCMetadataStandard.class);
//        DCMetadataStandard dCMetadataStandard = responseEntity.getBody();
//        System.out.println("DCMETADATASTANDARD-->" + dCMetadataStandard.getRecordIdentifier() + "---" + dCMetadataStandard.getIsCloseForEdit());
//        //dCMetadataStandard.setIsCloseForEdit("no");
//        List<String> title=new ArrayList<>();
//            title.add("science");
//        dCMetadataStandard.getMetadata().setTitle(title);
//
//        url = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/record/crowdsource/update/dc";
//        HttpEntity requestpost1 = new HttpEntity(dCMetadataStandard, headers);
//        ResponseEntity<Boolean> responseEntity1 = restTemplate.postForEntity(url, requestpost1, Boolean.class);
//        System.out.println("DCMETADA AFTEUPDATE-->" + responseEntity1.getBody());
        //MARC21MetadataStandard d=responseEntity.getBody();
//        url = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/information/all/MOI_1462422684740_1462423050685";
//        ResponseEntity<SubResourceDetail> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, SubResourceDetail.class);
//        SubResourceDetail sd = responseEntity.getBody();
//        ResourceInformation rinfo = responseEntity.getBody().getResourceInformation();
//        SubresourceInformation srinfo = responseEntity.getBody().getSubResourceInformation();
//        rinfo.setResourceCode("JAI_MATA_DI");
//
//        srinfo.setActivationFlag(true);
//        srinfo.setEmailId("kuch@kuch.kcu");
//
//        headers.setContentType(MediaType.APPLICATION_JSON);
//
//        HttpEntity requestpost = new HttpEntity(sd, headers);
//        url = "http://archive.pune.cdac.in:8080/MetadataIntegratorWebService/webservice/save";
//        SubResourceDetail responseEntity1 = restTemplate.postForObject(url, requestpost, SubResourceDetail.class);
//        System.out.println(responseEntity1);
//        url = "http://localhost:8080/metadata-integrator-rest/webservice/popular/records/rarebooks_1462767030866";
//        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
//        Gson g = new Gson();
//        List<Object> list = responseEntity.getBody();
//        System.out.println("" + list.size());
//        System.out.println("" + list);
//        Iterator it = list.iterator();
//        int i = 0;
//        while (i < list.size()) {
//            LinkedHashMap hashMap = (LinkedHashMap) list.get(i);
//            System.out.println(hashMap.get("recordLikeCount") + "" + hashMap.get("recordIdentifier"));
//            i++;
//        }
        //To check update record like or view or download count NVLI_PRIYA5_koha_1_1463114731390
//        String r = "NVLI_PRIYA5_koha_1_1463114731390";
//        HttpHeaders headers1 = new HttpHeaders();
//        headers1.add("Authorization", "Basic " + base64Creds);
//        headers1.add("recordIdentifiers", r);
//        HttpEntity<String> request1 = new HttpEntity(headers1);
//        RestTemplate restTemplate1 = new RestTemplate();
//        String url1 = "http://localhost:8080/metadata-integrator-rest/webservice/metadata/update/like";
//        ResponseEntity<Void> responseEntity1 = restTemplate.exchange(url1, HttpMethod.GET, request1, Void.class);
//    Gson gson = new Gson();
//    Use follwing way to access url of put method  dont remove it
        //        TagDetails details = new TagDetails();
        //        details.setRecordIdentifier("NVLI_RAREBK_book-1_meta_1462954970716");
        //        details.setTagType(Constants.UDCTAGGING);
        //        details.setTagValue("599.62&|&336.2&|&71");
        //        HttpEntity<String> request = new HttpEntity(details, headers);
        //        url = "http://localhost:8080/metadata-integrator-rest/webservice/add/tags";
        //        restTemplate.put(url, request);
//  Use to call most popularrecord method
//    url = "http://localhost:8080/metadata-integrator-rest/webservice/popular/records/rarebooks_1462767030866";
//        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);
//        Gson g = new Gson();
//        List<Object> list = responseEntity.getBody();
//        System.out.println("" + list.size());
//        System.out.println("" + list);
//        Iterator it = list.iterator();
//        int i = 0;
//        while (i < list.size()) {
//            LinkedHashMap hashMap = (LinkedHashMap) list.get(i);
//            System.out.println(hashMap.get("recordLikeCount") + "" + hashMap.get("recordIdentifier"));
//            i++;
//        }
//    url = "http://localhost:8080/metadata-integrator-rest/webservice/crowdsource/get/metadata/NVLI_RAREBK_alh_ald-AM-MIN-603-1700_1463049798003";
//        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
//        System.out.println(responseEntity.getBody());
//        String url = "http://10.208.27.120:8080/searchRarebooks/webservice/similar/india";
//        HttpEntity requestpost = new HttpEntity(headers);
//        ResponseEntity<List> responseEntity = restTemplate.exchange(url, org.springframework.http.HttpMethod.GET, requestpost, List.class);
//        System.out.println(responseEntity.getBody());
    }
}
