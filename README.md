# Readme

* When You Clone the project, you will  find property files. 
 Name of property file are 'admin-settings.properties' and 'cdn-setting.properties'.

* To Run the project, you need to create directory as '.nvli_search_rarebooks' in 'user.home'.
 ('user.home' is user directory)

* '.nvli_search_rarebooks' directory is default directory, where indexes and logs will be created.

 In '.nvli_search_rarebooks' directory, create sub-directory and name it as 'conf'.
 And copy  'admin-settings.properties' and 'cdn-setting.properties'  in 'conf' folder. 
 
 i.e your user directory should look a like:- 

    'user.home'
        '.nvli_search_rarebooks'
                'conf'
                    'admin-settings.properties' (this is file which will be copied by you from code base after appropriate settings)
                    'cdn-setting.properties' (this is file which will be copied by you from code base after appropriate settings)

# eg: 
	C:\Users\admin\.nvli_semantic_rarebooks\conf\admin-settings.properties
	C:\Users\admin\.nvli_semantic_rarebooks\conf\cdn-setting.properties

* Once you are done with copying the files, you need to change configuration in these files as per requirement.

* In 'admin-settings.properties', you need to change database name, username and password. And also change 'nvli.index.folder' property value with your directory
* In 'cdn-setting.properties', you need to change cdn IP/hostname

# Note 
* If doing multiple installations you need to change following.
    * Copy Code base and rename project folder For eg. searchMOI
    * Create directory in 'user.home' as explained above. For eg. you create directory '.nvli_search_moi'.
    * Copy both properties files in it. And change properties specified above.
    * Changes in source code
        * Change webAppRootKey with new value for eg: searchMOI
        * Change \src\main\webapp\WEB-INF\etc\conf\applicationContext.xml for PropertyPlaceholderConfigurer.
            i.e replace '.nvli_search_rarebooks' with '.nvli_search_moi'
        * conf folder location in -\src\main\webapp\WEB-INF\etc\conf\dispatcher-servlet.xml for PropertyPlaceholderConfigurer.
            i.e replace '.nvli_search_rarebooks' with '.nvli_search_moi'
        * log location in  -\src\main\webapp\WEB-INF\etc\conf\log4j.xml  for log base folder & log files.
            i.e replace '.nvli_search_rarebooks' with '.nvli_search_moi'
         
    


 
